<?php

use Illuminate\Http\Request;

use App\User;

use App\Http\Resources\UserResource;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




// ========== Landing Page ===============

// home button
Route::get('/Home', 'TripController@HomeMobile');

// search button
// string keyword
Route::get('/Search', 'PageController@HomeAdmin')->name('homemobile');



// ========== Sign Page ===============

/* 
string - email 
string - password(bcypt)*/
Route::get('/SignIn', 'Usercontroller@SigninMobile')->name('SigninMobile');

// Forgot password
// string - oldpassword
// string - newpassword
Route::get('/ForgotPassword', 'Usercontroller@SigninMobile')->name('ForgotPassword');


// Create Account
// Route::get('/SignIn', 'Usercontroller@SigninMobile')->name('SigninMobile');



// ========== Home ===============
Route::get('/Indonesia_Trip', 'TripController@Indonesia_Trip')->name('Indonesia_Trip');
Route::get('/International_Trip', 'TripController@International_Trip')->name('International_Trip');