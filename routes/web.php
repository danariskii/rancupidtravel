<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/Home', 'HomeController@index');

Route::get('/', 'PageController@Index')->name('Home');

Auth::routes();

// Route::get('/Home', 'PageController@Home')->name('Home');
// Route::get('/agentDashboard', 'PageController@Agentdashboard')->name('agentdashboard');

Route::get('/adminRancupid', 'PageController@HomeAdmin')->name('homeAdmin');
Route::get('/allTripAdmin', 'AdminController@AllTrip')->name('allTripAdmin');
Route::get('/addTripAdmin', 'AdminController@AddTrip')->name('addTripAdmin');






Route::get('/agentDashboard', 'PageController@AgentDashboard')->name('agentDashboard');
Route::get('/register-agent', 'AgentController@RegisterAgent')->name('register-agent');
Route::post('travel_identity', ['as' => 'travel_identity', 'uses' => 'AgentController@travel_identity']);
Route::get('travelAgentProfile', 'AgentController@travelAgentProfile')->name('travelAgentProfile');
Route::post('travel_identityR', ['as' => 'travel_identityR', 'uses' => 'AgentController@travel_identityR']);

Route::get('indonesia_trip', 'PageController@search')->name('indonesia_trip');
Route::get('international_trip', 'PageController@search')->name('international_trip');
// Route::get('search/{keyword}', ['as' => 'search', 'uses' => 'PageController@Search']);

Route::get('/search', 'PageController@Search')->name('search');

// Route::get('contact', ['as' => 'contact', 'uses' => 'AdminController@create']);
// Route::post('contact', ['as' => 'contact_store', 'uses' => 'AdminController@store']);





// ============= admin ===============
Route::get('/seeAllUser', 'AdminController@SeeAllUser')->name('seeAllUser');
Route::post('updateStatusAgent', ['as' => 'updateStatusAgent', 'uses' => 'AdminController@UpdateStatusAgent']);
Route::get('/recommendations', 'AdminController@recommendations')->name('recommendations');
Route::get('/addRecommendationsList', 'AdminController@AddRecommendationsList')->name('addRecommendationsList');

// ============= end page ============






//============== page ================
Route::get('/contactUs', 'PageController@ContactUs')->name('contactUs');
Route::get('/wishlist', 'PageController@wishlist')->name('wishlist');
Route::get('/cart', 'PageController@cart')->name('cart');
Route::get('/detail-trip/{agent}/{slug}', 'PageController@DetailTrip')->name('detail-trip');
Route::post('/addtocart', ['as' => 'addtocart', 'uses' => 'OrderController@addtocart']);
Route::post('/addtowishlist', ['as' => 'addtowishlist', 'uses' => 'OrderController@addtowishlist']);

Route::get('images/{image}', function($image = null)
{
    $path = storage_path().'/product/' . $image;
    // $path_2 = storage_path().'app/public/directory/' . $image;
	// $path_3 = storage_path().'app/public/directory/Menu/' . $image;
	// $path_4 = storage_path().'app/public/directory/Makanan/' . $image;
    if (file_exists($path)) { 
        return Response::download($path);
    }
    // if (file_exists($path_2)) { 
    //     return Response::download($path_2);
    // }
    // if (file_exists($path_3)) { 
    //     return Response::download($path_3);
    // } 
    // else 
    // {
   	// 	 return Response::download($path_4); 	
    // }
});

Route::get('thumbnail/{image}', function($image = null)
{
    $path = storage_path().'/products/' . $image;
    $path_2 = storage_path().'/directory/' . $image;
    if (file_exists($path)) { 
        return Response::download($path);
    } else {
   		 return Response::download($path_2); 	
    }
});


Route::get('/img/{path}/{relation}/{file}', ['as' => 'file_preview', 'uses' => 'PageController@preview']); 

Route::get('/handleback', 'PageController@HandleBack')->name('handleback');
Route::get('/error-payment', 'PageController@ErrorPayment')->name('error-payment');

Route::get('/myprofile/confirmed-payment', 'PageController@MyProfile')->name('myprofile');
Route::get('/myprofile/pending-payment', 'PageController@MyProfile')->name('myprofile');

Route::get('/Customer_Dashboard', 'CustomerController@index')->name('Customer_Dashboard');
Route::get('/Customer_Review', 'CustomerController@review')->name('Customer_review');
Route::get('/Customer_Discussion', 'CustomerController@discussion')->name('Customer_Discussion');

Route::post('/uploadphototrip', ['as' => 'uploadphototrip', 'uses' => 'TripController@uploadphototrip']);
//====================================


// ============== agent ===============
Route::get('/TravelAgentDashboardPage', 'AgentController@TravelAgentDashboard')->name('TravelAgentDashboardPage');
Route::get('/addTrip', 'AgentController@addTrip')->name('addTrip');
Route::get('/allTrip', 'AgentController@allTrip')->name('allTrip');
Route::get('/allOrders', 'AgentController@allOrders')->name('allOrders');
Route::get('/editTrip/{product}', 'AgentController@editTrip')->name('editTrip');

Route::post('addNewTrip', ['as' => 'add_NewTrip', 'uses' => 'AgentController@addNewTrip']);
Route::post('editNewTrip', ['as' => 'edit_NewTrip', 'uses' => 'AgentController@editNewTrip']);
Route::post('photo_trip_temp', ['as' => 'photo_trip_temp', 'uses' => 'AgentController@photo_trip_temp']);
Route::post('deleteTrip', ['as' => 'deleteTrip', 'uses' => 'AgentController@deleteTrip']);
Route::post('deletePhoto', ['as' => 'deletePhoto', 'uses' => 'AgentController@deletePhoto']);
// ============== end agent ===========

// =============== order ==============
Route::post('makeOrder', ['as' => 'makeOrder', 'uses' => 'OrderController@makeOrder']);
// ============= end order ============
// Landing Page
// Route::get('/SigninMobile', 'Usercontroller@SigninMobile')->name('SigninMobile');
// Route::get('/homemobile', 'PageController@HomeAdmin')->name('homemobile');
// Route::get('/search', 'PageController@HomeAdmin')->name('HomeAdmin');


// Route::get('/Search', function(){
// 	return view('Search');
// });




Route::get('/Package_Trip', 'PageController@Package_Trip')->name('Package_Trip');

Route::get('/Add_Package_Trip', 'PageController@Add_Package_Trip')->name('Package_Trip');