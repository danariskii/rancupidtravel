<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute_list extends Model
{
    protected $fillable = ['filename', 'created_by', 'attribute', 'type'];
}
