<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripDate extends Model
{
    protected $fillable = [
		'agent_id','product_id','start_trip_date','end_trip_date'
    ];

    public function ProductDate()
    {
		return $this->belongsTo('App\Product');
    }

}
