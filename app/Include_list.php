<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Include_list extends Model
{
    protected $fillable = [
        'agent_id','product_id','attribute_id'
    ];
}
