<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_price extends Model
{
    protected $fillable = ['product_id','min_quantity','max_quantity','price_order','price',
    ];

    public function ProductPrice()
    {
		return $this->belongsTo('App\Product');
    }

    public function getLowestPrice($product_id)
    {
		// $price = App\Detail_price::select('price')->where('product_id', $product->id)->first();
    	return $this->attributes['price']->where('price','>=',0)->where('product_id', $product_id)->min('price');
    }
}
