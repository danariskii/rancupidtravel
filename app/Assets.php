<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    protected $fillable = ['relation_id', 'filename', 'type', 'sticky', 'created_by'];
}
