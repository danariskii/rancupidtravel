<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_tags extends Model
{
    protected $fillable = [
        'tags_id','product_id'
    ];
}
