<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_recommendation extends Model
{
    protected $fillable = [
        'recommendation_id','product_id'
    ];
}
