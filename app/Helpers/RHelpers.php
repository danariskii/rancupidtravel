<?php 

namespace App\Helpers;
use DB;

use Auth;
use Config;
use Mail;
use File;

/*

Danariski genuine

*/


class RHelpers
{

    /*
    type trip
    1 = open trip
    2 = private trip

    roles user
    sa = super admin;
    v = vendor / agent travel;
    u = user / customer;

    status trip
    1 = draft
    2 = published
    3 = on going
    4 = expired

    asset 
    1 = Avatar Customer
    2 = Product
    3 = Avatar Travel Agent
    4 = Travel Agent IDC Photo
    5 = Travel Agent NPWP Photo

    status pembayaran
    1 = pending payment
    2 = waiting approval
    3 = approved
    */



	public function getCategories($parentId = 0)
    {
        $categories = [];

        foreach(Location::where('parent_id', 0)->get() as $category)
        {
            $categories = [
                'item' => $category,
                'children' => $this->getCategories($category->id)
            ];
        }

        return $categories;
    }

    public static function test2(){
        echo 'TESSSSSSSSSS';
    }

    public static function attributeVendor($category,$product) {
        $categories = Vendor::find($product)->info_vendors;
        if (!empty($categories)) {
            foreach ($categories as $key => $value) {
                if( $category == $value->info_vendor_id) {
                    echo 'checked';
                } else {
                    echo null;
                }
            }
        }
    }
    public static function locationVendor($category,$product) {
        $categories = Vendor::find($product)->locations;
        if (!empty($categories)) {
            foreach ($categories as $key => $value) {
                if( $category == $value->location_id) {
                    echo 'checked';
                } else {
                    echo null;
                }
            }
        }
    }

    /* Super admin */
    public static function isSuperMaster() {
        if( Auth::user()->access == Config::get('status.superadmin') || Auth::user()->access == Config::get('status.admin') ) {
            return true;
        }
    }
    /* Super admin */
    public static function isSuperAdmin() {
        if( Auth::user()->access == Config::get('status.superadmin') ) {
            return true;
        }
    }
    /*  admin */
    public static function isAdmin() {
        if( Auth::user()->access == Config::get('status.admin') ) {
            return true;
        }
    }
    /* merchant */
    public static function isVendor() {
        if( Auth::user()->access == Config::get('status.vendor') ) {
            return true;
        }
    }
    /* merchant */
    public static function isSales() {
        if( Auth::user()->access == Config::get('status.sales') ) {
            return true;
        }
    }
    /* merchant */
    public static function iscustomer() {
        if( Auth::user()->access == Config::get('status.customer') ) {
            return true;
        }
    }


    public static function StatusPembayaran($status)
    {
        if( $status == 0 )
        {
            return Config::get('status.0');
        }
        elseif( $status == 1 )
        {
            return Config::get('status.1');
        }
        elseif( $status == 2 )
        {
            return Config::get('status.2');
        }
    }


    /*
        Email Register
    */
    public static function emailRegister($input) { 

        $user = array( 'email' => $input['email'], 'subject' => $input['subject'] );
        Mail::send('emails.register', $input, function($message) use ($user){
            $message->from('info@dealmakan.com', 'Dealmakan');
            $message->to($user['email']);
            $message->subject( $user['subject'] );
        });  
 
    }

    /*
        Email Invoice
    */
    public static function emailInvoice($input) { 

        $user = array( 'email' => $input['email'], 'subject' => $input['subject'] );
        Mail::send('emails.invoice', $input, function($message) use ($user){
            $message->from('info@dealmakan.com', 'DealMakan');
            $message->to($user['email']);
            $message->subject( $user['subject'] );
        });  
 
    }


    /*
        Email confirm
    */
    public static function emailConfirm($input) { 

        $user = array( 'email' => $input['email'], 'subject' => $input['subject'] );
        Mail::send('emails.confirm', $input, function($message) use ($user){
            $message->from('info@dealmakan.org', 'DealMakan');
            $message->to($user['email']);
            $message->subject( $user['subject'] );
        });  
 
    }

    /*
        check self order
    */
    public static function checkSelfOrder($id) {
        $self_order = Order::where('customer_id', Auth::user()->id)->where('order_number', $id)->first();
        if(is_null($self_order))
            die('Order Number Not Exists');
    }
    public static function checkTransactionType($id) {
        $self_order = Order::where('customer_id', Auth::user()->id)->where('order_number', $id)->value('transaction_type');
        if(!is_null($self_order))
            die('Lakukan Pembayaran');
    }

    /*
        GLOB
    */
    public static function totalPayOnline($order_id) {
        $total_pay_online = OrderDetail::where('order_id', $order_id)->whereNull('usage')->where('payment_type', 2)->sum('price');
        return $total_pay_online;
    } 
    public static function statusOrder($order_id) {
        $result = Order::where('id', $order_id)->value('transaction_type');
        return $result;
    }   
    public static function getDiscount($price, $original_price) {
        $original_price = str_replace('.', '', $original_price);
        $price = str_replace('.', '', $price);

        if( $original_price != 0 ) {
            $discount = ( 1- $price / $original_price ) * 100;
            $discount =  abs( number_format($discount, 0) );
            return $discount;
        } else {
            return 0;
        }
    }
    public static function  getImages($relation_id, $filename) {
        $myfile = storage_path('/app/public/products/'.$relation_id.'/'. $filename); 
        $myfile2 = storage_path('/app/public/products/'.$relation_id.'/thumbnail/'. $filename); 
        if (File::exists($myfile2))  {  
            return url('/img/products/').'/'.$relation_id.'/'. $filename;
        } else {
          return 'http://placehold.it/240x240?text=Dealmakan';    
        }
    }

    public static function  getImagesDirectory($relation_id, $filename) {
        $myfile = storage_path('/app/public/directory/'.$relation_id.'/'. $filename); 
        $myfile2 = storage_path('/app/public/directory/'.$relation_id.'/thumbnail/'. $filename); 
        if (File::exists($myfile2))  {  
            return url('/img/directory/').'/'.$relation_id.'/'. $filename;
        } else {
          return 'http://placehold.it/240x240?text=Dealmakan';    
        }
    }
}
  