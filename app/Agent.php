<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
	use Searchable;
	
    protected $fillable = ['travel_agent_photos','travel_agent_name','telephone_agent','email_agent','identity_card_agent_number','identity_card_agent_photos','npwp_agent_number','npwp_agent_photos','office_agent_address','coordinate','media_social_facebook','media_social_instagram','media_social','website','whatapp_agent','line','user_id_1','user_id_2','user_id_3'];

    public function trips()
    {
    	return $this->hasMany('App\Product');
    }

    // public function searchableAs()
    // {
    //     return 'travel_agent_name';
    // }
}
