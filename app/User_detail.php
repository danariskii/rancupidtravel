<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_detail extends Model
{

    protected $fillable = [
    	'user_id', 'first_name', 'last_name', 'place_of_birth', 'date_of_birth', 'gender', 'occupation', 'phone_number', 'telephone_number', 'address',
    	'email', 'instagram', 'facebook', 'province', 'city', 'identity_card_number', 'identity_card_photos', 'npwp_number', 'npwp_photos', 'saving_book_photos'
    ];

}
