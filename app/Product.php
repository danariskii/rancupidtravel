<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Searchable;
    protected $fillable = [ 
        'agent_id','created_by','cp_user_id','cp_user_id_2','slug','title','type_trip','text_1','text_2','text_3','text_4','text_5','location_trip','location_trip_latitude','location_trip_longitude','meeting_point','meeting_point_latitude','meeting_point_longitude','total_size_photo','country_id','province_id','districs_id','city_id','place_id','status'    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    // public function searchable As()
    // {
    //     return 'title';
    // }

    public function AgentProduct()
    {
		return $this->belongsTo('App\Agent');
    }

    public function priceProduct()
    {
        return $this->hasMany('App\Detail_price');
    }

    public function dateProduct()
    {
        return $this->hasMany('App\TripDate');
    }

    public function attributeProduct()
    {
        return $this->hasMany('App\Attribute_list');
    }

    public function tagProduct()
    {
        return $this->hasMany('App\Item_tags');
    }

    public function clostestTrip()
    {
        return $this->hasMany('App\TripDate');
    }

    public function displayItem()
    {
        return $this->hasMany('App\Assets', 'relation_id');
    }

    public function tripPhoto()
    {
        return $this->hasMany('App\Assets', 'relation_id');
    }

    public function lowestPrice()
    {
        return $this->hasMany('App\Detail_price');
    }

}
