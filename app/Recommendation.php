<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $fillable = [
        'list_name','list_type','created_by','last_edited_by'
    ];
}
