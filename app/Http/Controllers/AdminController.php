<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\User;
use App\User_detail;
use App\Agent;

use DB;

use App\Http\Requests\CreateTripRequest;

use App\Product;
use App\TripDate;
use App\Itenerary;
use App\Include_list;
use App\Exclude_list;
use App\Destination;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Recommendation;
use App\Item_recommendation;
use App\Attribute_list;

class AdminController extends Controller
{

    public function SeeAllUser()
    {
        $select = 'AllUser';

        // $User = DB::table('Users')->get();
        $Users = DB::table('users')
        ->leftjoin('agents', 'agents.user_id_1', '=', 'users.id')->get();

        // dd($Users);
        
        $JumlahUser = count($Users);
        $AgentNew = Agent::where('status','1')->count();
        return view('admin/AllUser', compact('select', 'AgentNew', 'Users', 'JumlahUser'));
    }

    public function AllTrip()
    {
        $date = date('m-d');
        
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'sa')
            {
                $select = 'AllTrip';
                $JumlahUser = User::all()->count();
                $AgentNew = Agent::where('status','1')->count();

        $date = date('m-d');

                $Trips = 
                DB::select('SELECT *, 
                    (SELECT travel_agent_name FROM agents WHERE id = p.agent_id) as Agent_Name,
                    (SELECT start_trip_date from trip_dates where product_id = p.id 
                        and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                        and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as start_date
                    from products as p '  );

                // $Trips = DB::table('products')
                //     ->join('agents', 'agents.id', '=', 'products.agent_id')
                //     ->join('trip_dates', 'trip_dates.product_id', '=', 'products.id')
                //     ->whereRaw('DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) and start_trip_date not like \'%-' . $date . '\'')
                //     ->orderBy('start_trip_date')
                //     ->groupBy('product_id')                    
                //     ->get();
                
                // $trip_dates =  DB::table('trip_dates')
                // ->select('product_id', 'start_trip_date')
                // ->whereRaw('DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) and start_trip_date not like \'%-' . $date . '\'')
                // ->orderBy('start_trip_date')
                // ->groupBy('product_id')
                // ->get();

                // $AgentTrips = Agent::find(1)->trips();
                // $Tripnya = Product::find(1)->AgentProduct();

                // $AgentTrips = Agent::with('trips')->get();
                // $Tripnya = Product::with('AgentProduct.travel_agent_name')->get();

        $today_birthdays = User_detail::whereRaw("date_of_birth like '%-$date'")
            ->orderBy('date_of_birth', 'desc')
            ->get();

        $upcoming_birthdays = User_detail::whereRaw('DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(date_of_birth) and date_of_birth not like \'%-' . $date . '\'')
            ->orderBy('date_of_birth', 'desc')
            ->limit(10)
            ->get();

                // dd($Trips);
                return view('admin/AllTrip', compact('select', 'AgentNew', 'JumlahUser', 'Trips', 'AgentTrips'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
        // return view('agent/adminlte/AddPackageTrip', compact('select', $select));
    }

    public function AddTrip()
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'sa')
            {
                $select = 'AddTrip';
                $Attribute_list = Attribute_list::all();
                $JumlahUser = User::count();
                $AgentNew = Agent::where('status','1')->count();
                return view('admin/AddTrip', compact('select', 'JumlahUser','AgentNew','Attribute_list'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
        // return view('Customer_Dashboard', compact('select', 'User'));
    }


    // CreateTripRequest $request
    public function addNewTrip( Request $request )
    {
        // return redirect()->route('Home');



        // $Product = Product::Create();
        // $Product->id_agent = '1';
        // $Product->created_by = Auth::user()->id;
        // $Product->cp_user_id = user_id;
        // $Product->cp_user_id_2 = user_id_2;



        // dd($request->email);
        // dd($request->name);
        // dd($request->nama);

        /*
        $request->Title
        $request->Description
        $request->type_trip

        $request->country
        $request->provinsi
        $request->postal_code
        $request->locality
        $request->route
        $request->street_number

        $request->min_range_1
        $request->max_range_1
        $request->tripprice_1 

        ===
        encodedStr = htmlentities($html);
        $html = html_entity_decode($encodedStr);
        ===
        $request->itenerary
        $request->include
        $request->exclude
        $request->destination
        
        */

        // $Trip = new Product();

        // $Trip->title = $request->Title;
        // $Trip->description_1 = $request->itenerary;
        // $Trip->description_2 = $request->include;
        // $Trip->description_3 = $request->exclude;
        // $Trip->description_4 = $request->destination;

        // $Trip->id_agent = Auth::user()->id;
        // $Trip->cp_user_id = user_id;
        // $Trip->cp_user_id_2 = user_id_2;

        // price_list_number

        // $Trip->save();

        // $name = $request->get('nama');
        // $User = User::find(Auth::user()->id);
        // return Response::json($User);
        // dd($request->max_range_4);
        return $request->destination;

    }

    public function UpdateStatusAgent( Request $request )
    {
        $agent_id =  $request->agent_id;
        $status = $request->status;

        $Agent = Agent::find($agent_id);
        $Agent->status = $status;
        $Agent->save();

        $owner = $Agent->user_id_1;
        $User = User::find($owner);
        $User->roles = 'v';
        $User->save();

        return redirect()->route('seeAllUser');
    }

    public function recommendations(Request $request)
    {
        $select = 'Recommendations';
        $JumlahUser = User::count();
        $AgentNew = Agent::where('status','1')->count();

        // $recommendations = Recommendation::
        $recommendations = DB::table('recommendations as r')
        ->leftjoin('users', 'users.id', '=', 'created_by')
        ->select(DB::raw('list_name, list_type, (SELECT username FROM users u WHERE u.id = r.created_by) as created_by, (SELECT username FROM users u WHERE u.id = r.last_edited_by) as last_edited_by, (SELECT count(product_id) FROM item_recommendations ir WHERE ir.recommendation_id = r.id) as number_of_item'))
        ->get();
        
        // dd($recommendations);
        return view('admin/Recommendations', compact('recommendations','select', 'AgentNew', 'JumlahUser'));
    }

    public function AddRecommendationsList(Request $request)
    {
        $select = 'Recommendations';
        $JumlahUser = User::count();
        $AgentNew = Agent::where('status','1')->count();
        $recommendations = Recommendation::all();
        return view('admin/AddRecommendationsList', compact('recommendations','select', 'AgentNew', 'JumlahUser'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTripRequest $request)
    {
        //
        // dd($request->email);
        // dd($request->name);
        dd($request->message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
