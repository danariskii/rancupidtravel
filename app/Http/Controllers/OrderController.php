<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Cart;
use App\Wishlist;
use Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    public function addtocart(Request $request)
    {
        if ( Auth::check() )
        {
            $user_id = Auth::user()->id;
            $product_id = $request->id_product;

            $Cart = Cart::updateOrCreate(['customer_id' => $user_id, 'product_id' => $product_id]);
            $Cart->customer_id = $user_id;
            $Cart->product_id = $product_id;
            $Cart->quantity = '1';
            $Cart->save();

            $status = 'success';
            // return Response::json(array('Cart'=>$Cart,'status'=>$status));
            return Response::json($status);
        }
        else
        {
            $status = 'failed';
            return Response::json($status);
        }
    }

    public function addtowishlist(Request $request)
    {
        if ( Auth::check() )
        {
            $user_id = Auth::user()->id;
            $product_id = $request->id_product;

            $Wishlist = Wishlist::updateOrCreate(['customer_id' => $user_id, 'product_id' => $product_id]);
            $Wishlist->customer_id = $user_id;
            $Wishlist->product_id = $product_id;
            $Wishlist->save();

            $status = 'success';
            return Response::json($status);
        }
        else
        {
            $status = 'failed';
            return Response::json($status);
        }
    }

    public function makeOrder(Request $request)
    {
        Veritrans_Config::$serverKey = "VT-server-RF7wcX1Z2p0zeNl3iFRybmQK";
        $enable_payments = array('credit_card','cimb_clicks','mandiri_clickpay','echannel');

        $transaction_details = array(
          'order_id' => rand(),
          'gross_amount' => 94000, // no decimal allowed for creditcard
        );

        $cart = User::find(Auth::user()->id)->cart();
        dd($cart);
        $item_details = Array();
        
        // foreach( $products as $product)
            
            // $values = Array(
            //   'id' => $product->id,
            //   'price' => $price->price,
            //   'quantity' => $quantity->quantity,
            //   'name' => $product->title
            // );

            // array_push($item_details, $values);


        $transaction = array(
          'enabled_payments' => $enable_payments,
          'transaction_details' => $transaction_details,
          'item_details' => $item_details,
        );
          // 'customer_details' => $customer_details,

        $snapToken = Veritrans_Snap::getSnapToken($transaction);

// Transaction ID
// Transaction Total Nominal
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
