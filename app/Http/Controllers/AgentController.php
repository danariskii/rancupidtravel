<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;

use App\Assets;
use App\User;
use App\Product;
use App\TripDate;
use App\Itenerary;
use App\Include_list;
use App\Exclude_list;
use App\Destination;
use App\Agent;
use App\detail_price;
use App\Attribute_list;
use App\Recommendation;
use App\Item_recommendation;
use App\Tags;
use App\Item_tags;

use Session;
use DB;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function TravelAgentDashboard()
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'v')
            {
                $select = 'Dashboard';
                $user_id = Auth::user()->id;
                $Agent = Agent::where('user_id_1', $user_id)->first();
                return view('agent/Home', compact('select','Agent'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
    }

public function allOrders()
{
    return view('agent/Orders');
}

    public function AllTrip()
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'v')
            {
                $select = 'AllTrip';
                $user_id = Auth::user()->id;
                $Agent = Agent::where('user_id_1', $user_id)->first();                
                $Trips = Product::where('agent_id',$Agent->id)->get();

                // DB::select('SELECT *, 
                //     (SELECT travel_agent_name FROM agents WHERE id = '.$Agent->id.') as Agent_Name,
                //     (SELECT start_trip_date from trip_dates where product_id = p.id 
                //         and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                //         and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as start_date
                //     from products as p where agent_id='.$Agent->id );

                $AgentTrips = Agent::find(1)->trips();


                // dd($start_date_count);
                return view('agent/AllTrip', compact('select', 'Agent', 'Trips'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
        // return view('agent/adminlte/AddPackageTrip', compact('select', $select));
    }

    public function editTrip($product)
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'v')
            {
                $select = 'AddTrip';
                $user_id = Auth::user()->id;
                $Agent = Agent::where('user_id_1', $user_id)->first();
                $Attribute_list = Attribute_list::all();
                $product = Product::where('id', $product)->first();
                $tagged = $product->tagProduct->pluck('tags_id');
                // $tagged = Item_tags::select('tags_id')->where('product_id', $product->id)->get();
                //$tagged = DB::select('SELECT tags_id from item_tags where product_id = '.$product->id);//->where('product_id', $product->id)->get();
                $tagged = json_encode($tagged);
                // $tagged = json_encode('[{"tags_id":2},{tags_id:4},{"tags_id":5}]');
                // ["1","2","3"]
                // dd($tagged[0]->tags_id);
                $sticky = $product->tripPhoto->where('sticky','1')->first();
                // dd($sticky);

                if ( Session::exists('temp_photo_id') )
                {
                    $temp_id = Session::get('temp_photo_id');
                    $temp_id = $temp_id[0];
                }
                else
                {
                    $temp_id = $user_id.'-'.$Agent->id.'-'.time().'-'.rand();
                    Session::push('temp_photo_id', $temp_id);
                }
                
                // return $product;
                return view('agent/AddTrip', compact('select','Agent','Attribute_list', 'product', 'temp_id', 'tagged', 'sticky'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
        // return view('Customer_Dashboard', compact('select', 'User'));
    }

    public function AddTrip()
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'v')
            {
                $select = 'AddTrip';
                $user_id = Auth::user()->id;
                $Agent = Agent::where('user_id_1', $user_id)->first();
                $Attribute_list = Attribute_list::all();

                if ( Session::exists('temp_photo_id') )
                {
                    $temp_id = Session::get('temp_photo_id');
                    $temp_id = $temp_id[0];
                }
                else
                {
                    $temp_id = $user_id.'-'.$Agent->id.'-'.time().'-'.rand();
                    Session::push('temp_photo_id', $temp_id);
                }

                    // dd($temp_id);disini, habis bikin sesssion agent id
                $tagged = null;
                return view('agent/AddTrip', compact('select','Agent','Attribute_list', 'temp_id', 'tagged'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
        // return view('Customer_Dashboard', compact('select', 'User'));
    }

    public function addNewTrip( Request $request )
    {
        // dd($request->total_size_photo);
        $user_id = Auth::user()->id;
        $Agent = Agent::where('user_id_1', $user_id)->first();
        $id_product_edit = $request->id_product_edit;

        if($id_product_edit != 'baru')
        {
            $Trip = Product::find($id_product_edit);
        }
        else
        {
            $Trip = Product::Create();
        }

        $Trip->agent_id = $Agent->id;
        $Trip->created_by = $user_id;
        $Trip->cp_user_id = $user_id;
        // $Product->cp_user_id_2 = user_id_2;

        $Trip->slug = str_slug($request->Title);
        $Trip->title = $request->Title;
        $Trip->type_trip = $request->type_trip;

        $Trip->location_trip = $request->location_trip;
        $Trip->location_trip_latitude = $request->latitude_data;
        $Trip->location_trip_longitude = $request->longitude_data;

        $Trip->meeting_point = $request->meeting_point;
        $Trip->meeting_point_latitude = $request->latitude_data_mepo;
        $Trip->meeting_point_longitude = $request->longitude_data_mepo;
        // $Trip->meeting_point = $request->meeting_point;
        // location_trip_geolocation
        // meeting_point_geolocation
        $Trip->status = $request->statusTrip;
        
    // ========================= price function ======================================
        $ready_id_todelete = $request->ready_id_todelete;
        $ready_id_todelete = explode(',',$ready_id_todelete);
        $count_ready_id_todelete = count($ready_id_todelete);
        if( ($ready_id_todelete) != null )
        {
            for ($c=0; $c < $count_ready_id_todelete; $c++) 
            { 
                $delete_detail_price = detail_price::where('id', $ready_id_todelete[$c])->delete();
            }
        }

        $price_list_number = $request->price_list_number;
        for ($i=1; $i <= $price_list_number ; $i++)
        {
            $min_quantity = 'min_range_'.$i;
            $max_quantity = 'max_range_'.$i;
            $price = 'ready_price_'.$i;
            $ready_id = 'ready_id_'.$i;

            // $detail_price = detail_price::Create();
            $detail_price = detail_price::firstOrCreate(['id' => $request->$ready_id]);

            $detail_price->min_quantity = $request->$min_quantity;
            $detail_price->max_quantity = $request->$max_quantity;
            $detail_price->price = $request->$price;
            $detail_price->price_order = $i;
            $detail_price->product_id = $Trip->id;

            $detail_price->save();
        }
    // ===================== end price function ======================================

        $Trip->text_1 = $request->Itenerary;
        $Trip->text_2 = $request->Include;
        $Trip->text_3 = $request->Exclude;
        $Trip->text_4 = $request->Destination;
        $Trip->text_5 = $request->Description;

        $date_trip_number = null;
        $date_trip_number = $request->date_trip_number;

    // ========================= date function ======================================
        $ready_id_date_todelete = $request->ready_id_date_todelete;
        $ready_id_date_todelete = explode(',',$ready_id_date_todelete);
        $count_ready_id_date_todelete = count($ready_id_date_todelete);
        if( ($ready_id_date_todelete) != null )
        {
            for ($crdt=0; $crdt < $count_ready_id_date_todelete; $crdt++) 
            {
                $delete_trip_date = TripDate::where('id', $ready_id_date_todelete[$crdt])->delete();
            }
        }

        $check_date_trip_number = TripDate::where('product_id', $Trip->id)->get();
        for ($i=1; $i <= $date_trip_number ; $i++)
        {
            $start_trip = 'start_trip_'.$i;
            $end_trip = 'end_trip_'.$i;
            $ready_id_date = null;
            $ready_id_date = 'ready_id_date_'.$i;
            $ready_id_date = $request->$ready_id_date;

            if ($ready_id_date == null) 
            {
                $TripDate = TripDate::create();
                $TripDate->agent_id = $Agent->id;
                $TripDate->product_id = $Trip->id;
                $TripDate->start_trip_date = $request->$start_trip;
                $TripDate->end_trip_date = $request->$end_trip;
                $TripDate->save();
        // dd($TripDate);
            }
        }
    // ======================= end date function =====================================

    // ==================== include icon function ====================================
        $include_icon_list_number = $request->include_icon_list_number;

        $include_icon_id_todelete = $request->include_icon_id_todelete;
        $include_icon_id_todelete = explode(',',$include_icon_id_todelete);
        $count_include_icon_id_todelete = count($include_icon_id_todelete);

        if( ($include_icon_id_todelete) != null )
        {
            for ($iitd=0; $iitd < $count_include_icon_id_todelete; $iitd++) 
            {
                $delete_include_icon = Include_list::where('id', $include_icon_id_todelete[$iitd])->delete();
            }
        }

        $ready_part_icon_towrite = $request->ready_part_icon_towrite;
        $ready_part_icon_towrite = explode(',', $ready_part_icon_towrite);

        for ($i=0; $i < $include_icon_list_number ; $i++)
        {

            $include_id = 'include_id_'.$ready_part_icon_towrite[$i];
            $icon_id = 'icon_id_'.$ready_part_icon_towrite[$i];

            $Include_list = Include_list::firstOrCreate(['id' => $request->$include_id]);

            $Include_list->product_id = $Trip->id;
            $Include_list->agent_id = $Agent->id;
            $Include_list->attribute_id = $request->$icon_id;

            $Include_list->save();
        }
    // ==================== end include icon function ================================

        $Trip->total_size_photo = $request->total_size_photo;
        $Trip->save();

        $Trip_id = $Trip->id;

    // ==================== tag function ================================
        $tags = $request->tags;
                // dd($tags);


        if ($tags != null)
        {
                $tagged = $Trip->tagProduct->pluck('tags_id');
                $tagged = $tagged->toArray();
                $tagged = array_map('strval',$tagged);
                // $tagged = array_map((string)$tagged);

                if(count($tags) > count($tagged))
                {
                    $deleteTags = array_diff( $tags, $tagged);
                }
                else
                {
                    $deleteTags = array_diff( $tagged, $tags);
                }

                foreach ($deleteTags as $deleteTag) 
                {
                    $deletedTags = Item_tags::where('product_id', $Trip_id)->where('tags_id', $deleteTag)->delete();
                }

            foreach ($tags as $tag) 
            {
                $tag_data = Tags::where(['id' => $tag])->first();
                if ( isset($tag_data) == null ) 
                {
                    // dd('1. '.$tag_data);
                    $tag_data = Tags::create();
                    $tag_data->title = $tag;
                    $tag_data->save();
                }
                    // dd($tag_data);
                // $Trip->tagProduct()->sync($tag_data->id);

                $tag_product = Item_tags::firstOrCreate(['tags_id' => $tag_data->id], ['product_id' => $Trip_id]);
                $tag_product->tags_id = $tag_data->id;
                $tag_product->product_id = $Trip_id;
                $tag_product->save();
            }
        }
    // ================ end tag function ================================


    // ==================== photos function ================================
        // Get array of all source files
        $deletePhotos = $request->ready_id_photo_todelete;
        if( ($deletePhotos)!= "" )
        {
            $deletePhotos = explode(',',$deletePhotos);
            $count_deletePhotos = count($deletePhotos);

            for ($dp = 0; $dp < $count_deletePhotos; $dp++)
            {
                    $deletedPhoto = Assets::where('id', $deletePhotos[$dp])->where('type', '2')->first();
                    $deleteFilePhoto = public_path('storage/product/'.$Trip_id).'/'.$deletedPhoto->filename;
                    // dd($deleteFilePhoto);
                    $deletedPhoto->delete();
                    unlink($deleteFilePhoto);
            }
        }

        $temp_photo_id = Session::get('temp_photo_id');
        $temp_photo_id = $temp_photo_id[0];
        $source = public_path('storage/temp/').$temp_photo_id;

        if (!file_exists( public_path('/storage/product/').$Trip_id)) 
        {
            // mkdir( storage_path('app').'/public/product/'.$Trip_id, 0777, true);
            // mkdir( storage_path('app').'/public/product/'.$Trip_id.'/thumbnail', 0777, true);

            mkdir( public_path('storage/product/').$Trip_id, 0777, true);
            mkdir( public_path('storage/product/').$Trip_id.'/thumbnail', 0777, true);
        }
        if( file_exists( $source ) )
        {
            $files = scandir( $source );
            // $destination1 = storage_path('app/public/product/'.$Trip_id);
            $destination2 = public_path('storage/product/'.$Trip_id);

            // Identify directories
            // Cycle through all source files
            foreach ($files as $file) {
                if (in_array($file, array(".",".."))) continue;
                // If we copied this successfully, mark it for deletion
                if (copy($source.'/'.$file, $destination2.'/'.$file))
                {
                    $photo_slug = str_replace(" ","-",$file);
                    rename($destination2.'/'.$file, $destination2.'/'.$photo_slug);
                    $trip_photo = Assets::create();
                    $trip_photo->relation_id = $Trip_id;
                    $trip_photo->filename = $photo_slug;
                    $trip_photo->type = '2';
                    $trip_photo->save();
                    
                    // copy($source.'/'.$file, $destination2.'/'.$file);
                    $delete[] = $source.'/'.$file;
                }
            }

            // dd($delete);
            // Delete all successfully-copied files
            foreach ($delete as $file) {
              unlink($file);
            }
            rmdir( public_path('storage/temp/').$temp_photo_id );
        }

        $sticky_id = $request->sticky_id;
        if (is_numeric($sticky_id))
        {
            $update_sticky = Assets::where('relation_id', $Trip_id)->where('sticky', 1)->first();
            if ($update_sticky != null) {
                $update_sticky->sticky = null;
                $update_sticky->save();
            }
            $update_sticky = Assets::find($sticky_id);
            $update_sticky->sticky = 1;
            $update_sticky->save();
        }
        else if ($sticky_id == 'false')
        {
            // dd('false');
        }
        else
        {
            $update_sticky = Assets::where('relation_id', $Trip_id)->where('sticky', 1)->first();
            if ($update_sticky != null) {
                $update_sticky->sticky = null;
                $update_sticky->save();
            }

            $sticky_slug = str_replace(" ","-",$sticky_id);
            $find_photo = Assets::where('relation_id', $Trip_id)->where('filename', $sticky_slug)->first();
            $find_photo->sticky = 1;
            $find_photo->save();
        }
    // ================ end photos function ================================

        return redirect()->route('allTrip');

        /*
        $request->country
        $request->provinsi
        $request->postal_code
        $request->locality
        $request->route
        $request->street_number

        ===
        encodedStr = htmlentities($html);
        $html = html_entity_decode($encodedStr);
        ===
        
        */

        // $Trip->save();
        // return $request->destination;
    }

    public function photo_trip_temp( Request $request )
    {
        $temp_photo_id = $request->get('temp_photo_id');

        if (!file_exists( public_path('storage/temp/').$temp_photo_id))
        {
            mkdir( public_path('storage/temp/').$temp_photo_id, 0777, true);            
        }        

        $file = $request->file('file');

            // $photos = Input::all();
        // dd($photos);
            // $response = $this->image->upload($photos);

        // foreach ($files as $file)
        {
            $filename = $file->getClientOriginalName();
            $file->move( public_path('storage/temp/').$temp_photo_id, $filename);
        }
        return response()->json(['success'=>$filename]);

        // mkdir( public_path('asset/temp/').$temp_photo_id.'/thumbnail', 0777, true);
        // disini

    }

    public function deleteTrip( Request $request )
    {
        // dd($request);
        $id = $request->get('id');
        $Product = Product::find($id);
        $Price = detail_price::where('product_id', $id)->delete();
        $include = Include_list::where('product_id', $id)->delete();
        $assets = Assets::where('relation_id', $id)->where('type', '2')->get();
        $Product->delete();
        // $destination1 = storage_path('app/public/product/'.$id);
        $destination2 = public_path('storage/product/'.$id);

        if( $assets != null)
        {
            foreach ($assets as $asset)
            {
                // $file1 = $destination1.'/'.$asset->filename;
                $file2 = $destination2.'/'.$asset->filename;
                // unlink($file1);
                unlink($file2);
            }
        }    

        // rmdir( $destination1.'/thumbnail' );
        // rmdir( $destination1 );
        rmdir( $destination2.'/thumbnail' );
        rmdir( $destination2 );

        $dates = TripDate::where('product_id', $id)->delete();
        return response()->json(['success']);

        // kurang delete assets dan item tags
    }

    public function deletePhoto( Request $request )
    {
        // dd($request->get('filename'));
        // $product_id = $request->get('product_id');
        // $filename = $request->get('filename');
        // $asset = Assets::where('relation_id', $product_id)->where('type', '2')->delete();

        // $destination1 = storage_path('app/public/product/95');
        $destination2 = public_path('storage/product/112');
        // $file1 = $destination1.'/'.$filename;
        $file2 = $destination2.'/1416921091317.jpg';
        // unlink($file1);
        unlink($file2);

        return response()->json(['success']);
    }

    public function RegisterAgent()
    {
        $user_id = Auth::user()->id;
        $Agent = Agent::where('user_id_1', $user_id)->first();

        if($Agent != null)
        {
            $select = 'TravelAgentProfile';
            return view('agent/TravelAgentProfile', compact('select', 'Agent'));
        }
        else
        {
            $select = 'RegisterAgent';
            return view('agent/RegisterAgent', compact('select', 'Agent'));
        }
    }

    public function travelAgentProfile()
    {
        $select = 'TravelAgentProfile';

        $user_id = Auth::user()->id;
        $Agent = Agent::where('user_id_1', $user_id)->first();

        return view('agent/TravelAgentProfile', compact('select', 'Agent'));
    }

    public function travel_identity(Request $request)
    {
        $user_id = Auth::user()->id;
        $Agent = Agent::firstOrNew(['user_id_1' => $user_id]);
        // dd($Agent);

        if ( $Agent->exists == null )
        {
            $Agent->status = '1';
        }

        $Agent->user_id_1 = $user_id;
        $Agent->travel_agent_name = $request->travel_name;
        $Agent->telephone_agent = $request->telephone;
        $Agent->email_agent = $request->email;
        $Agent->npwp_agent_number = $request->npwp_number;
        $Agent->identity_card_agent_number = $request->identity_card_number;
        $Agent->office_agent_address = $request->office_address;

        $Agent->save();

        $select = 'TravelAgentProfile';
        return view('agent/TravelAgentProfile', compact('select', 'Agent'));
    }

    public function recommendations(Request $request)
    {
        $recommendations = recommendation::all();
        return view('agent/Recommendations', compact('recommendations'));
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
