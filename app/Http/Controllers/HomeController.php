<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'sa') 
            {
                // $select = 'Dashboard';
                // return view('admin/Home', compact('select', $select));
                return redirect()->route('homeAdmin');
            }
            elseif (Auth::user()->roles == 'v') 
            {
                // dd('asd');
                // $select = 'Home';
                // return view('agent/adminlte/home', compact('select', $select));
                return redirect()->route('Home');
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
    }
}
