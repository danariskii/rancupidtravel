<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\User_detail;
use Auth;
use DB;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $select = 'Overview';
        $User = User::find( Auth::user()->id );
        $User_detail = User_detail::where('user_id', '=', $User->id)->firstOrFail();
        // dd($User_detail);
        return view('Customer/Dashboard', compact('select', 'User', 'User_detail'));
    }

    public function review()
    {
        $select = 'Review';
        $User = User::find( Auth::user()->id );
        return view('Customer/Customer_Review', compact('select'));
    }

    public function addReview()
    {
        $ReviewUser = $request->review;
        $User = User::find( Auth::user()->id );
        $Review = Review::Create();
        $Review->ReviewBy = $User->id;
        $Review->Agent_id = $ReviewUser->agent_id;
        $Review->message = $ReviewUser->message;
        $Review->value1 = $ReviewUser->value1;
        $Review->value2 = $ReviewUser->value2;
        $Review->value3 = $ReviewUser->value3;
        $Review->value4 = $ReviewUser->value4;
        $Review->value5 = $ReviewUser->value5;
        $Review->save();
    }

    public function Productreview()
    {
        $select = 'Review';
        $User = User::find( Auth::user()->id );
        return view('Customer/Customer_Review', compact('select'));
    }

    public function addProductReview()
    {
        $ReviewUser = $request->review;
        $User = User::find( Auth::user()->id );
        $Review = Review::Create();
        $Review->ReviewBy = $User->id;
        $Review->Agent_id = $ReviewUser->product_id;
        $Review->message = $ReviewUser->message;
        $Review->value1 = $ReviewUser->value1;
        $Review->value2 = $ReviewUser->value2;
        $Review->value3 = $ReviewUser->value3;
        $Review->value4 = $ReviewUser->value4;
        $Review->value5 = $ReviewUser->value5;
        $Review->save();
    }

    public function discussion()
    {
        $select = 'Discussion';
        $User = User::find( Auth::user()->id );
        return view('Customer/Customer_Discussion', compact('select'));
    }

    public function addDiscussion()
    {
        $discussionUser = $request->discussion;
        $User = User::find( Auth::user()->id );
        $discussion = Discussion::findOrCreate( $discussionUser->id );
        
        $discussion->product_id = $discussionUser->product_id;
        $discussion->user_id = $discussionUser->user_id;
        $discussion->message = $discussionUser->message;
        $discussion->reply_id = $discussionUser->reply_id;
        $discussion->save();
    }

    public function message()
    {
        $select = 'Message';
        $User = User::find( Auth::user()->id );
        return view('Customer/Customer_Message', compact('select'));
    }

    public function addReplyMessage()
    {
        $MessageUser = $request->Message;
        $User = User::find( Auth::user()->id );
        $Message = Message::findOrCreate( $MessageUser->id );
        
        $Message->product_id = $MessageUser->product_id;
        $Message->user_id = $MessageUser->user_id;
        $Message->message = $MessageUser->message;
        $Message->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
