<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Product;

use App\Http\Resources\Users;
use App\Http\Resources\UserCollection;

use App\Http\Resources\IndonesiaTrip; //<<collection
use App\Http\Resources\Indonesia_Trip; 

class TripController extends Controller
{

    public function uploadphototrip(Request $request)
    {
dd('$ProjectID');
        $ProjectID = $request->get('ProjectID');
        if ($ProjectID == 'create')
        {
            $file = $request->file('file');
            $filename = str_random(4).'-'.time().'-'.$file->getClientOriginalName();

            $img = Image::make($file);
            $img->save( public_path('asset/app/public/test/'. $filename), 60);

            // $file->move(public_path('/uploads/Project/Temp/'.$file), $filename);
        }
        else
        {

        }







        // $file = Input::file('file');
        // $destinationPath = 'uploads';
        // // If the uploads fail due to file system, you can try doing public_path().'/uploads' 
        // $filename = str_random(12);
        // //$filename = $file->getClientOriginalName();
        // //$extension =$file->getClientOriginalExtension(); 
        // $upload_success = Input::file('file')->move($destinationPath, $filename);

        if( $img ) {
           return Response::json('success', 200);
        } else {
           return Response::json('error', 400);
        }
    }

    public function HomeMobile()
    {
        $User = User::all();
        return new UserCollection($User);
    }

    public function Indonesia_Trip()
    {
        $Indonesia_Trip = Product::all();
        return new IndonesiaTrip($Indonesia_Trip);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
