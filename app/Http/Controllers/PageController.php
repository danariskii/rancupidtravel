<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Assets;
use App\User;
use App\Agent;
use Auth;
use App\Product;
use App\Detail_price;
use App\Recommendation;
use App\Item_recommendation;
use App\TripDate;
use App\Include_list;
use App\Cart;
use App\Wishlist;

class PageController extends Controller
{

    public function Search(Request $request)
    {
        $keyword = $request->keyword;

        $products = Product::search($keyword)->get();

        // $keyword = Detail_price::getLowestPrice('1')->first();
        // return $products[0]->id;

        return view('Search', compact('keyword','products'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // DB::raw('SELECT id from recommendations where recommendations.list_name = Hot Trip')
        // $HotTrip_id = Item_recommendation::select('product_id')->where('recommendation_id', '=', 1)->get();
        // $HotTrip_id = DB::select('SELECT GROUP_CONCAT(product_id) FROM item_recommendations where recommendation_id = 1');

        $recommendation_list = Recommendation::find(1);

        $date = date('m-d');

        $HotTrips = 
        DB::select('SELECT *, 
            (SELECT travel_agent_name FROM agents WHERE id = p.agent_id) as Agent_Name,
            (SELECT MIN(price) FROM detail_prices WHERE product_id = p.id) AS Trip_Price,
            (SELECT start_trip_date from trip_dates where product_id = p.id 
                and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as start_date,
            (SELECT end_trip_date from trip_dates where product_id = p.id 
                and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as end_date,
            (SELECT filename FROM assets where relation_id = p.id and sticky = 1) as dp_item
            from products as p where id IN (SELECT product_id FROM item_recommendations where recommendation_id = 1)'  );

        // $photo = DB::select(SELECT filename FROM assets WHERE product_id );

        // $upcoming_birthdays = User_detail::whereRaw('DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(date_of_birth) and date_of_birth not like \'%-' . $date . '\'')
        //     ->orderBy('date_of_birth', 'desc')
        //     ->limit(10)
        //     ->get();

        // $upcoming_birthdays = DB::select('SELECT start_trip_date from trip_dates where product_id = 1 and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1');
        $asd = DB::select('SELECT * FROM trip_dates AS tp1 WHERE tp1.id = 
        (SELECT TP3.id FROM (SELECT id, DATEDIFF(start_trip_date,CURRENT_DATE) AS diff FROM trip_dates tp2 WHERE tp2.product_id = 4 ORDER BY diff ASC LIMIT 1) AS TP3)
        GROUP BY product_id');

        // dd($HotTrips);
        // ->whereIn('p.id', $HotTrip_id);
        // ->get();
        
        // ->leftjoin('detail_prices as dp', 'dp.product_id', '=', 'p.id')
        // ->select(DB::raw('SELECT price from detail_prices where detail_prices.product_id = p.id and min(price)'))
        // ->priceProduct()->min('price');

        // $Detail_price = Detail_price::select('price')->where('product_id', '=', '1')->min('price');

        return view('Home', compact('HotTrips','recommendation_list'));
    }

    public function AgentDashboard()
    {
        if (Auth::check())
        {            
                $select = 'Dashboard';
                $JumlahUser = User::count();

            if (Auth::user()->roles == 'sa')
            {
                $AgentNew = Agent::where('status','1')->count();
                return view('admin/Home', compact('select', 'JumlahUser', 'AgentNew'));
            }
            elseif(Auth::user()->roles == 'v')
            {
                $user_id = Auth::user()->id;
                $Agent = Agent::where('user_id_1', $user_id)->first();
                return view('agent/Home', compact('select','Agent'));
            }
            else
            {
                return redirect()->route('register-agent');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
    }


    public function HomeAdmin()
    {
        if (Auth::check())
        {            
            if (Auth::user()->roles == 'sa')
            {
                $select = 'Dashboard';
                $JumlahUser = User::count();
                $AgentNew = Agent::where('status','1')->count();
                // dd($AgentNew);
                return view('admin/Home', compact('select', 'JumlahUser', 'AgentNew'));
            }
            else 
            {
                return redirect()->route('Home');
            }
        }
        else 
        {
            return redirect()->route('Home');
        }
        // return view('agent/adminlte/AddPackageTrip', compact('select', $select));
    }

    public function ContactUs()
    {
        return view('ContactUs');
    }

    public function DetailTrip($agent, $slug)
    {
        $agents = explode("-", $agent);
        $agent_name = null;
        foreach ($agents as $agent) 
        {
            if($agent_name == null)
            {
                $agent_name = $agent;
            }
            else
            {
                $agent_name = $agent_name.' '.$agent;
            }
        }
        $agent = Agent::where('travel_agent_name', $agent_name)->first();
        $product = Product::where('slug',$slug)->where('agent_id',$agent->id)->first();
        $prices = Detail_price::where('product_id',$product->id)->get();
        $dates = TripDate::where('product_id',$product->id)->get();
        $include_list = Include_list::where('product_id', $product->id)->get();

        return view('DetailProduct', compact('agent', 'product', 'prices', 'dates', 'include_list'));
    }

    public function cart()
    {
        if ( Auth::check() )
        {
            $user_id = Auth::user()->id;

            $cart = cart::select('product_id')->where('customer_id',$user_id)->get();

            $date = date('m-d');        
            $products =  
            DB::select('SELECT *, 
                (SELECT travel_agent_name FROM agents WHERE id = p.agent_id) as Agent_Name,
                (SELECT MIN(price) FROM detail_prices WHERE product_id = p.id) AS Trip_Price,
                (SELECT start_trip_date from trip_dates where product_id = p.id 
                    and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                    and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as start_date,
                (SELECT end_trip_date from trip_dates where product_id = p.id 
                    and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                    and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as end_date,
                (SELECT filename FROM assets where relation_id = p.id and sticky = 1) as dp_item
                from products as p where id IN (SELECT product_id FROM carts where customer_id = '.$user_id.')'  );

                        // $price = Detail_price::select('price')
                        // ->where('product_id', 6)
                        // ->where('max_quantity')->max();
                        // ->where('min_quantity', '<=', 9)

    // return($price);

            return view('Cart', compact('cart', 'products'));
        }
        else
        {
            return redirect()->route('login');
        }
    }

    public function wishlist()
    {
        if ( Auth::check() )
        {
            $user_id = Auth::user()->id;

            $Wishlist = Wishlist::select('product_id')->where('customer_id',$user_id)->get();

            $date = date('m-d');        
            $products =  
            DB::select('SELECT *, 
                (SELECT travel_agent_name FROM agents WHERE id = p.agent_id) as Agent_Name,
                (SELECT MIN(price) FROM detail_prices WHERE product_id = p.id) AS Trip_Price,
                (SELECT start_trip_date from trip_dates where product_id = p.id 
                    and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                    and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as start_date,
                (SELECT end_trip_date from trip_dates where product_id = p.id 
                    and DAYOFYEAR(curdate()) + 1 <= DAYOFYEAR(start_trip_date) 
                    and start_trip_date not like \'%-' . $date . '\' ORDER BY start_trip_date ASC limit 1) as end_date,
                (SELECT filename FROM assets where relation_id = p.id and sticky = 1) as dp_item
                from products as p where id IN (SELECT product_id FROM wishlists where customer_id = '.$user_id.')'  );
    
            return view('Wishlist', compact('Wishlist', 'products'));
        }
        else
        {
            return redirect()->route('login');
        }
    }

    public function preview($path, $relation, $file, $thumb=null)
    {
        
        $path = storage_path('/'.$path.'/'. $relation .'/')  . $file;
        return $path;
        $handler = new \Symfony\Component\HttpFoundation\File\File($path);

        //$lifetime = 31556926; // One year in second
        $lifetime = 3155; // One year in seconds

        /**
        * Prepare some header variables
        */
        $file_time = $handler->getMTime(); // Get the last modified time for the file (Unix timestamp)

        $header_content_type = $handler->getMimeType();
        $header_content_length = $handler->getSize();
        $header_etag = md5($file_time . $path);
        $header_last_modified = gmdate('r', $file_time);
        $header_expires = gmdate('r', $file_time + $lifetime);

        $headers = array(
            'Content-Disposition' => 'inline; filename="' . $file . '"',
            'Last-Modified' => $header_last_modified,
            'Cache-Control' => 'must-revalidate',
            'Expires' => $header_expires,
            'Pragma' => 'public',
            'Etag' => $header_etag
        );

        /**
        * Is the resource cached?
        */
        $h1 = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && $_SERVER['HTTP_IF_MODIFIED_SINCE'] == $header_last_modified;
        $h2 = isset($_SERVER['HTTP_IF_NONE_MATCH']) && str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == $header_etag;

        if ($h1 || $h2) {
            return Response::make('', 304, $headers); // File (image) is cached by the browser, so we don't have to send it again
        }

        $headers = array_merge($headers, array(
            'Content-Type' => $header_content_type,
            'Content-Length' => $header_content_length
        ));

        return Response::make(file_get_contents($path), 200, $headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
