<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\User_detail;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/Home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // dd($data->dob);
        return Validator::make($data, [
            'First_Name'  => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
            'gender'    => 'required',
            'dob'       => 'required|string',
            'Handphone' => 'required|numeric|digits_between:10,13',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $User = User::create([
            'username'  => $data['First_Name'],
            'email'     => $data['email'],
            'password'  => bcrypt($data['password']),
            'roles'     => 'u',
        ]);

        $User_detail = User_detail::create([
            'user_id' => $User->id,
            'first_name' => $data['First_Name'],
            'last_name' => $data['Last_Name'],
            'date_of_birth' => $data['dob'],
            'gender' => $data['gender'],
            'phone_number' => $data['Handphone'],
        ]);

        return $User;
    }
}
