<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Indonesia_Trip extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'description_1' => $this->description_1,
            'description_2' => $this->description_2,
        ];
        
        // return parent::toArray($request);
    }
}
