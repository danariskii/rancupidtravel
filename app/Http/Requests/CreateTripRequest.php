<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use todoparrot\Http\Requests\Request;

class CreateTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        // untuk validasi form, siapa aja yg bisa gunain
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Title' => 'required',
            'Description' => 'required',
            // 'email' => 'required|email',
            //
        ];
    }
}
