<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\Attribute_list;

class attributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Attribute_list::truncate();

        $Attribute_list = Attribute_list::create();
        $Attribute_list->filename = 'snorkling.png';
        $Attribute_list->created_by = '1';
        $Attribute_list->attribute = 'snorkling';
        $Attribute_list->type = 'activity';

        $Attribute_list->save();


        $Attribute_list = Attribute_list::create();
        $Attribute_list->filename = 'hiking.png';
        $Attribute_list->created_by = '1';
        $Attribute_list->attribute = 'hiking';
        $Attribute_list->type = 'activity';

        $Attribute_list->save();


        $Attribute_list = Attribute_list::create();
        $Attribute_list->filename = 'beach.png';
        $Attribute_list->created_by = '1';
        $Attribute_list->attribute = 'beach';
        $Attribute_list->type = 'place';

        $Attribute_list->save();


        $Attribute_list = Attribute_list::create();
        $Attribute_list->filename = 'mountain.png';
        $Attribute_list->created_by = '1';
        $Attribute_list->attribute = 'mountain';
        $Attribute_list->type = 'place';

        $Attribute_list->save();


        $Attribute_list = Attribute_list::create();
        $Attribute_list->filename = 'car.png';
        $Attribute_list->created_by = '1';
        $Attribute_list->attribute = 'car';
        $Attribute_list->type = 'facility';

        $Attribute_list->save();


        $Attribute_list = Attribute_list::create();
        $Attribute_list->filename = 'launch.png';
        $Attribute_list->created_by = '1';
        $Attribute_list->attribute = 'launch';
        $Attribute_list->type = 'facility';

        $Attribute_list->save();

    }
}
