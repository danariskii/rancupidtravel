<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
		App\Product::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few articles in our database:
        for ($i = 1; $i < 5; $i++) {
            App\Product::create([
				'id_agent' => $i,
				'created_by' => $i,
				'cp_user_id' => '08161616329',
				'cp_user_id_2' => '087225206',
				'id_country' => '1',
				'id_province' => rand(1, 36),
				'id_districs' => rand(1, 500),
				'id_city' => rand(1, 5),
				'id_place' => rand(1,50),
				'title' => 'Sawarna '.$faker->sentence,
				'description_1' => $faker->sentence,
				'description_2' => $faker->sentence,
				'description_3' => $faker->sentence,
				'description_4' => $faker->sentence,
				'price_1'		=> rand(500000, 600000),
				'price_2'		=> rand(600000, 700000),
				'price_3'		=> rand(700000, 800000),
				'price_4'		=> rand(900000, 900000),
				'range_price_1'	=> '1-3',
				'range_price_2'	=> '3-6',
				'range_price_3'	=> '6-9',
				'range_price_4'	=> '9>'
            ]);
        }

        for ($i = 1; $i < 5; $i++) {
            App\Product::create([
				'id_agent' => $i,
				'created_by' => $i,
				'cp_user_id' => '08161616329',
				'cp_user_id_2' => '087225206',
				'id_country' => '1',
				'id_province' => rand(1, 36),
				'id_districs' => rand(1, 500),
				'id_city' => rand(1, 5),
				'id_place' => rand(1,50),
				'title' => 'Derawan '.$faker->sentence ,
				'description_1' => $faker->sentence,
				'description_2' => $faker->sentence,
				'description_3' => $faker->sentence,
				'description_4' => $faker->sentence,
				'price_1'		=> rand(500000, 600000),
				'price_2'		=> rand(600000, 700000),
				'price_3'		=> rand(700000, 800000),
				'price_4'		=> rand(900000, 900000),
				'range_price_1'	=> '1-3',
				'range_price_2'	=> '3-6',
				'range_price_3'	=> '6-9',
				'range_price_4'	=> '9>'
            ]);
        }

        for ($i = 1; $i < 5; $i++) {
            App\Product::create([
				'id_agent' => $i,
				'created_by' => $i,
				'cp_user_id' => '08161616329',
				'cp_user_id_2' => '087225206',
				'id_country' => '1',
				'id_province' => rand(1, 36),
				'id_districs' => rand(1, 500),
				'id_city' => rand(1, 5),
				'id_place' => rand(1,50),
				'title' => 'Krakatau '.$faker->sentence ,
				'description_1' => $faker->sentence,
				'description_2' => $faker->sentence,
				'description_3' => $faker->sentence,
				'description_4' => $faker->sentence,
				'price_1'		=> rand(500000, 600000),
				'price_2'		=> rand(600000, 700000),
				'price_3'		=> rand(700000, 800000),
				'price_4'		=> rand(900000, 900000),
				'range_price_1'	=> '1-3',
				'range_price_2'	=> '3-6',
				'range_price_3'	=> '6-9',
				'range_price_4'	=> '9>'
            ]);
        }

        for ($i = 1; $i < 5; $i++) {
            App\Product::create([
				'id_agent' => $i,
				'created_by' => $i,
				'cp_user_id' => '08161616329',
				'cp_user_id_2' => '087225206',
				'id_country' => '1',
				'id_province' => rand(1, 36),
				'id_districs' => rand(1, 500),
				'id_city' => rand(1, 5),
				'id_place' => rand(1,50),
				'title' => 'Pahaawang '.$faker->sentence ,
				'description_1' => $faker->sentence,
				'description_2' => $faker->sentence,
				'description_3' => $faker->sentence,
				'description_4' => $faker->sentence,
				'price_1'		=> rand(500000, 600000),
				'price_2'		=> rand(600000, 700000),
				'price_3'		=> rand(700000, 800000),
				'price_4'		=> rand(900000, 900000),
				'range_price_1'	=> '1-3',
				'range_price_2'	=> '3-6',
				'range_price_3'	=> '6-9',
				'range_price_4'	=> '9>'
            ]);
        }

        for ($k = 1; $k < 25; $k++) {
            App\Product::create([
				'id_agent' => $k,
				'created_by' => $k,
				'cp_user_id' => '08161616329',
				'cp_user_id_2' => '087225206',
				'id_country' => $k,
				'id_province' => rand(1, 36),
				'id_districs' => rand(1, 500),
				'id_city' => rand(1, 5),
				'id_place' => rand(1,50),
				'title' => 'Paris',
				'description_1' => $faker->sentence,
				'description_2' => $faker->sentence,
				'description_3' => $faker->sentence,
				'description_4' => $faker->sentence,
				'price_1'		=> rand(500000, 600000),
				'price_2'		=> rand(600000, 700000),
				'price_3'		=> rand(700000, 800000),
				'price_4'		=> rand(900000, 900000),
				'range_price_1'	=> '1-3',
				'range_price_2'	=> '3-6',
				'range_price_3'	=> '6-9',
				'range_price_4'	=> '9>'
            ]);
        }
    }
}
