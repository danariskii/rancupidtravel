<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_dates', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('agent_id')->nullable();
            $table->bigInteger('product_id')->nullable();

            $table->dateTimeTz('start_trip_date')->nullable();
            $table->dateTimeTz('end_trip_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_dates');
    }
}
