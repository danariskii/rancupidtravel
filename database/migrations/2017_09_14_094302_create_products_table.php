<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('agent_id')->nullable();
            $table->bigInteger('created_by')->nullable();

            $table->bigInteger('cp_user_id')->nullable();
            $table->bigInteger('cp_user_id_2')->nullable();

            $table->longText('slug')->nullable();
            $table->longText('title')->nullable();
            $table->mediumText('type_trip')->nullable();

            $table->longText('text_1')->nullable();
            $table->longText('text_2')->nullable();
            $table->longText('text_3')->nullable();
            $table->longText('text_4')->nullable();
            $table->longText('text_5')->nullable();

            $table->longText('location_trip')->nullable();
            $table->longText('location_trip_latitude')->nullable();
            $table->longText('location_trip_longitude')->nullable();
            $table->longText('meeting_point')->nullable();
            $table->longText('meeting_point_latitude')->nullable();
            $table->longText('meeting_point_longitude')->nullable();

            $table->bigInteger('total_size_photo')->nullable();
            $table->bigInteger('country_id')->nullable();
            $table->bigInteger('province_id')->nullable();
            $table->bigInteger('districs_id')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->bigInteger('place_id')->nullable();

            $table->integer('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
