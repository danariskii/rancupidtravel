<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_prices', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('product_id')->nullable();
            $table->integer('min_quantity')->nullable();
            $table->integer('max_quantity')->nullable();

            $table->bigInteger('price_order')->nullable();            
            $table->bigInteger('price')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_prices');
    }
}
