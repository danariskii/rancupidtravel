<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->longText('travel_agent_photos')->nullable();
            $table->longText('travel_agent_name')->nullable();
            $table->bigInteger('telephone_agent')->nullable();
            $table->string('email_agent')->nullable();

            $table->bigInteger('identity_card_agent_number')->nullable();
            $table->longText('identity_card_agent_photos')->nullable();

            $table->bigInteger('npwp_agent_number')->nullable();
            $table->longText('npwp_agent_photos')->nullable();

            $table->longText('office_agent_address')->nullable();
            $table->longText('coordinate')->nullable();

            $table->Text('media_social_facebook')->nullable();
            $table->Text('media_social_instagram')->nullable();
            $table->longText('media_social')->nullable();
            $table->longText('website')->nullable();

            $table->mediumInteger('whatapp_agent')->nullable();
            $table->Text('line')->nullable();

            $table->bigInteger('user_id_1')->nullable();
            $table->bigInteger('user_id_2')->nullable();
            $table->bigInteger('user_id_3')->nullable();

            $table->integer('status')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
