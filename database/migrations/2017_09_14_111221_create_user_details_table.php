<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id');
            $table->mediumText('first_name')->nullable();
            $table->mediumText('last_name')->nullable();

            $table->text('place_of_birth')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->Integer('gender')->nullable();
            $table->longText('avatar')->nullable();

            $table->mediumText('occupation')->nullable();
            $table->bigInteger('phone_number')->nullable();
            $table->bigInteger('telephone_number')->nullable();

            $table->longText('address')->nullable();
            $table->string('email')->nullable();

            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();

            $table->Integer('province')->nullable();
            $table->Integer('city')->nullable();


            $table->bigInteger('identity_card_number')->nullable();
            $table->longText('identity_card_photos')->nullable();
            $table->bigInteger('npwp_number')->nullable();
            $table->longText('npwp_photos')->nullable();
            $table->longText('saving_book_photos')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
