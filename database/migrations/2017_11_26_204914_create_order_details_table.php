<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('order_id')->nullable();
            $table->bigInteger('agent_id')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->string('voucher_code')->nullable();
            $table->bigInteger('total_price')->nullable();
            $table->bigInteger('customer_identity_card_number')->nullable();
            $table->bigInteger('customer_name')->nullable();
            $table->char('usage', 2)->nullable();
            $table->dateTime('expired')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
