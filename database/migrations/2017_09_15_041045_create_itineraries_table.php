<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itineraries', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('agent_id')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->bigInteger('place_id')->nullable();
            $table->string('day')->nullable();
            $table->dateTimeTz('time')->nullable();
            $table->longText('objective')->nullable();

            $table->bigInteger('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraries');
    }
}
