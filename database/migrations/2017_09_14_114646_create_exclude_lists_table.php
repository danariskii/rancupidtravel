<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExcludeListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exclude_lists', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('agent_id')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->bigInteger('attribute_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exclude_lists');
    }
}
