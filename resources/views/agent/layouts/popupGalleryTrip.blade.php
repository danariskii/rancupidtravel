<!-- The Modal/Lightbox -->
<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <div class="modal-content">

    <?php
      $countThumbnailPopup = 1;
      $totalPhoto = count($photos);
    ?>

    @foreach ($photos as $photo )
      <div class="mySlides">
        <div class="numbertext">{{$countThumbnailPopup}} / {{$totalPhoto}}</div>
        <img src="{{ asset('/asset/app/public/product/').'/'.$product->id.'/'.$photo->filename }}" style="width:100%">
      </div>
      <?php $countThumbnailPopup = $countThumbnailPopup + 1; ?>
    @endforeach

    <!-- Next/previous controls -->
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <!-- Caption text -->
    <div class="caption-container">
      <p id="caption"></p>
    </div>

    <!-- Thumbnail image controls -->
    <?php
      $countPopup = 1;
    ?>

    @foreach ( $photos as $photo )
      <div class="column">
        <img class="demo" src="{{ asset('/asset/app/public/product/').'/'.$product->id.'/'.$photo->filename }}" onclick="currentSlide('{{$countPopup}}')" alt="Nature">
      </div>
      <?php $countPopup = $countPopup + 1; ?>
    @endforeach

  </div>
</div>