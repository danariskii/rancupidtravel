@extends('layouts.master')


@section('main-content')
<div class="row">

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'travel_identity', 'class' => 'form')) !!}

    <div class="col-md-8">
        <!-- general form elements disabled -->
        <div class="box box-info">
            <div class="box-header with-border">
            	<h3 class="box-title">Travel Agent Profile</h3>
            	<h3 class="box-title" style="float: right;" onclick="checkstatus('{{$Agent->status}}')">Status : 
            		@if($Agent->status == '1')<span class="label label-warning">Waiting Approval</span>
            		@elseif($Agent->status == '2')<span class="label label-success">Approved</span>
            		@elseif($Agent->status == '3')<span class="label label-danger">Banned</span>
            		@endif
            	</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

	                <!-- name trip -->
					<div class="form-group">
					    {!! Form::label('Travel Agent Name') !!}
					    {!! Form::text('travel_name', $Agent->travel_agent_name, array('required','class'=>'form-control', 'placeholder'=>'Travel Agent Name')) !!}
					</div>

	                <!-- telephone -->
					<div class="form-group">
					    {!! Form::label('Telephone') !!}
					    {!! Form::text('telephone', $Agent->telephone_agent, array( 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('Email') !!}
					    {!! Form::text('email', $Agent->email_agent, array('required','class'=>'form-control', 'placeholder'=>'Email')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('NPWP Number') !!}
					    {!! Form::text('npwp_number', $Agent->npwp_agent_number, array( 'class'=>'form-control', 'placeholder'=>'NPWP Number')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('Identity Card Number') !!}
					    {!! Form::text('identity_card_number', $Agent->identity_card_agent_number, array( 'class'=>'form-control', 'placeholder'=>'Identity Card Number')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('Office Address') !!}
					    {!! Form::text('office_address', $Agent->office_agent_address, array( 'class'=>'form-control', 'placeholder'=>'Office Address')) !!}
					</div>

					<div class="form-group">
						<input type="submit" name="save" value="Update" id="save">
					</div>

			</div>
		</div>
	</div>

{!! Form::close() !!}
</div>
@endsection