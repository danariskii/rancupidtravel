@extends('layouts.master')

@section('main-content')
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->

      				<div class="col-xs-8" style="margin-top: 1em;">
      					<table>
      					<!-- Default -->
      					<tr>
      						<td>
      						<div class="btn-group">
      							<button type="button" class="btn btn-default active">All</button>
      							<button type="button" class="btn label-success">Upcoming Trip</button>
      							<button type="button" class="btn label-info">Published Trip</button>
      							<button type="button" class="btn label-warning">Draft</button>
      							<button type="button" class="btn label-danger">Expired</button>
      						</div>
      						</td>
      					</tr>
      					<!-- /.warning -->
      					</table>
      				</div>

      				<div class="col-xs-4" style="right: 0; margin-top: 1em;">
      					<!-- <button type="button" class="btn btn-block btn-info">Add New Trip +</button> -->
                <a href="/addTrip" class="btn btn-block label-info">Add New Trip +</a>
      				</div>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <!-- <th>Travel Agent</th> -->
                    <th>Name Trip</th>
                    <th>Date of Trip</th>
                    <th>Type of Trip</th>
                    <th>Status</th>
                    <th>Number of Participants</th>
                    <th>Edit / View</th>
                  </tr>
                </thead>
                <tbody style="text-align:center;">
                @foreach ($Trips as $Trip)
                  <tr>
                    <td>{{ $Trip->title }}</td>
                    <?php 
                      $start_date = App\Product::find($Trip->id)->clostestTrip()->orderBy('start_trip_date', 'asc')->limit(1)->get();
                      $start_date_count = App\Product::find($Trip->id)->clostestTrip()->orderBy('start_trip_date', 'asc')->limit(1)->count();
                    ?>
                    @if ($start_date_count != "0")
                      <?php $start_date = date("d - m - Y", strtotime($start_date[0]->start_trip_date)); ?>
                      <td>{{ $start_date }}</td> <!-- "Y-m-d h:i:sa" -->
                    @else
                      <td> - </td> <!-- "Y-m-d h:i:sa" -->
                    @endif
                    
                    @if ( $Trip->type_trip == 1 )
                    <td>Open Trip</td>
                    @elseif ($Trip->type_trip == 2)
                    <td>Private Trip</td>
                    @else
                    <td> - </td>
                    @endif
                    
                    @if ( $Trip->status == 1)
                    <td><span class="label label-warning">Pending</span></td>
                    @elseif ( $Trip->status == 2 )
                    <td><span class="label label-primary">Ready Trip</span></td>
                    @elseif ( $Trip->status == 3 )
                    <td><span class="label label-success">On Going</span></td>
                    @elseif ( $Trip->status == 4 )
                    <td><span class="label label-danger">Expired</span></td>
                    @endif
                    
                    <td>0 People's</td>
                    <td class="row">
                      <a class="btn btn-block label-info col-md-5" href="/editTrip/{{$Trip->id}}" style="width: 50%; margin: 0;">edit</a>
                      <button class="btn btn-block label-danger col-md-5" onclick="deleteTrip('{{$Trip->id}}')" style="width: 50%; margin: 0;">delete</button>
                    </td>
                  </tr>
                @endforeach
                </tbody>
                <tfoot>
<!--                   <tr>
                    <th>Travel Agent</th>
                    <th>Name Trip</th>
                    <th>Date of Trip</th>
                    <th>Type of Trip</th>
                    <th>Status</th>
                    <th>Number of Participants</th>
                    <th>Edit / View</th>
                  </tr> -->
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
@endsection