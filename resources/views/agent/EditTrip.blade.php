@extends('layouts.master')


@section('main-content')
<div class="row">

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'edit_NewTrip', 'class' => 'form', 'files' => true, 'onsubmit' => 'validateFormSubmit()')) !!}

    <div class="col-md-8">
        <!-- general form elements disabled -->
        <div class="box box-info">
            <div class="box-header with-border">
            	<h3 class="box-title">Information Trip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <!-- name trip -->
				<div class="form-group">
				    {!! Form::label('Trip Title') !!}
				    {!! Form::text('Title', $product->title, array('required', 'class'=>'form-control', 'placeholder'=>'Trip Title')) !!}
				</div>

				<!-- trip deskripsi -->
				<div class="form-group">
				    {!! Form::label('Trip Description') !!}
				    {!! Form::textarea('Description', $product->text_5, array('required', 'class'=>'form-control tripDescription', 'placeholder'=>'Trip Description', 'size' => '20x4')) !!}
				</div>

				<!-- type trip -->
				<div class="form-group">
				    {!! Form::label('Type Trip') !!}
					<select class="form-control" style="width: 100%;" id="include_icon" required="required">
						<option selected="selected"></option>
						<option value="1">open trip</option>
						<option value="2">private trip</option>
					</select>
				</div>

				<!-- lokasi trip-->
				<div id="locationField" class="form-group" style="height:350px; padding-bottom: 10px;">
					{!! Form::label('Location Trip') !!}

				    <input id="pac-input" class="controls" type="text" placeholder="Enter a location" name="location_trip" required>
				    <div id="type-selector" class="controls">
				      <input type="radio" name="type" id="changetype-all" checked="checked">
				      <label for="changetype-all">All</label>
				    </div>
				    <div id="map"></div>

				      <!-- <input type="radio" name="type" id="changetype-establishment">
				      <label for="changetype-establishment">Establishments</label>

				      <input type="radio" name="type" id="changetype-address">
				      <label for="changetype-address">Addresses</label>

				      <input type="radio" name="type" id="changetype-geocode">
				      <label for="changetype-geocode">Geocodes</label> -->
				</div>

			</div>
		</div>

		<!-- Minimum section -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Minimun Quantity and Price</h3>
				<!-- <label style="float: right; text-align: center;">
					<input type="checkbox" class="flat-red">
					<h5>Advance Price</h5>
				</label> -->
			</div>
			<div class="box-body">
				<input type="hidden" name="price_list_number" id="price_list_number" value="1"></input>
				<table style="text-align: center;">
					<theead>
						<tr>
							<td class="header_quantityprice">No</td>
							<td class="header_quantityprice">
									<i class="fa fa-user"></i>
							</td>
							<td class="header_quantityprice">
									<i class="fa fa-group"></i>
							</td>
							<td class="header_quantityprice">
									<i class="fa fa-money"></i>
							</td>
							<td>Add</td>							
						</tr>
					</theead>
					<tbody id="price_list">
						<tr id="row_1">
							<td class="form-control">1</td>
							<td>
								<input class="form-control quantitymin" type="number" value="1" id="min_range_1" name="min_range_1" min="1" disabled="true">
							</td>
							<td>
								<input class="form-control quantitymax" type="number" value="2" id="max_range_1" name="max_range_1" min="2">
							</td>
							<td>
								<input type="text" value="0" id="price_1" class="price_trip form-control" required="required">
							</td>
							<td>
								<button  class="btn btn-block btn-primary addDetailPrice" type="button">Add Detail Price +</button>
							</td>
							<input type="hidden" name="ready_price_1" id="ready_price_1" value=""></input>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<!-- Photo section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Upload Destination Photo</h3>
			</div>
		    <div class="box-body pad">
		      <div class="form-group">
		      		<!-- accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" -->
					<input id=DestinationPhoto type="file" accept=".jpg, .JPG, .jpeg, .JPEG, .png ,.PNG" name="trip_destination[]" multiple="multiple">
		      </div>
		    </div>
                  <div class="progress-group" style="padding:0 1em 0.5em 1em;">
                    <span class="progress-text">Total Photo Size </span>
                    <span class="progress-number" id="countmaxsizephoto"><b>0</b>/35 mb</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-aqua" style="width: 0%; background-color: #00c0ef;" id="maxsizephoto"></div>
                    </div>
                  </div>
		</div>

		<!-- Itenerary section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Itenerary</h3>
			</div>
		    <div class="box-body pad">
		      <div class="form-group">
		            <textarea id="Itenerary" name="itenerary" rows="10" cols="80" required></textarea>
		      </div>
		    </div>
		</div>

		<!-- Include section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Include</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Include" name="include" rows="10" cols="80" required></textarea>
				</div>
		    </div>
		</div>

		<!-- Exclude section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Exclude</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Exclude" name="exclude" rows="10" cols="80" required></textarea>
				</div>
		    </div>
		</div>

		<!-- Destination section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Destination</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Destination" name="destination" rows="10" cols="80" required></textarea>
				</div>
		    </div>
		</div>

	</div>

	<!-- Date -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Date Trip</h3>
			</div>
			<div class="box-body">
				<input type="hidden" name="date_trip_number" id="date_trip_number" value="0"></input>
				<!-- Date and time range -->
				<div class="form-group">
					<label>Pick Start and End Date Trip</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</div>
						<input type="text" class="form-control pull-right" id="reservationtime" style="height: 40px;" required>
						<div class="input-group-addon" style="padding: 0; margin: 0;">
							<button  class="btn btn-block btn-primary" id="add_new_date" type="button" style="float: right;">+</button>
						</div>
					</div>
				</div>

				<div id = "date_list">
				</div>
			
			</div>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>

	<!-- include -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Include Icon</h3>
			</div>
			<div class="box-body pad">
				<input type="hidden" name="include_icon_list_number" id="include_icon_list_number" value="0"></input>
				<div class="form-group">
					<!-- <label>Include</label> -->
					<select class="form-control select2" style="width: 100%;" id="include_icon">
						<option selected="selected"></option>
						@foreach ($Attribute_list as $Attribute)
							<option value="{{$Attribute->id}}">{{ $Attribute->attribute}}</option>
						@endforeach
					</select>
				</div>

				<div id = "include_list">
				</div>

			</div>
		</div>
	</div>

	<!-- tag -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Tag</h3>
			</div>
			<div class="box-body pad">

						<div class="form-group">
							<!-- <label>Multiple</label> -->
							<select class="form-control select2" multiple="multiple" data-placeholder="Type your tag here" style="width: 100%;">
								<option>Hiking</option>
								<option>Snorkling</option>
								<option>Beach</option>
								<option>Island</option>
							</select>
						</div>

			</div>
		</div>
	</div>

	<!-- status -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body pad">

					<div class="form-group">
						<!-- <label>Include</label> -->
						<select class="form-control" name="statusTrip" style="width: 100%;">
							<option selected="selected" value="1">Draft</option>
							<option value="2">Publish</option>
						</select>
					</div>

					<div class="form-group">
						<button class="form-group btn btn-block btn-primary" id="submitTrip" type="submit">
							Save
						</button>
					</div>	

			</div>
		</div>
	</div>

{!! Form::close() !!}

</div>
@endsection