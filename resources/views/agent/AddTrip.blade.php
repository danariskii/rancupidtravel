@extends('layouts.master')


@section('main-content')
<div class="row">

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'add_NewTrip', 'class' => 'form', 'files' => true, 'onsubmit' => 'validateFormSubmit()')) !!}

    <div class="col-md-8">
        <!-- general form elements disabled -->
        <div class="box box-info">
            <div class="box-header with-border">
            	<h3 class="box-title">Information Trip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <!-- name trip -->
				<div class="form-group">
				    {!! Form::label('Trip Title') !!}
				    {!! Form::text('Title', isset($product->title)?$product->title:'' , array('required', 'class'=>'form-control', 'placeholder'=>'Trip Title')) !!}
					<div class="box-footer no-pad">
						<span><i class="fa fa-info-circle"></i> Your trip title</span>
					</div>
				</div>

				<!-- trip deskripsi -->
				<div class="form-group">
				    {!! Form::label('Trip Description') !!}
				    {!! Form::textarea('Description', isset($product->title)?$product->text_5:'' , array('required', 'class'=>'form-control tripDescription', 'placeholder'=>'Trip Description', 'size' => '20x4')) !!}
					<div class="box-footer no-pad">
						<span><i class="fa fa-info-circle"></i> Little description for your trip</span>
					</div>
				</div>

				<!-- type trip -->
				<div class="form-group">
				    {!! Form::label('Type Trip') !!}
					<select class="form-control" style="width: 100%;" required="required" name="type_trip">
						<option selected="selected"></option>
						@if( isset($product->type_trip) )
						<option value="1" @if(($product->type_trip == 1)) selected @endif >open trip</option>
						<option value="2" @if(($product->type_trip == 2)) selected @endif >private trip</option>
						@else
						<option value="1" >open trip</option>
						<option value="2" >private trip</option>
						@endif
					</select>
					<div class="box-footer no-pad">
						<span><i class="fa fa-info-circle"></i> Type for your trip</span>
					</div>
				</div>

				<!-- lokasi trip-->
				<div id="locationField" class="form-group" style="height:350px; padding-bottom: 10px;">
					{!! Form::label('Location Trip') !!}
					<input type="hidden" name="latitude_data" id="latitude_data" value="{{ isset($product->location_trip_latitude)?$product->location_trip_latitude:'' }}"></input>
					<input type="hidden" name="longitude_data" id="longitude_data" value="{{ isset($product->location_trip_longitude)?$product->location_trip_longitude:'' }}"></input>
				    <input id="pac-input" class="controls" type="text" placeholder="Enter a location" name="location_trip" 
				    required value="{{ isset($product->location_trip)?$product->location_trip:'' }}">
				    <div id="type-selector" class="controls">
				    	<input type="radio" name="type" id="changetype-all" checked="checked">
				    	<label for="changetype-all">All</label>
				    </div>
				    <div id="map"></div>
				</div>

				<!-- Meeting Poin-->
				<div id="locationField1" class="form-group" style="height:350px; padding-bottom: 10px; padding-top: 30px;">
					{!! Form::label('Meeting Poin') !!}
					<input type="hidden" name="latitude_data_mepo" id="latitude_data_mepo" value="{{ isset($product->meeting_point_latitude)?$product->meeting_point_latitude:'' }}"></input>
					<input type="hidden" name="longitude_data_mepo" id="longitude_data_mepo" value="{{ isset($product->meeting_point_longitude)?$product->meeting_point_longitude:'' }}"></input>
				    <input id="pac-inputMepo" class="controls" type="text" placeholder="Enter a location" name="meeting_point" 
				    required value="{{ isset($product->meeting_point)?$product->meeting_point:'' }}">
				    <div id="type-selectorMepo" class="controls">
				    	<input type="radio" name="type" id="changetype-all" checked="checked">
				    	<label for="changetype-all">All</label>
				    </div>
				    <div id="mapMePo"></div>
				</div>
			</div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for tagging your trip location</span>
			</div>
		</div>

		<!-- Minimum section -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Minimum Quantity and Price</h3>
				<!-- <label style="float: right; text-align: center;">
					<input type="checkbox" class="flat-red">
					<h5>Advance Price</h5>
				</label> -->
			</div>
			<div class="box-body">
				<table style="text-align: center;">
					<theead>
						<tr>
							<td class="header_quantityprice">No</td>
							<td class="header_quantityprice">
									<i class="fa fa-user"></i>
							</td>
							<td class="header_quantityprice">
									<i class="fa fa-group"></i>
							</td>
							<td class="header_quantityprice">
									<i class="fa fa-money"></i>
							</td>
							<td>Add</td>							
						</tr>
					</theead>
					<tbody id="price_list">
						@if( isset($product->id) )
							<?php
								$detail_prices = App\detail_price::where('product_id', $product->id)->get();
								$price_list_number = count($detail_prices);
								// $lastnumberquantity = $detail_prices[$price_list_number-1]->max_quantity;
							?>
							<input type="hidden" name="ready_id_todelete" id="ready_id_todelete" value=""></input>
							<!-- <input type="hidden" name="total_to_delete" id="total_to_delete" value="0"></input> -->
							@if( $price_list_number != null || $price_list_number != 0) 
							<input type="hidden" name="price_list_number" id="price_list_number" value="{{ isset($price_list_number)?$price_list_number:'1' }}"></input>
								@foreach( $detail_prices as $detail_price )
									<tr id="row_{{ $detail_price->price_order }}">
										<input type="hidden" name="ready_id_{{ $detail_price->price_order }}" id="ready_id_{{ $detail_price->price_order }}" value="{{ $detail_price->id }}"></input>
										<td class="form-control">{{ $detail_price->price_order }}</td>
										<td>
											<input class="form-control quantitymin" type="number" value="{{ $detail_price->min_quantity }}" id="min_range_{{$detail_price->price_order}}" name="min_range_{{$detail_price->price_order}}" min="1" readonly="true">
										</td>
										<td>
											<input class="form-control quantitymax" type="number" value="{{ $detail_price->max_quantity }}" id="max_range_{{$detail_price->price_order}}" name="max_range_{{$detail_price->price_order}}" min="2">
										</td>
										<td>
											<input type="text" value="{{ $detail_price->price }}" id="price_{{$detail_price->price_order}}" class="price_trip form-control" required="required">
										</td>
										<td>
											@if( $detail_price->price_order == 1 )
												<button  class="btn btn-block btn-primary addDetailPrice" type="button">Add Detail Price +</button>
											@elseif( $detail_price->price_order == $price_list_number )
												<button type="button" name="remove" id="{{ $detail_price->price_order }}" class="btn btn-danger btn_remove_pl remove_quantity">X</button>
											@else
												<button type="button" name="remove" id="{{ $detail_price->price_order }}" class="btn btn-danger btn_remove_pl remove_quantity" disabled>X</button>
											@endif
										</td>
										<input type="hidden" name="ready_price_{{$detail_price->price_order}}" id="ready_price_{{$detail_price->price_order}}" value="{{ $detail_price->price }}"></input>
									</tr>
								@endforeach
							@else
								<input type="hidden" name="price_list_number" id="price_list_number" value="{{ isset($price_list_number)?'1':'1' }}"></input>					
								<tr id="row_1">
									<input type="hidden" name="ready_id_1" id="ready_id_1" value=""></input>
									<td class="form-control">1</td>
									<td>
										<input class="form-control quantitymin" type="number" value="1" id="min_range_1" name="min_range_1" min="1" readonly="true">
									</td>
									<td>
										<input class="form-control quantitymax" type="number" value="2" id="max_range_1" name="max_range_1" min="2">
									</td>
									<td>
										<input type="text" value="0" id="price_1" class="price_trip form-control" required="required">
									</td>
									<td>
										<button  class="btn btn-block btn-primary addDetailPrice" type="button">Add Detail Price + </button>
									</td>
									<input type="hidden" name="ready_price_1" id="ready_price_1" value=""></input>
									<!-- <input type="hidden" name="lastnumberquantity" id="lastnumberquantity" value=" $lastnumberquantity }}"></input> -->
								</tr>
							@endif
						@else
							<input type="hidden" name="price_list_number" id="price_list_number" value="1"></input>
							<input type="hidden" name="ready_id_todelete" id="ready_id_todelete" value=""></input>
							<tr id="row_1">
								<input type="hidden" name="ready_id_1" id="ready_id_1" value=""></input>
								<td class="form-control">1</td>
								<td>
									<input class="form-control quantitymin" type="number" value="1" id="min_range_1" name="min_range_1" min="1" readonly="true">
								</td>
								<td>
									<input class="form-control quantitymax" type="number" value="2" id="max_range_1" name="max_range_1" min="2">
								</td>
								<td>
									<input type="text" value="0" id="price_1" class="price_trip form-control" required="required">
								</td>
								<td>
									<button  class="btn btn-block btn-primary addDetailPrice" type="button">Add Detail Price +</button>
								</td>
								<input type="hidden" name="ready_price_1" id="ready_price_1" value=""></input>
								<!-- <input type="hidden" name="lastnumberquantity" id="lastnumberquantity" value=" $lastnumberquantity }}"></input> -->
							</tr>
						@endif
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<span><i class="fa fa-user"></i> Minimum Quantity</span> &ensp;&ensp;
				<span><i class="fa fa-group"></i> Maximum Quantity</span> &ensp;&ensp;
				<span><i class="fa fa-money"></i> Quantity Price</span>
			</div>
		</div>

		<!-- Itenerary section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Itenerary</h3>
			</div>
		    <div class="box-body pad">
		      <div class="form-group">
					{!! Form::textarea('Itenerary', isset($product->text_1)?$product->text_1:'' , array('required', 'class'=>'form-control', 'placeholder'=>'Itenerary Trip', 'size' => '80x10')) !!}
		      </div>
		    </div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for details schedule in this trip</span>
			</div>
		</div>

		<!-- Include section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Include</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Include" name="Include" rows="10" cols="80" required>{{ isset($product->text_2)?$product->text_2:'' }}</textarea>
				</div>
		    </div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for details things include in this trip</span>
			</div>
		</div>

		<!-- Exclude section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Exclude</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Exclude" name="Exclude" rows="10" cols="80" required>{{ isset($product->text_3)?$product->text_3:'' }}</textarea>
				</div>
		    </div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i>  This section for details things exclude <i class="important_info">(not include)</i> in this trip</span>
			</div>
		</div>

		<!-- Destination section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Destination</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Destination" name="Destination" rows="10" cols="80" required>{{ isset($product->text_4)?$product->text_4:'' }}</textarea>
				</div>
		    </div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for destination details will customer visit in this trip</span>
			</div>
		</div>

	</div>

	<!-- Date -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Date Trip</h3>
			</div>
			<div class="box-body">
				<!-- Date and time range -->
				<div class="form-group">
					<label>Pick Start and End Date Trip</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</div>
						<input type="text" class="form-control pull-right" id="reservationtime" style="height: 40px;">
						<div class="input-group-addon" style="padding: 0; margin: 0;">
							<button  class="btn btn-block btn-primary" id="add_new_date" type="button" style="float: right;">+</button>
						</div>
					</div>
				</div>

				<div id = "date_list">
					@if( isset($product->id) )
						<?php
							$trip_dates = App\TripDate::where('product_id', $product->id)->get();
							$trip_dates_total = count($trip_dates);
							$count_dates = null;
						?>
						@if( $trip_dates != null )
							@foreach( $trip_dates as $trip_date )
								<?php $count_dates = $count_dates + 1; ?>
								<div class="alert alert-success alert-dismissible" id="date_trip_{{ $count_dates }}">
									<button type ="button" id="{{ $count_dates }}" class="close btn_remove_dt" data-dismiss="alert" 
									aria-hidden="true" style="color: white; opacity:1">
									&times;</button>
									<?php
										$start_trip_date = date("d/m/Y", strtotime($trip_date->start_trip_date)); 
										$end_trip_date = date("d/m/Y", strtotime($trip_date->end_trip_date)); 
									?>
									{{ $start_trip_date }} - {{ $end_trip_date }}
									<input type="hidden" name="start_trip_{{ $count_dates }}" id="start_trip_{{ $count_dates }}" value="{{ $start_trip_date }}"></input>
									<input type="hidden" name="end_trip_{{ $count_dates }}" id="end_trip_{{ $count_dates }}" value="{{ $end_trip_date }}"></input>
									<input type="hidden" name="ready_id_date_{{$count_dates}}" id="ready_id_date_{{ $count_dates }}" value="{{ $trip_date->id }}">
								</div>
							@endforeach
						@endif
					@endif
				</div>
				<input type="hidden" name="date_trip_number" id="date_trip_number" value="{{ isset($trip_dates_total)?$trip_dates_total:'' }}"></input>
				<input type="hidden" name="ready_id_date_todelete" id="ready_id_date_todelete" value=""></input>
			</div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for list date this trip</span>
			</div>
		</div>
		<!-- /.box -->
	</div>

	<!-- include icon-->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Include Icon</h3>
			</div>
			<div class="box-body pad">
				<div class="form-group">
					<!-- <label>Include</label> -->
					<select class="form-control select2" style="width: 100%;" id="include_icon">
						<option selected="selected"></option>
						@foreach ($Attribute_list as $Attribute)
							<option value="{{$Attribute->id}}" data-icon="{{ asset('/storage/assets/Include_Icon/').'/'.$Attribute->filename }}">
								{{ $Attribute->attribute }}
							</option>
						@endforeach
					</select>
				</div>

				<div id = "include_list">
					@if( isset($product->id) )
						<?php
							$Include_list = App\Include_list::where('product_id', $product->id)->get();
							$Include_list_total = App\Include_list::where('product_id', $product->id)->count();
							$count_include = null;
							$ready_part_icon_towrite = null;
							for ($i=1; $i <= $Include_list_total; $i++) 
							{ 
								if ($i == 1)
								{
									$ready_part_icon_towrite = $i;
								}
								else
								{
									$ready_part_icon_towrite = $ready_part_icon_towrite .','.$i;
								}
							}
						?>
						<input type="hidden" name="include_icon_list_number" id="include_icon_list_number" value="{{ isset($Include_list_total)?$Include_list_total:'' }}"></input>
						<input type="hidden" name="include_icon_id_todelete" id="include_icon_id_todelete" value=""></input>
						<input type="hidden" name="ready_part_icon_towrite" id="ready_part_icon_towrite" value="{{ isset($ready_part_icon_towrite)?$ready_part_icon_towrite:'' }}"></input>
						@if( $Include_list != null )
							@foreach( $Include_list as $Include )
								<?php
									$count_include = $count_include + 1;
									$Icon = App\Attribute_list::where('id', $Include->attribute_id)->first();
								?>
								<div class="alert alert-info alert-dismissible" id="include_{{$count_include}}">
									<button type ="button" id="{{$count_include}}" class="close btn_remove_ii" data-dismiss="alert" aria-hidden="true" style="color: white; opacity:1" >&times;</button>
									<img src="/storage/assets/Include_Icon/{{ $Icon->filename }}" style="width: 20%;">
									{{ $Icon->attribute }}
									<input type="hidden" name="icon_id_{{ $count_include }}" id="icon_id_{{ $count_include }}" value="{{ $Icon->id }}"></input>
									<input type="hidden" name="include_id_{{ $count_include }}" id="include_id_{{ $count_include }}" value="{{ $Include->id }}"></input>
								</div>
							@endforeach
						@endif
					@else
						<input type="hidden" name="include_icon_list_number" id="include_icon_list_number" value=""></input>
						<input type="hidden" name="include_icon_id_todelete" id="include_icon_id_todelete" value=""></input>
						<input type="hidden" name="ready_part_icon_towrite" id="ready_part_icon_towrite" value=""></input>
					@endif
				</div>
			</div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for manage icon that will appear in your trip</span>
			</div>
		</div>
	</div>

	<!-- tag -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Tag</h3>
			</div>
			<div class="box-body pad">
				<?php $tags = App\Tags::all(); ?>
				<div class="form-group">
					<!-- <label for="my-select">Tag</label> -->
					<select id="tag" class="form-control" multiple="multiple" name="tags[]">
					@foreach ( $tags as $tag )
						<option value="{{ $tag->id }}">{{ $tag->title }}</option>
					@endforeach
					</select>
				</div>
			</div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for tagging your trip</span>
			</div>
		</div>
	</div>

	<!-- status -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body pad">
					<div class="form-group">
						<!-- <label>Include</label> -->
						<select class="form-control" name="statusTrip" style="width: 100%;">
							@if( isset($product->status) )
								<option value="1" @if(($product->status == 1)) selected @endif >Draft</option>
								<option value="2" @if(($product->status == 2)) selected @endif >Publish</option>
							@else
								<option selected="selected" value="1">Draft</option>
								<option value="2">Publish</option>
							@endif
							
						</select>
					</div>
					<div class="form-group">
						<input type="hidden" name="id_product_edit" id="id_product_edit" value="{{ isset($product->id)?$product->id:'baru' }}"></input>
						<input type="hidden" name="temp_id" value="{{ isset($temp_id)?$temp_id:'' }}"></input>
						<input type="hidden" name="ready_id_photo_todelete" id="ready_id_photo_todelete" value=""></input>
						<input type="hidden" name="total_size_photo" id="total_size_photo" value="{{ isset($product->total_size_photo)?$product->total_size_photo:'0' }}"></input>
						<input type="hidden" name="edit_boolean" id="edit_boolean" value="{{ isset($product->id)?'true':'false' }}"></input>
						<input type="hidden" class="sticky_id" name="sticky_id" id="sticky_id" value="{{ isset($sticky->id)?$sticky->id:'false' }}"></input>
						<button class="form-group btn btn-block btn-primary" id="submitTrip" type="submit">
							Save
						</button>
					</div>	
			</div>
			<div class="box-footer">
				<span><i class="fa fa-info-circle"></i> This section for status your trip,</span><br>
				<span>Draft : trip saved, but not published.</span><br>
				<span>Publish : trip saved and publish to website</span>
			</div>
		</div>
	</div>

{!! Form::close() !!}

		<!-- Photo section -->
	<div class="col-md-8">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Upload Destination Photo<span id="photoCounter"></span></h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		      		<!-- accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" -->
					<!-- <input id=DestinationPhoto type="file" accept=".jpg, .JPG, .jpeg, .JPEG, .png ,.PNG" name="trip_destination[]" multiple="multiple"> -->

					{!! Form::open(array('route' => 'photo_trip_temp', 'class' => 'form dropzone', 'id' => 'myDropzoneTripPhoto', 'files' => true, 'enctype' => 'multipart/form-data')) !!}
						<input type="hidden" name="temp_photo_id" id="temp_photo_id" value="{{ $temp_id }}"></input>
						{{ csrf_field() }}
		
						<div class="dz-message"></div>

		                <div class="fallback">
		                    <input name="file" type="file" multiple />
		                </div>

		                <div class="dropzone-previews" id="dropzonePreview"></div>

		                <h4 style="text-align: center;color:#428bca;">Drop images in this area  <span class="glyphicon glyphicon-hand-down"></span></h4>

					{!! Form::close() !!}
				</div>
		    </div>
			<div class="progress-group" style="padding:0 1em 0.5em 1em;">
				<?php
					if( isset($product->total_size_photo) )
					{
						$sizePhoto = $product->total_size_photo;
						$sizePhotostatus = $sizePhoto / 1000000;
        				$sizePhotostatus = round($sizePhotostatus, 2);
						$sizePhoto = ($sizePhoto / 35000000) * 100;
					}
				?>
				<span class="progress-text">Total Photo Size </span>
				<span class="progress-number" id="countmaxsizephoto"><b>{{ isset($product->total_size_photo)?$sizePhotostatus:'0' }}</b>/35 mb</span>				
				<div class="progress sm">
					<div class="progress-bar progress-bar-aqua" 
					style="width: {{ isset($product->total_size_photo)?$sizePhoto:'0' }}%; background-color: #00c0ef;" id="maxsizephoto"></div>
				</div>
			</div>
			<?php
					$countphotos = null;
				if( isset($product->id) )
				{	
					$photos = App\Product::find($product->id)->tripPhoto()->get();
					$countphotos = App\Product::find($product->id)->tripPhoto()->count();
				}
			?>
			<!-- habis buat tombol delete dan mark di dropzone, buat opacity checkbox ,// remove total size -->
<style type="text/css">
	input[type="checkbox"] {
	    position: absolute;
	    opacity: 0;
	}
	.checkmark {
	    position: absolute;
	    top: 0;
	    left: 0;
	    height: 25px;
	    width: 25px;
	    background-color: #eee;
	}

	/* On mouse-over, add a grey background color */
	.mark-photo:hover input ~ .checkmark {
	    background-color: #ccc;
	}

	/* When the checkbox is checked, add a blue background */
	.mark-photo input:checked ~ .checkmark {
	    background-color: #2196F3;
	    border: 0.5px white dashed;
	}

	/* Create the checkmark/indicator (hidden when not checked) ✔*/
	.checkmark:after {
	    content: "";
	    position: absolute;
	    display: none;
	}

	/* Show the checkmark when checked */
	.mark-photo input:checked ~ .checkmark:after {
	    display: block;
	}

	/* Style the checkmark/indicator */
	.mark-photo .checkmark:after {
	    left: 9px;
	    top: 3px;
	    width: 7px;
	    height: 15px;
	    border: solid white;
	    border-width: 0 3px 3px 0;
	    -webkit-transform: rotate(45deg);
	    -ms-transform: rotate(45deg);
	    transform: rotate(45deg);
	}
</style>

<div class="gallery">
<div>
	<h4>Uploaded Photo</h4>
@if( $countphotos != 0 && $countphotos != null )	
	<label class="switch">
		<input type="checkbox" name="edit[]" id="editGallery" onclick="toggleEditGallery()">
		<span class="slider round"></span>
	</label>
	<label style="vertical-align: top;">Edit Gallery</label>
@endif
</div>
@if( $countphotos != 0 && $countphotos != null )
	<input type="hidden" name="PhotosNumber" id="PhotosNumber" value="{{ isset($countphotos)?$countphotos:'' }}"></input>
	@foreach ($photos as $photo )
	<div class="img-w" id="photo_id_{{ $photo->id }}">
		<div class="photo-action">
			<div class="mark-photo">
				<input type="checkbox" class="triggersticky" {{isset($photo->sticky)?'checked':''}}>
				<span class="checkmark" id="{{ $photo->id }}"></span>				
			</div>
			<button type="button" class="btn btn-danger fa fa-times-circle removeUploadedPhotoBtn" id="removeUploadedPhoto" 
			onclick="deletePhoto( '{{ $photo->id }}' , '{{ isset($product->id)?$product->id:''}}' , '{{ $photo->filename }}' )" 
			style="padding: 5px 6px;"></button>
		</div>
		<img src="{{ asset('/storage/product/').'/'.$product->id.'/'.$photo->filename }}" alt="">
	</div>
	@endforeach
@else
	<input type="hidden" name="PhotosNumber" id="PhotosNumber" value=""></input>
	<div class="no-photo">
		<span>no photos uploaded yet</span>
	</div>
@endif
</div>


		</div>
	</div>



</div>
@endsection