@extends('layouts.master')


@section('main-content')
<div class="row">

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'travel_identity', 'class' => 'form')) !!}

    <div class="col-md-8">
        <!-- general form elements disabled -->
        <div class="box box-info">
            <div class="box-header with-border">
            	<h3 class="box-title">Travel Agent Profile</h3>
            	<h3 class="box-title" style="float: right;">Status :Unregistered</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

					<div class="form-group">
					    {!! Form::label('Travel Agent Name') !!}
					    {!! Form::text('travel_name', null, array('required','class'=>'form-control', 'placeholder'=>'Travel Agent Name')) !!}
					</div>

	                <!-- telephone -->
					<div class="form-group">
					    {!! Form::label('Telephone') !!}
					    {!! Form::text('telephone', null, array( 'class'=>'form-control', 'placeholder'=>'Telephone')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('Email') !!}
					    {!! Form::text('email', null, array('required','class'=>'form-control', 'placeholder'=>'Email')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('NPWP Number') !!}
					    {!! Form::text('npwp_number', null, array( 'class'=>'form-control', 'placeholder'=>'NPWP Number')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('Identity Card Number') !!}
					    {!! Form::text('identity_card_number', null, array( 'class'=>'form-control', 'placeholder'=>'Identity Card Number')) !!}
					</div>

					<div class="form-group">
					    {!! Form::label('Office Address') !!}
					    {!! Form::text('office_address', null, array( 'class'=>'form-control', 'placeholder'=>'Office Address')) !!}
					</div>

					<div class="form-group">
						<input type="submit" name="save" value="save" id="save">
					</div>

			</div>
		</div>
	</div>

{!! Form::close() !!}
</div>
@endsection