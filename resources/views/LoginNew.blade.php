@extends('layouts.master-frontend')

@section('main-content')
<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
<div class="container" style="margin-top: 2em;">
	<div class="login-fullpage">
		<div class="row">
			<div class="login-logo"><img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_2.JPG') }}" alt=""></div>
			<div class="col-xs-12 col-sm-7">
				<div class="f-login-content">
					<div class="f-login-header">
						<div class="f-login-title color-dr-blue-2">Welcome back!</div>
						<div class="f-login-desc color-grey">Please login to your account</div>
					</div>

					<form class="f-login-form">
						<div class="input-style-1 b-50 type-2 color-5">
							<input type="text" placeholder="Enter your email or username" required>
						</div>
						<div class="input-style-1 b-50 type-2 color-5">
							<input type="text" placeholder="Enter your password" required>
						</div>
						<div class="checkbox-group">
							<div class="input-entry color-3">
								<input class="checkbox-form" id="text-1" type="checkbox" name="checkbox" value="climat control">
								<label class="clearfix" for="text-1">
								<span class="sp-check"><i class="fa fa-check"></i></span>
								<span class="checkbox-text">Register now</span>
								</label>
							</div>
							<div class="input-entry color-3">
								<input class="checkbox-form" id="text-2" type="checkbox" name="checkbox" value="climat control">
								<label class="clearfix" for="text-2">
								<span class="sp-check"><i class="fa fa-check"></i></span>
								<span class="checkbox-text">Remember me</span>
								</label>
							</div>
						</div>
						<input type="submit" class="login-btn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o" value="LOGIN TO YOUR ACCOUNT">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<a href="#" class="c-button full b-60 bg-blue-7 hv-blue-7-o"><i class="fa fa-facebook"></i><span>login via facebook</span></a>
							</div>
							<div class="col-xs-12 col-sm-6">
								<a href="#" class="c-button full b-60 bg-blue-8 hv-blue-8-o"><i class="fa fa-twitter"></i><span>login via facebook</span></a>
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
	<div class="full-copy">© 2017 All rights reserved. Rancupid's Travel</div>
</div>
@endsection