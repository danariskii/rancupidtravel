@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('First_Name') ? ' has-error' : '' }}">
                            <label for="First_Name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="First_Name" type="text" class="form-control" name="First_Name" value="{{ old('First_Name') }}" required autofocus>

                                @if ($errors->has('First_Name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('First_Name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('Last_Name') ? ' has-error' : '' }}">
                            <label for="Last_Name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="Last_Name" type="text" class="form-control" name="Last_Name" value="{{ old('Last_Name') }}" required autofocus>

                                @if ($errors->has('Last_Name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Last_Name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="gender" class="col-md-4 control-label">Gender</label>

                            <div class="col-md-6">
                                {{ Form::radio('gender', '1') }}Male<br>
                                {{ Form::radio('gender', '2') }}Female

                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                    <input type="text" value="" class=" form-control pull-right" id="datepicker" name="dob" value="{{ old('dob') }}" required>
                                  </div>
                                  <!-- <input type="text" class="form-control pull-right" id="datepicker" name="dob" value="{{ old('dob') }}" required> -->
                                  <!-- <input type="hidden" name="readydob" id="alt-datepicker"> -->
                                  <!-- <p>Alt Date: <input id="alt-datepicker" type="text"></p> -->
                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('Handphone') ? ' has-error' : '' }}">
                            <label for="Handphone" class="col-md-4 control-label">Handphone</label>

                            <div class="col-md-6">
                                <input id="Handphone" type="text" class="form-control" name="Handphone" value="{{ old('Handphone') }}" required autofocus>

                                @if ($errors->has('Handphone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Handphone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
