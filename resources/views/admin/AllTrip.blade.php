@extends('layouts.master')

@section('main-content')
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->

      				<div class="col-xs-8" style="margin-top: 1em;">
      					<table>
      					<!-- Default -->
      					<tr>
      						<td>
      						<div class="btn-group">
      							<button type="button" class="btn btn-default active">All</button>
      							<button type="button" class="btn label-success">On Going Trip</button>
      							<button type="button" class="btn label-info">Ready Trip</button>
      							<button type="button" class="btn label-warning">Pending</button>
      							<button type="button" class="btn label-danger">Used</button>
      						</div>
      						</td>
      					</tr>
      					<!-- /.warning -->
      					</table>
      				</div>

      				<div class="col-xs-4" style="right: 0; margin-top: 1em;">
      					<!-- <button type="button" class="btn btn-block btn-info">Add New Trip +</button> -->
                <a href="/addTripAdmin" class="btn btn-block label-info">Add New Trip +</a>
      				</div>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover" style="text-align: center;">
                <thead>
                  <tr>
                    <th>Travel Agent</th>
                    <th>Name Trip</th>
                    <th>Date of Trip</th>
                    <th>Type of Trip</th>
                    <th>Status</th>
                    <th>Number of Participants</th>
                    <th>View</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($Trips as $Trip)
                  <tr>
                    <td>{{ $Trip->Agent_Name }}</td>
                    <td>{{ $Trip->title }}</td>
                    <td>{{ $Trip->start_date }}</td> <!-- "Y-m-d h:i:sa" -->
                    
                    @if ( $Trip->type_trip == 1 )
                    <td>Open Trip</td>
                    @elseif ($Trip->type_trip == 2)
                    <td>Private Trip</td>
                    @endif
                    
                    @if ( $Trip->status == 1)
                    <td><span class="label label-warning">Pending</span></td>
                    @elseif ( $Trip->status == 2 )
                    <td><span class="label label-primary">Ready Trip</span></td>
                    @elseif ( $Trip->status == 3 )
                    <td><span class="label label-success">On Going</span></td>
                    @elseif ( $Trip->status == 4 )
                    <td><span class="label label-danger">Expired</span></td>
                    @endif
                    
                    <td>0 People's</td>
                    <td><button>View Detail</button></td>
                  </tr>
                @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>Travel Agent</th>
                    <th>Name Trip</th>
                    <th>Date of Trip</th>
                    <th>Type of Trip</th>
                    <th>Status</th>
                    <th>Number of Participants</th>
                    <th>View</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
@endsection