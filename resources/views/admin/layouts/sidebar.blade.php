  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->username }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li @if($select == 'Dashboard') class="active" @endif>
          <a href="/adminRancupid">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li @if($select == 'TravelAgentProfile') class="active" @endif>
          <a href="/travelAgentProfile">
            <i class="fa fa-id-card-o"></i> <span>Travel Agent Profile</span>
          </a>
        </li>

        <!-- Trip -->
        <li @if($select == 'Recommendations' || $select == 'Includes') class="active treeview" @else class="treeview" @endif>
          <a href="#">
            <i class="fa fa-chain"></i> <span>Attributes</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if($select == 'Recommendations') class="active" @endif>  
                <a href="/recommendations">
                    <i class="fa fa-rocket"></i> Recommendations 
                </a>
            </li>
            <li @if($select == 'Includes') class="active" @endif>
                <a href="/includes">
                    <i class="fa fa-modx"></i> Includes 
                </a>
            </li>
          </ul>
        </li>

        <!-- Trip -->
        <li @if($select == 'AllTrip' || $select == 'AddTrip') class="active treeview" @else class="treeview" @endif>
          <a href="#">
            <i class="fa fa-suitcase"></i> <span>Trip</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li @if($select == 'AddTrip') class="active" @endif>  
                <a href="/addTripAdmin">
                    <i class="fa fa-plus-circle"></i> Add Trip 
                </a>
            </li>
            <li @if($select == 'AllTrip') class="active" @endif>
                <a href="/allTripAdmin">
                    <i class="fa fa-suitcase"></i> See All Trip 
                </a>
            </li>
          </ul>
        </li>

        <li @if($select == 'AllUser') class="active" @endif>
          <a href="/seeAllUser">
            <i class="fa fa-group"></i>
            <span>See All User</span>
            <span class="pull-right-container">
              @if( $AgentNew != 0 )
              <span class="label pull-right bg-green">{{$AgentNew}}</span>
              @else
              <span class="label pull-right">{{$JumlahUser}}</span>
              @endif
            </span>
          </a>
        </li>

        <li>
          <a href="#">
            <i class="fa fa-comment-o"></i>
            <span>Directly Message</span>
            <span class="pull-right-container">
              <span class="label pull-right bg-green">4</span>
            </span>
          </a>
        </li>

        <li>
          <a href="#">
            <i class="fa fa-comments-o"></i>
            <span>Discussion Product</span>
            <span class="pull-right-container">
              <span class="label pull-right bg-red">4</span>
            </span>
          </a>
        </li>

        <li>
          <a href="#">
            <i class="fa fa-star-o"></i>
            <span>Review Product</span>
            <span class="pull-right-container">
              <span class="label pull-right bg-blue">4</span>
            </span>
          </a>
        </li>

        <li>
          <a href="#">
            <i class="fa fa-thumbs-o-up"></i>
            <span>Review Agent</span>
            <span class="pull-right-container">
              <span class="label pull-right bg-yellow">4</span>
            </span>
          </a>
        </li>

        <!-- <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>