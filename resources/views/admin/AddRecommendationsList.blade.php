@extends('layouts.master')

@section('main-content')
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
              {!! Form::open(array('route' => 'add_NewTrip', 'class' => 'form')) !!}
        				<div class="col-xs-4" style="margin-top: 1em;">
                  <div class="form-group">
                    <label style="margin-right: 1em;">Name List</label>
                    <input type="text" name="list_name" style="width: 100%;">                    
                  </div>
                </div>
                  
                <div class="col-xs-4" style="margin-top: 1em;">
                  <div class="form-group">
                    <label>Type Item</label>
                    <select class="form-control" style="width: 100%;" id="TypeItemRec">
                        <option selected="selected"></option>
                        <option value="Trip">Trip (system) </option>
                        <option value="Agent">Travel Agent (system)</option>
                        <option value="Destination">Destination (system)</option>
                        <option value="Trip">Trip</option>
                        <option value="Agent">Travel Agent</option>
                        <option value="Destination">Destination</option>
                    </select>
                  </div>
        				</div>

                <div class="col-xs-4" style="margin-top: 2.9em;">
                  <div class="form-group">
                    <!-- <label>Type Item</label> -->
                    <a href="#" class="btn btn-block label-info">Add Item +</a>                    
                  </div>
                </div>

                <div class="col-xs-8">
                  <div class="form-group">
                    <label>Item</label>
                    <select class="form-control select2" style="width: 100%;" id="Item">
                        <option selected="selected"></option>
                    </select>
                  </div>
                </div>

        				<div class="col-xs-4" style="right: 0; margin-top: 1.9em;">
                  <a href="#" class="btn btn-block label-info">Save</a>
        				</div>
              {!! Form::close() !!}
            </div>
            
            <hr>
            
            <!-- /.box-header -->
            <div class="box-body">
              <input type="hidden" name="number_of_list_item" id="number_of_list_item" value="0"></input>
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Travel Agent</th>
                    <th>Name Trip</th>
                    <th>Date of Trip</th>
                    <th>Type of Trip</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($recommendations as $recommendation)
                  <tr>
                    <td>1</td>
                    <td>{{ $recommendation->list_name }}</td>
                    <td>{{ $recommendation->list_type }}</td>
                    <td>{{ $recommendation->created_by }}</td>
                    <td>{{ $recommendation->last_edited_by }}</td>
                    <td>jumlah</td>
                    <td><button>Edit</button></td>                    
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Travel Agent</th>
                    <th>Name Trip</th>
                    <th>Date of Trip</th>
                    <th>Type of Trip</th>
                    <th>Status</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
@endsection