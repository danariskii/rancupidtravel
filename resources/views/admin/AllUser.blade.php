@extends('layouts.master')

@section('main-content')
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <!-- <h3 class="box-title">Data Table With Full Features</h3> -->

      				<div class="col-xs-8" style="margin-top: 1em;">
      					<table>
      					<tr>
      						<td>
      						<div class="btn-group">
      							<button type="button" class="btn btn-default active">All</button>
                    <button type="button" class="btn label-success">User</button>
                    <button type="button" class="btn label-info">Agent</button>
      							<button type="button" class="btn label-warning">Pending Agent</button>
      							<!-- <button type="button" class="btn label-danger">Used</button> -->
      						</div>
      						</td>
      					</tr>
      					</table>
      				</div>

      				<div class="col-xs-4" style="right: 0; margin-top: 1em;">
      					<!-- <button type="button" class="btn btn-block btn-info">Add New Trip +</button> -->
                <a href="/addTripAdmin" class="btn btn-block label-info">Add New Trip +</a>
      				</div>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Name User</th>
                    <!-- <th>Sign Date</th> -->
                    <th>Last Active</th>
                    <th>Total Order</th>
                    <th>Travel Agent Name</th>
                    <th>Travel Agent Status</th>
                    <th>Number Trip</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($Users as $User)
                  <tr>
                    <td>1</td>
                    <td>{{ $User->username}}</td>
                    <td>{{ $User->updated_at }}</td> <!-- "Y-m-d h:i:sa" -->
                    <td>0</td>
                    <td>{{ isset($User->travel_agent_name)?$User->travel_agent_name:'-' }}</td>

                    <td>
                      @if($User->status != null)
                      <div id="status">
                        @if($User->status == '1')
                        <span class="label label-warning">Waiting Approval</span>
                        @elseif($User->status == '2')
                        <span class="label label-success">Approved</span>
                        @elseif($User->status == '3')
                        <span class="label label-danger">Banned</span>
                        @endif
                      </div>
                      <div class="btn-group">
                        <!-- <button type="button" class="btn btn-info">View Profile</button> -->
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                          <span class="caret"></span>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                          <li @if($User->status == '1') class="active" @endif>
                              <a onclick="updatestatusAgent('{{$User->id}}','1')">Pending</a>
                          </li>
                          <li @if($User->status == '2') class="active" @endif>
                            <a onclick="updatestatusAgent('{{$User->id}}','2')">Approve</a></li>
                          <li class="divider"></li>
                          <li @if($User->status == '3') class="active" @endif>
                            <a onclick="updatestatusAgent('{{$User->id}}','3')">Banned</a></li>
                        </ul>
                      </div>
                      @else
                      -
                      @endif
                    </td>

                    <td>
                      0
                    </td>
                    <!-- <td>X</td> -->
                  </tr>
                  @endforeach
                    <!-- 
                    <td><span class="label label-primary">Ready Trip</span></td>
                    <td><span class="label label-danger">Used</span></td> -->
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Name User</th>
                    <!-- <th>Sign Date</th> -->
                    <th>Last Active</th>
                    <th>Total Order</th>
                    <th>Travel Agent Name</th>
                    <th>Travel Agent Status</th>
                    <th>Number Trip</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
@endsection