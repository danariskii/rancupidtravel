@extends('layouts.master')

@section('main-content')
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <div class="col-xs-8" style="margin-top: 1em;">
                <h3 class="box-title">All Recommendation List</h3>
              </div>
              <div class="col-xs-4" style="right: 0; margin-top: 1em;">
                <a href="/addRecommendationsList" class="btn btn-block label-info">Add New Recommendation List +</a>
              </div>
            </div>
            
            <hr>
            
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped table-hover">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>List Name</th>
                    <th>List Type</th>
                    <th>Created By</th>
                    <th>Last Edited By</th>
                    <th>Number of Item</th>
                    <th>View / Edit</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($recommendations as $recommendation)
                  <tr>
                    <td>1</td>
                    <td>{{ $recommendation->list_name }}</td>
                    <td>{{ $recommendation->list_type }}</td>
                    <td>{{ $recommendation->created_by }}</td>
                    <td>{{ $recommendation->last_edited_by }}</td>
                    <td>{{ $recommendation->number_of_item }}</td>
                    <td><button>Edit</button></td>                    
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>List Name</th>
                    <th>List Type</th>
                    <th>Created By</th>
                    <th>Last Edited By</th>
                    <th>Number of Item</th>
                    <th>View / Edit</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
@endsection