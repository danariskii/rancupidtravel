@extends('layouts.master')


@section('main-content')
<div class="row">

<ul>
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</ul>

{!! Form::open(array('route' => 'add_NewTrip', 'class' => 'form')) !!}

    <div class="col-md-8">
        <!-- general form elements disabled -->
        <div class="box box-info">
            <div class="box-header with-border">
            	<h3 class="box-title">Information Trip</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <!-- name trip -->
				<div class="form-group">
				    {!! Form::label('Trip Title') !!}
				    {!! Form::text('Title', null, array( 'class'=>'form-control', 'placeholder'=>'Trip Title')) !!}
				</div>

				<!-- trip deskripsi -->
				<div class="form-group">
				    {!! Form::label('Trip Description') !!}
				    {!! Form::textarea('Description', null, array( 'class'=>'form-control tripDescription', 'placeholder'=>'Trip Description', 'size' => '20x4')) !!}
				</div>

				<!-- type trip -->
				<div class="form-group">
				    {!! Form::label('Type Trip') !!}
					{{ Form::select('type_trip', [
					   '0' => '-',
					   '1' => 'open trip',
					   '2' => 'private trip'], '0', ['class' => 'form-control select2']
					) }}
				</div>

				<!-- style="height:350px;" -->
				<!-- <div id="locationField" class="form-group" > 
					{!! Form::label('Meeting Point') !!}

			    	<input id="autocomplete" placeholder="Enter your address" name="meeting_point" onFocus="geolocate()" type="text" class="form-control"></input>

			    	<input type="hidden" class="field" name="street_number" id="street_number" disabled="true"></input>
			    	<input type="hidden" class="field" name="route" id="route" disabled="true"></input>
			    	<input type="hidden" class="field" name="locality" id="locality" disabled="true"></input>
			    	<input type="hidden" class="field" name="provinsi" id="administrative_area_level_1" disabled="true"></input>
			    	<input type="hidden" class="field" name="postal_code" id="postal_code" disabled="true"></input>
			    	<input type="hidden" class="field" name="country" id="country" disabled="true"></input>
			    </div> -->

				<!-- lokasi trip-->
				<div id="locationField" class="form-group" style="height:350px; padding-bottom: 10px;">
					{!! Form::label('Location Trip') !!}

				    <input id="pac-input" class="controls" type="text" placeholder="Enter a location" name="location_trip">
				    <div id="type-selector" class="controls">
				      <input type="radio" name="type" id="changetype-all" checked="checked">
				      <label for="changetype-all">All</label>

				    </div>
				    <div id="map"></div>

				      <!-- <input type="radio" name="type" id="changetype-establishment">
				      <label for="changetype-establishment">Establishments</label>

				      <input type="radio" name="type" id="changetype-address">
				      <label for="changetype-address">Addresses</label>

				      <input type="radio" name="type" id="changetype-geocode">
				      <label for="changetype-geocode">Geocodes</label> -->
				</div>

			</div>
		</div>

		<!-- Minimum section -->
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Minimun Quantity and Price</h3>
				<!-- <label style="float: right; text-align: center;">
					<input type="checkbox" class="flat-red">
					<h5>Advance Price</h5>
				</label> -->
			</div>
			<div class="box-body">
				<input type="hidden" name="price_list_number" id="price_list_number" value="1"></input>
				<table style="text-align: center;" id="price_list">
						<tr>
							<td class="header_quantityprice">
									<i class="fa fa-user"></i>
							</td>
							<td class="header_quantityprice">
									<i class="fa fa-group"></i>
							</td>
							<td class="header_quantityprice">
									<i class="fa fa-money"></i>
							</td>
							<td>Add</td>							
						</tr>
						<tr id="row_1">
							<td>
								<input class="form-control" type="number" value="1" id="min_range_1" name="min_range_1" min="1">
							</td>
							<td>
								<input class="form-control" type="number" value="2" id="max_range_1" name="max_range_1" min="2">
							</td>
							<td>
								<input type="text" value="0" id="price_1" class="price_trip form-control">
							</td>
							<td>
								<button  class="btn btn-block btn-primary addDetailPrice" type="button">Add Detail Price +</button>
							</td>
							<input type="hidden" name="ready_price_1" id="ready_price_1" value=""></input>
						</tr>
				</table>
			</div>
		</div>

		<!-- Itenerary section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Itenerary</h3>
			</div>
		    <div class="box-body pad">
		      <div class="form-group">
		            <textarea id="Itenerary" name="itenerary" rows="10" cols="80"></textarea>
		      </div>
		    </div>
		</div>

		<!-- Include section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Include</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Include" name="include" rows="10" cols="80"></textarea>
				</div>
		    </div>
		</div>

		<!-- Exclude section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Exclude</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Exclude" name="exclude" rows="10" cols="80"></textarea>
				</div>
		    </div>
		</div>

		<!-- Destination section -->
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Destination</h3>
			</div>
		    <div class="box-body pad">
				<div class="form-group">
		            <textarea id="Destination" name="destination" rows="10" cols="80"></textarea>
				</div>
		    </div>
		</div>

	</div>

	<!-- Date -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Date Trip</h3>
				<button  class="btn btn-block btn-primary" id="add_new_date" type="button" style="float: right; width: auto;">+</button>
			</div>
			<div class="box-body">
				<input type="hidden" name="date_trip_number" id="date_trip_number" value="0"></input>
				<!-- Date and time range -->
				<div class="form-group">
					<label>Pick Start and End Date Trip</label>
					<div class="input-group">
						<div class="input-group-addon">
							<i class="fa fa-clock-o"></i>
						</div>
						<input type="text" class="form-control pull-right" id="reservationtime">
					</div>
				</div>

				<div id = "date_list">
				</div>
			
			</div>
			<!-- /.box-body -->
		</div>
	<!-- /.box -->
	</div>

	<!-- include -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Include Icon</h3>
			</div>
			<div class="box-body pad">
				<input type="hidden" name="include_icon_list_number" id="include_icon_list_number" value="0"></input>
				<div class="form-group">
					<!-- <label>Include</label> -->
					<select class="form-control select2" style="width: 100%;" id="include_icon">
						<option selected="selected"></option>
						@foreach ($Attribute_list as $Attribute)
							<option value="{{$Attribute->id}}">{{$Attribute->id}}{{ $Attribute->attribute}}</option>
						@endforeach
					</select>
				</div>

				<div id = "include_list">
				</div>

			</div>
		</div>
	</div>

	<!-- tag -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Tag</h3>
			</div>
			<div class="box-body pad">

						<div class="form-group">
							<!-- <label>Multiple</label> -->
							<select class="form-control select2" multiple="multiple" data-placeholder="Type your tag here" style="width: 100%;">
								<option>Hiking</option>
								<option>Snorkling</option>
								<option>Beach</option>
								<option>Island</option>
							</select>
						</div>

			</div>
		</div>
	</div>

	<!-- status -->
	<div class="col-md-4">
		<div class="box box-info">
			<div class="box-header">
				<h3 class="box-title">Status</h3>
			</div>
			<div class="box-body pad">

					<div class="form-group">
						<!-- <label>Include</label> -->
						<select class="form-control" name="statusTrip" style="width: 100%;">
							<option selected="selected" value="1">Draft</option>
							<option value="2">Publish</option>
						</select>
					</div>

					<div class="form-group">
						<input type="submit" name="save" value="save" id="save" class="btn btn-block btn-primary">
					</div>	
			</div>
		</div>
	</div>

{!! Form::close() !!}
</div>
@endsection