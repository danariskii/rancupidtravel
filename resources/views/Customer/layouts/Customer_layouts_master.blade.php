@extends('layouts.master-frontend')

@section('htmlheader_title')
    Customer Dashboard
@endsection

@section('main-content')
<div class="inner-banner">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container" style="padding-top: 5em;">
			<!-- <ul class="banner-breadcrumb color-white clearfix">
			<li><a class="link-blue-2" href="#">home</a> /</li>
			<li><a class="link-blue-2" href="#">tours</a> /</li>
			<li><span class="color-blue-2">list tours</span></li>
			</ul> -->
			<h2 class="color-white">Customize your Profile</h2>
			<!-- <h4 class="color-white">We found: <span>640</span> tours</h4> -->
		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
	<div class="container">
	<div class="row">
		<div class="accordion-filter row">
		@include('Customer.layouts.sidebar_customer')

		<div class="col-xs-12 col-sm-8" >
			<div class="accordion style-5">
				
				<div class="acc-panel">
					<div class="acc-title active"><span class="acc-icon"></span>Overview</div>
					<div class="acc-body" style="display: block;">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
											<h5>First Name : <p style="color: black;">{{ $User_detail->first_name }}</p></h5>
							</div>
							<div class="col-xs-12 col-sm-6">
											<h5>Last Name : <p style="color: black;">{{ $User_detail->last_name }}</p></h5>
							</div>
							<div class="col-xs-12 col-sm-6">
											<h5>Email : <p style="color: black;">{{ $User->email }}</p></h5>
							</div>
							<div class="col-xs-12 col-sm-6">
											<h5>Gender : <p style="color: black;">@if ($User_detail->gender == 1) Male @else Female @Endif</p></h5>
							</div>
							<!-- <div class="col-xs-12 col-sm-6">
											<h5>Age : <p style="color: black;">24</p></h5>
							</div> -->		
						</div>
					</div>
				</div>

				<div class="acc-panel Profile" >
					<div class="acc-title"></span>Your Profile</div>
					<div class="acc-body">
						<form class="contact-form js-contact-form" action="http://demo.nrgthemes.com/projects/travel/mail.php" method="POST" action="#">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<div class="input-style-1 type-2 color-2">
										<label><strong style="color: black"> First Name </strong></label>
										<input type="text" name="fields[name]" required="" placeholder="Enter your name" value="{{ $User_detail->first_name }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12">
									<div class="input-style-1 type-2 color-2">
										<label><strong style="color: black"> Last Name </strong></label>
										<input type="text" name="fields[name]" required="" placeholder="Enter your name" value="{{ $User_detail->last_name }}">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12">
									<div class="input-style-1 type-2 color-2">
										<label><strong style="color: black"> Email </strong></label>
										<input type="text" name="fields[email]" required="" placeholder="Enter your email" value="{{ $User->email }}">
									</div>
								</div>
								<div class="col-xs-12">
									<textarea class="area-style-1 color-1" name="fields[text]" required="" placeholder="Enter your Adrress"></textarea>
									<div class="text-center">
										<button type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o"><span>Save</span></button>
									</div>
									<input type="hidden" name="fields[code]" value="56345678safs_">
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="acc-panel Password">
					<div class="acc-title"><span class="acc-icon"></span>Change Your Password</div>
					<div class="acc-body">
						<form class="contact-form js-contact-form" action="http://demo.nrgthemes.com/projects/travel/mail.php" method="POST" action="#">
							<div class="row">
								<div class="col-xs-12 col-sm-12">
									<div class="input-style-1 type-2 color-2">
										<input type="text" name="fields[name]" required="" placeholder="Old Password" value="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12">
									<div class="input-style-1 type-2 color-2">
										<input type="text" name="fields[email]" required="" placeholder="New Password" value="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-12">
									<div class="input-style-1 type-2 color-2">
										<input type="text" name="fields[email]" required="" placeholder="Confirm Password" value="">
									</div>
								</div>
								<div class="col-xs-12">
									<div class="text-center">
										<button type="submit" class="c-button bg-dr-blue-2 hv-dr-blue-2-o"><span>Save</span></button>
									</div>
									<input type="hidden" name="fields[code]" value="56345678safs_">
								</div>
							</div>
						</form>
					</div>
				</div>

				<div class="acc-panel WaitingTrip">
					<div class="acc-title"><span class="acc-icon"></span>Pending Trip</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="{{ asset('/storage/product/1/1.jpg') }}" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>Tour to Belitung</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="{{ asset('/storage/product/2/2.jpg') }}" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>Expedisi to Kawah Ijen</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="acc-panel ReadyTrip">
					<div class="acc-title"><span class="acc-icon"></span>Ready Trip</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="{{ asset('/storage/product/3/3.jpg') }}" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>Belitung</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="{{ asset('/storage/product/4/4.jpg') }}" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>Karimun Jawa</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="acc-panel Journey">
					<div class="acc-title"><span class="acc-icon"></span>History</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="{{ asset('/storage/product/5/5.jpg') }}" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="{{ asset('/storage/product/6/6.jpg') }}" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="acc-panel Message">
					<div class="acc-title"><span class="acc-icon"></span>History</div>
					<div class="acc-body body-chat" style="background-color: #f8f8f8;">
					  <div class="container-chat clearfix">
					    <div class="people-list" id="people-list">
					      <div class="search">
					        <input type="text" placeholder="search" />
					        <i class="fa fa-search"></i>
					      </div>
					      <ul class="list">
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Vincent Porter</div>
					            <div class="status">
					              <i class="fa fa-circle online"></i> online
					            </div>
					          </div>
					        </li>
					        
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_02.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Aiden Chavez</div>
					            <div class="status">
					              <i class="fa fa-circle offline"></i> left 7 mins ago
					            </div>
					          </div>
					        </li>
					        
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_03.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Mike Thomas</div>
					            <div class="status">
					              <i class="fa fa-circle online"></i> online
					            </div>
					          </div>
					        </li>
					        
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_04.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Erica Hughes</div>
					            <div class="status">
					              <i class="fa fa-circle online"></i> online
					            </div>
					          </div>
					        </li>
					        
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_05.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Ginger Johnston</div>
					            <div class="status">
					              <i class="fa fa-circle online"></i> online
					            </div>
					          </div>
					        </li>
					        
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_06.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Tracy Carpenter</div>
					            <div class="status">
					              <i class="fa fa-circle offline"></i> left 30 mins ago
					            </div>
					          </div>
					        </li>
					        
					        <li class="clearfix">
					          <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_07.jpg" alt="avatar" />
					          <div class="about">
					            <div class="name">Christian Kelly</div>
					            <div class="status">
					              <i class="fa fa-circle offline"></i> left 10 hours ago
					            </div>
					          </div>
					        </li>
					        
					      </ul>
					    </div>
					    
					    <div class="chat">
					      <div class="chat-header clearfix">
					        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01_green.jpg" alt="avatar" />
					        
					        <div class="chat-about">
					          <div class="chat-with">Chat with Vincent Porter</div>
					          <div class="chat-num-messages">already 1 902 messages</div>
					        </div>
					        <i class="fa fa-star"></i>
					      </div> <!-- end chat-header -->
					      
					      <div class="chat-history">
					        <ul>
					          <li class="clearfix">
					            <div class="message-data align-right">
					              <span class="message-data-time" >10:10 AM, Today</span> &nbsp; &nbsp;
					              <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>
					              
					            </div>
					            <div class="message other-message float-right">
					              Hi Vincent, how are you? How is the project coming along?
					            </div>
					          </li>
					          
					          <li>
					            <div class="message-data">
					              <span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
					              <span class="message-data-time">10:12 AM, Today</span>
					            </div>
					            <div class="message my-message">
					              Are we meeting today? Project has been already finished and I have results to show you.
					            </div>
					          </li>
					          
					          <li class="clearfix">
					            <div class="message-data align-right">
					              <span class="message-data-time" >10:14 AM, Today</span> &nbsp; &nbsp;
					              <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>
					              
					            </div>
					            <div class="message other-message float-right">
					              Well I am not sure. The rest of the team is not here yet. Maybe in an hour or so? Have you faced any problems at the last phase of the project?
					            </div>
					          </li>
					          
					          <li>
					            <div class="message-data">
					              <span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
					              <span class="message-data-time">10:20 AM, Today</span>
					            </div>
					            <div class="message my-message">
					              Actually everything was fine. I'm very excited to show this to our team.
					            </div>
					          </li>
					          
					          <li>
					            <div class="message-data">
					              <span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
					              <span class="message-data-time">10:31 AM, Today</span>
					            </div>
					            <i class="fa fa-circle online"></i>
					            <i class="fa fa-circle online" style="color: #AED2A6"></i>
					            <i class="fa fa-circle online" style="color:#DAE9DA"></i>
					          </li>
					          
					        </ul>
					        
					      </div> <!-- end chat-history -->
					      
					      <div class="chat-message clearfix">
					        <textarea name="message-to-send" id="message-to-send" placeholder ="Type your message" rows="3"></textarea>
					                
					        <i class="fa fa-file-o"></i> &nbsp;&nbsp;&nbsp;
					        <i class="fa fa-file-image-o"></i>
					        
					        <button>Send</button>

					      </div> <!-- end chat-message -->
					      
					    </div> <!-- end chat -->
					    
					  </div> <!-- end container -->

					<script id="message-template" type="text/x-handlebars-template">
					  <li class="clearfix">
					    <div class="message-data align-right">
					      <span class="message-data-time" >{{time, Today</span> &nbsp; &nbsp;
					      <span class="message-data-name" >Olia</span> <i class="fa fa-circle me"></i>
					    </div>
					    <div class="message other-message float-right">
					      {{messageOutput
					    </div>
					  </li>
					</script>

					<script id="message-response-template" type="text/x-handlebars-template">
					  <li>
					    <div class="message-data">
					      <span class="message-data-name"><i class="fa fa-circle online"></i> Vincent</span>
					      <span class="message-data-time">{{time, Today</span>
					    </div>
					    <div class="message my-message">
					      {{response
					    </div>
					  </li>
					</script>

					</div>
				</div>		

				<div class="acc-panel Review">
					<div class="acc-title"><span class="acc-icon"></span>Your Review</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">Write a Review</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">Write a Review</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="acc-panel Discuss">
					<div class="acc-title"><span class="acc-icon"></span>Discussion</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="additional-block">
							<h4 class="additional-title">comments <span class="color-dr-blue-2">(4)</span></h4>
							<ul class="comments-block">
								<li class="comment-entry clearfix">
									<img class="commnent-img" src="img/detail/comment_1.jpg" alt="">
									<div class="comment-content clearfix" style="background-color: white;">
									<div class="tour-info-line">
									<div class="tour-info">
									<img src="img/calendar_icon_grey.png" alt="">
									<span class="font-style-2 color-dark-2">03/07/2015</span>
									</div>
									<div class="tour-info">
									<img src="img/people_icon_grey.png" alt="">
									<span class="font-style-2 color-dark-2">By Emma Stone</span>
									</div>
									</div>
									<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
									<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
									</div>
									
									<ul class="comments-block">
									<li class="comment-entry clearfix">
									<img class="commnent-img" src="img/detail/comment_2.jpg" alt="">
									<div class="comment-content clearfix" style="background-color: white;">
									<div class="tour-info-line">
									<div class="tour-info">
									<img src="img/calendar_icon_grey.png" alt="">
									<span class="font-style-2 color-dark-2">03/07/2015</span>
									</div>
									<div class="tour-info">
									<img src="img/people_icon_grey.png" alt="">
									<span class="font-style-2 color-dark-2">By Emma Stone</span>
									</div>
									</div>
									<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
									<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
									</div>
									</li>
									</ul>
								</li>
								<li class="comment-entry clearfix">
									<img class="commnent-img" src="img/detail/comment_3.jpg" alt="">
									<div class="comment-content clearfix" style="background-color: white;">
									<div class="tour-info-line">
									<div class="tour-info">
										<img src="img/calendar_icon_grey.png" alt="">
										<span class="font-style-2 color-dark-2">03/07/2015</span>
									</div>
									<div class="tour-info">
										<img src="img/people_icon_grey.png" alt="">
										<span class="font-style-2 color-dark-2">By Emma Stone</span>
									</div>
									</div>
									<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.
									</div>
										<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
									</div>
								</li>
								<li class="comment-entry clearfix">
									<img class="commnent-img" src="img/detail/comment_1.jpg" alt="">
									<div class="comment-content clearfix">
									<div class="tour-info-line">
									<div class="tour-info">
									<img src="img/calendar_icon_grey.png" alt="">
									<span class="font-style-2 color-dark-2">03/07/2015</span>
									</div>
									<div class="tour-info">
									<img src="img/people_icon_grey.png" alt="">
									<span class="font-style-2 color-dark-2">By Emma Stone</span>
									</div>
									</div>
									<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
									<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<div class="acc-panel Favorite">
					<div class="acc-title"><span class="acc-icon"></span>Favorite Agent Travel</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix grid-content">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="acc-panel Wishlist">
					<div class="acc-title"><span class="acc-icon"></span>Wishlist Package</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="acc-panel WDestination">
					<div class="acc-title"><span class="acc-icon"></span>Wishlist Destination</div>
					<div class="acc-body" style="background-color: #f8f8f8;">
						<div class="list-header clearfix">
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by price</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low price to High</a>
										<a href="#">High price to Lowest</a>
									</span>
								</div>
							</div>
							<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
								<div class="drop">
									<b>Sort by ranking</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
									<span>
										<a href="#">Low Ranking to High</a>
										<a href="#">High Ranking to Low</a>
									</span>
								</div>
							</div>
							<div class="list-view-change">
								<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
								<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
								<div class="change-to-label fr color-grey-8">View:</div>
							</div>
						</div>
						<div class="list-content clearfix grid-content">

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>

							<div class="list-item-entry">
								<div class="hotel-item style-3 bg-white">
									<div class="table-view">
										<div class="radius-top cell-view">
											<img src="img/car_1.png" alt="">
										</div>
										<div class="title hotel-middle clearfix cell-view">
											<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
											<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
											<h4><b>tours in greece</b></h4>
											<div class="rate-wrap">
												<div class="rate">
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
													<span class="fa fa-star color-yellow"></span>
												</div>
												<i>485 rewies</i>
											</div>
											<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
										</div>
										<div class="title hotel-right clearfix cell-view">
											<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
											<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>


		</div>
	</div>
	</div>
</div>


@endsection