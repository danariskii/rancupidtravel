<!-- <div class="col-xs-12 col-sm-4 col-md-3">
	<div class="sidebar style-2 clearfix">
		<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">search</h4>
			<div class="search-inputs">
				<div class="form-block clearfix">
					<div class="input-style-1 b-50 color-4">
						<img src="img/loc_icon_small_grey.png" alt="">
						<input type="text" placeholder="Where do you want to go?">
					</div>
				</div>
				<div class="form-block clearfix">
					<div class="input-style-1 b-50 color-4">
					<img src="img/calendar_icon_grey.png" alt="">
					<input type="text" placeholder="Check In" class="datepicker">
					</div>
				</div>
				<div class="form-block clearfix">
					<div class="input-style-1 b-50 color-4">
					<img src="img/calendar_icon_grey.png" alt="">
					<input type="text" placeholder="Check Out" class="datepicker">
					</div>
				</div>
			</div>
			<input type="submit" class="c-button b-40 bg-dr-blue hv-dr-blue-o" value="search">
		</div>

		<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">categories</h4>
			<ul class="sidebar-category color-2">
			<li class="active">
			<a class="cat-drop" href="#">tours <span class="fr">(68)</span></a>
			<ul>
			<li><a href="#">sea tours (785)</a></li>
			<li><a href="#">food tours (85)</a></li>
			<li><a href="#">romantic tours (125)</a></li>
			<li><a href="#">honeymoon tours (70)</a></li>
			<li><a href="#">mountain tours (159)</a></li>
			</ul>
			</li>
			<li>
			<a class="cat-drop" href="#">hotels <span class="fr">(125)</span></a>
			<ul>
			<li><a href="#">sea tours (785)</a></li>
			<li><a href="#">food tours (85)</a></li>
			<li><a href="#">romantic tours (125)</a></li>
			<li><a href="#">honeymoon tours (70)</a></li>
			<li><a href="#">mountain tours (159)</a></li>
			</ul>
			</li>
			<li>
			<a class="cat-drop" href="#">cruises <span class="fr">(75)</span></a>
			<ul>
			<li><a href="#">sea tours (785)</a></li>
			<li><a href="#">food tours (85)</a></li>
			<li><a href="#">romantic tours (125)</a></li>
			<li><a href="#">honeymoon tours (70)</a></li>
			<li><a href="#">mountain tours (159)</a></li>
			</ul>
			</li>
			<li>
			<a class="cat-drop" href="#">flights <span class="fr">(93)</span></a>
			<ul>
			<li><a href="#">sea tours (785)</a></li>
			<li><a href="#">food tours (85)</a></li>
			<li><a href="#">romantic tours (125)</a></li>
			<li><a href="#">honeymoon tours (70)</a></li>
			<li><a href="#">mountain tours (159)</a></li>
			</ul>
			</li>
			</ul>
		</div>

		<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">price range</h4>
			<div class="slider-range color-2 clearfix" data-counter="$" data-position="start" data-from="0" data-to="1500" data-min="0" data-max="2000">
			<div class="range"></div>
			<input type="text" class="amount-start" readonly value="$0">
			<input type="text" class="amount-end" readonly value="$1500">
			</div>
			<input type="submit" class="c-button b-40 bg-dr-blue hv-dr-blue-o" value="search">
			</div>
			<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">star rating</h4>
			<div class="sidebar-rating">
			<div class="input-entry color-3">
			<input class="checkbox-form" id="star-5" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="star-5">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="star-4" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="star-4">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="star-3" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="star-3">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="star-2" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="star-2">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			<span class="fa fa-star color-yellow"></span>
			</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="star-1" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="star-1">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			</span>
			</label>
			</div>
			</div>
		</div>

		<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">Facility</h4>
			<div class="sidebar-rating">
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-1" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-1">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Swimming Pool</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-2" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-2">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Gym / Fitness</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-3" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-3">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Car Park</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-4" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-4">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Spa / Sauna</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-5" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-5">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Airport Transfer</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-6" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-6">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Smoking Area</span>
			</label>
			</div>
			<div class="input-entry color-3">
			<input class="checkbox-form" id="text-7" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="text-7">
			<span class="sp-check"><i class="fa fa-check"></i></span>
			<span class="checkbox-text">Pet-friendly</span>
			</label>
			</div>
			</div>
		</div>

		<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">Review Score</h4>
			<div class="sidebar-score">
			<div class="input-entry type-2 color-4">
			<input class="checkbox-form" id="score-5" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="score-5">
			<span class="checkbox-text">
			5
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			</span>
			</span>
			<span class="sp-check"><i class="fa fa-check"></i></span>
			</label>
			</div>
			<div class="input-entry type-2 color-4">
			<input class="checkbox-form" id="score-4" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="score-4">
			<span class="checkbox-text">
			4
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			</span>
			</span>
			<span class="sp-check"><i class="fa fa-check"></i></span>
			</label>
			</div>
			<div class="input-entry type-2 color-4">
			<input class="checkbox-form" id="score-3" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="score-3">
			<span class="checkbox-text">
			3
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			</span>
			</span>
			<span class="sp-check"><i class="fa fa-check"></i></span>
			</label>
			</div>
			<div class="input-entry type-2 color-4">
			<input class="checkbox-form" id="score-2" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="score-2">
			<span class="checkbox-text">
			2
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			</span>
			</span>
			<span class="sp-check"><i class="fa fa-check"></i></span>
			</label>
			</div>
			<div class="input-entry type-2 color-4">
			<input class="checkbox-form" id="score-1" type="checkbox" name="checkbox" value="climat control">
			<label class="clearfix" for="score-1">
			<span class="checkbox-text">
			1
			<span class="rate">
			<span class="fa fa-star color-yellow"></span>
			</span>
			</span>
			<span class="sp-check"><i class="fa fa-check"></i></span>
			</label>
			</div>
			</div>
		</div>

		<div class="sidebar-block">
			<h4 class="sidebar-title color-dark-2">Area</h4>
			<div class="sidebar-rating">
				<div class="input-entry color-3">
				<input class="checkbox-form" id="area-1" type="checkbox" name="checkbox" value="climat control">
				<label class="clearfix" for="area-1">
				<span class="sp-check"><i class="fa fa-check"></i></span>
				<span class="checkbox-text">Earls Court</span>
				</label>
				</div>
				<div class="input-entry color-3">
				<input class="checkbox-form" id="area-2" type="checkbox" name="checkbox" value="climat control">
				<label class="clearfix" for="area-2">
				<span class="sp-check"><i class="fa fa-check"></i></span>
				<span class="checkbox-text">Victoria and Westminster</span>
				</label>
				</div>
				<div class="input-entry color-3">
				<input class="checkbox-form" id="area-3" type="checkbox" name="checkbox" value="climat control">
				<label class="clearfix" for="area-3">
				<span class="sp-check"><i class="fa fa-check"></i></span>
				<span class="checkbox-text">Bloomsbury - Fitzrovia</span>
				</label>
				</div>
				<div class="input-entry color-3">
				<input class="checkbox-form" id="area-4" type="checkbox" name="checkbox" value="climat control">
				<label class="clearfix" for="area-4">
				<span class="sp-check"><i class="fa fa-check"></i></span>
				<span class="checkbox-text">West End -Soho</span>
				</label>
				</div>
				<div class="input-entry color-3">
				<input class="checkbox-form" id="area-5" type="checkbox" name="checkbox" value="climat control">
				<label class="clearfix" for="area-5">
				<span class="sp-check"><i class="fa fa-check"></i></span>
				<span class="checkbox-text">Chelsea - Kensington</span>
				</label>
				</div>
			</div>
		</div>
	</div>
</div> -->

	<div class="col-xs-12 col-sm-4">
		<div class="accordion-chooser">
			<a data-fifter=".Overview"		@if($select == 'Overview')		class="active" @endif	href="#">Overview</a>
			<a data-fifter=".Profile"		@if($select == 'Profile')		class="active" @endif	href="#">Your Profile</a>
			<a data-fifter=".Password"		@if($select == 'Password')		class="active" @endif	href="#">Change Password</a>
			<a data-fifter=".WaitingTrip"	@if($select == 'WaitingTrip')	class="active" @endif	href="#">Pending Trip</a>
			<a data-fifter=".ReadyTrip"		@if($select == 'ReadyTrip')		class="active" @endif	href="#">Upcoming Trip</a>
			<a data-fifter=".Journey"		@if($select == 'Journey')		class="active" @endif	href="#">History</a>
			<a data-fifter=".Message"		@if($select == 'Journey')		class="active" @endif	href="#">Message</a>
			<a data-fifter=".Review"		@if($select == 'Review')		class="active" @endif	href="#">Review</a>
			<a data-fifter=".Discuss"		@if($select == 'Discuss')		class="active" @endif	href="#">Discussion</a>
			<a data-fifter=".Favorite"		@if($select == 'Favorite')		class="active" @endif	href="#">Favorite Travel Agent</a>
			<a data-fifter=".Wishlist"		@if($select == 'Wishlist')		class="active" @endif	href="#">Wishlist Package Travel</a>
			<a data-fifter=".WDestination"	@if($select == 'WDestination')	class="active" @endif	href="#">Wishlist Destination</a>
			<!-- <a data-fifter="*" href="#">let’s travel Support</a> -->
		</div>
	</div>