@extends('layouts.master-frontend')

@section('htmlheader_title')
    Customer Dashboard
@endsection

@section('main-content')
<div class="inner-banner">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container" style="padding-top: 5em;">
			<!-- <ul class="banner-breadcrumb color-white clearfix">
			<li><a class="link-blue-2" href="#">home</a> /</li>
			<li><a class="link-blue-2" href="#">tours</a> /</li>
			<li><span class="color-blue-2">list tours</span></li>
			</ul> -->
			<h2 class="color-white">Customize your Profile</h2>
			<!-- <h4 class="color-white">We found: <span>640</span> tours</h4> -->
		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
	<div class="container">
	<div class="row">
	
	@include('layouts.sidebar_customer')

		<div class="col-xs-12 col-sm-8 col-md-8">
			<div class="list-header clearfix">
				<!-- <div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
					<div class="drop">
						<b>Sort by price</b>
						<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
						<span>
							<a href="#">ASC</a>
							<a href="#">DESC</a>
						</span>
					</div>
				</div>
				<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
					<div class="drop">
						<b>Sort by ranking</b>
						<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
						<span>
							<a href="#">ASC</a>
							<a href="#">DESC</a>
						</span>
					</div>
				</div> -->
				<div class="list-view-change">
					<div class="change-grid color-1 fr"><i class="fa fa-th"></i></div>
					<div class="change-list color-1 fr active"><i class="fa fa-bars"></i></div>
					<div class="change-to-label fr color-grey-8">View:</div>
				</div>
			</div>
			<div class="list-content clearfix">

				<div class="list-item-entry">
					<div class="hotel-item style-3 bg-white">
						<div class="table-view">
							<div class="radius-top cell-view">
								<img src="img/car_1.png" alt="">
							</div>
							<div class="title hotel-middle clearfix cell-view">
								<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
								<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
								<h4><b>tours in greece</b></h4>
								<div class="rate-wrap">
									<div class="rate">
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
									</div>
									<i>485 rewies</i>
								</div>
								<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
							</div>
							<div class="title hotel-right clearfix cell-view">
								<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
								<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
							</div>
						</div>
					</div>
				</div>

				<div class="list-item-entry">
					<div class="hotel-item style-3 bg-white">
						<div class="table-view">
							<div class="radius-top cell-view">
								<img src="img/car_1.png" alt="">
							</div>
							<div class="title hotel-middle clearfix cell-view">
								<div class="date list-hidden">July <strong>19th</strong> to July <strong>26th</strong></div>
								<div class="date grid-hidden"><strong>19.07 - 26.07 / 7</strong> night</div>
								<h4><b>tours in greece</b></h4>
								<div class="rate-wrap">
									<div class="rate">
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
										<span class="fa fa-star color-yellow"></span>
									</div>
									<i>485 rewies</i>
								</div>
								<p class="f-14 grid-hidden">Nunc cursus libero purus ac congue arcu cur sus ut sed vitae pulvinar. Nunc cursus libero purus ac congue arcu.</p>
							</div>
							<div class="title hotel-right clearfix cell-view">
								<!-- <div class="hotel-person color-dark-2">from <span class="color-blue">$273</span> person</div> -->
								<a class="c-button b-40 bg-blue hv-blue-o grid-hidden" href="#">view more</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
	</div>
</div>


@endsection