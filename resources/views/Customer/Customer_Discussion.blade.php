@extends('layouts.master-frontend')

@section('htmlheader_title')
    Customer Dashboard
@endsection

@section('main-content')
<div class="inner-banner">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container" style="padding-top: 5em;">
			<!-- <ul class="banner-breadcrumb color-white clearfix">
			<li><a class="link-blue-2" href="#">home</a> /</li>
			<li><a class="link-blue-2" href="#">tours</a> /</li>
			<li><span class="color-blue-2">list tours</span></li>
			</ul> -->
			<h2 class="color-white">Customize your Profile</h2>
			<!-- <h4 class="color-white">We found: <span>640</span> tours</h4> -->
		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
	<div class="container">
	<div class="row">
	
	@include('layouts.sidebar_customer')

		<div class="col-xs-12 col-sm-8 col-md-8">
			<div class="additional-block">
				<h4 class="additional-title">comments <span class="color-dr-blue-2">(4)</span></h4>
				<ul class="comments-block">
					<li class="comment-entry clearfix">
						<img class="commnent-img" src="img/detail/comment_1.jpg" alt="">
						<div class="comment-content clearfix" style="background-color: white;">
						<div class="tour-info-line">
						<div class="tour-info">
						<img src="img/calendar_icon_grey.png" alt="">
						<span class="font-style-2 color-dark-2">03/07/2015</span>
						</div>
						<div class="tour-info">
						<img src="img/people_icon_grey.png" alt="">
						<span class="font-style-2 color-dark-2">By Emma Stone</span>
						</div>
						</div>
						<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
						<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
						</div>
						
						<ul class="comments-block">
						<li class="comment-entry clearfix">
						<img class="commnent-img" src="img/detail/comment_2.jpg" alt="">
						<div class="comment-content clearfix" style="background-color: white;">
						<div class="tour-info-line">
						<div class="tour-info">
						<img src="img/calendar_icon_grey.png" alt="">
						<span class="font-style-2 color-dark-2">03/07/2015</span>
						</div>
						<div class="tour-info">
						<img src="img/people_icon_grey.png" alt="">
						<span class="font-style-2 color-dark-2">By Emma Stone</span>
						</div>
						</div>
						<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
						<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
						</div>
						</li>
						</ul>
					</li>
					<li class="comment-entry clearfix">
						<img class="commnent-img" src="img/detail/comment_3.jpg" alt="">
						<div class="comment-content clearfix" style="background-color: white;">
						<div class="tour-info-line">
						<div class="tour-info">
							<img src="img/calendar_icon_grey.png" alt="">
							<span class="font-style-2 color-dark-2">03/07/2015</span>
						</div>
						<div class="tour-info">
							<img src="img/people_icon_grey.png" alt="">
							<span class="font-style-2 color-dark-2">By Emma Stone</span>
						</div>
						</div>
						<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.
						</div>
							<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
						</div>
					</li>
					<li class="comment-entry clearfix">
						<img class="commnent-img" src="img/detail/comment_1.jpg" alt="">
						<div class="comment-content clearfix">
						<div class="tour-info-line">
						<div class="tour-info">
						<img src="img/calendar_icon_grey.png" alt="">
						<span class="font-style-2 color-dark-2">03/07/2015</span>
						</div>
						<div class="tour-info">
						<img src="img/people_icon_grey.png" alt="">
						<span class="font-style-2 color-dark-2">By Emma Stone</span>
						</div>
						</div>
						<div class="comment-text color-grey">Nunc a tincidunt lectus, vitae molestie ante. Maecenas feugiat commodo sem facilisis vulputate. Integer fermentum laoreet sollicitud insollicitudin sollicitudin.</div>
						<a class="comment-reply c-button b-26 bg-dr-blue-2 hv-dr-blue-2-o" href="#">Reply</a>
						</div>
					</li>
				</ul>
			</div>
			</div>
		</div>
	</div>
</div>


@endsection