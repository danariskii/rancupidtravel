@extends('Customer.layouts.Customer_layouts_master')

@section('dashboard-content')
					<div class="acc-title active"><span class="acc-icon"></span>Overview</div>
					<div class="acc-body" style="display: block;">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
											<h5>First Namer : <p style="color: black;">{{ $User_detail->first_name }}</p></h5>
							</div>
							<div class="col-xs-12 col-sm-6">
											<h5>Last Name : <p style="color: black;">{{ $User_detail->last_name }}</p></h5>
							</div>
							<div class="col-xs-12 col-sm-6">
											<h5>Email : <p style="color: black;">{{ $User->email }}</p></h5>
							</div>
							<div class="col-xs-12 col-sm-6">
											<h5>Gender : <p style="color: black;">@if ($User_detail->gender == 1) Male @else Female @Endif</p></h5>
							</div>
							<!-- <div class="col-xs-12 col-sm-6">
											<h5>Age : <p style="color: black;">24</p></h5>
							</div> -->		
						</div>
					</div>
@endsection