@extends('layouts.master-frontend')

@section('main-content')
<div class="inner-banner">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container" style="padding-top: 5em;">
			<!-- <ul class="banner-breadcrumb color-white clearfix">
			<li><a class="link-blue-2" href="#">home</a> /</li>
			<li><a class="link-blue-2" href="#">tours</a> /</li>
			<li><span class="color-blue-2">list tours</span></li>
			</ul> -->
			<h2 class="color-white">all tours for you</h2>
			<h4 class="color-white">We found: <span>640</span> tours</h4>
		</div>
	</div>
</div>
<div class="main-wraper padd-90 bg-grey-2 tabs-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<div class="second-title">
			<h2>Cart</h2>
			</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-12">
			<div class="simple-tab type-2 tab-wrapper">
			<!-- <div class="tab-nav-wrapper">
				<div class="nav-tab  clearfix">
					<div class="nav-tab-item active">
					Sports
					</div>
					<div class="nav-tab-item">
					Culture and History
					</div>
					<div class="nav-tab-item">
					Nightlife
					</div>
					<div class="nav-tab-item">
					hotels
					</div>
					<div class="nav-tab-item">
					Flights
					</div>
				</div>
			</div> -->
			<div class="tabs-content tabs-wrap-style clearfix" style="background-color: white;">
			<div class="tab-info active">
				<div class="acc-body">
					<div class="acc-body-block">

						@foreach( $products as $product)
							<div class="alert bg-1" style="background-color: #c4eeff; color:black;">
								<span>Trip by<h5>{{ $product->Agent_Name }}</h5></span>
								<img src="{{ '/storage/product/'.$product->id.'/'.$product->dp_item }}" alt="" style="width: 10%;">
								
								<!-- <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span> -->
								<i class="fa fa-times-circle-o"></i>
							</div>
						@endforeach

						<div>
							<button style="float: left; background-color: red; color: white;" class="c-button  hv-transparent">Back</button>
							<button style="float: right; background-color: #23b0e8; color: white;" class="c-button  hv-transparent" id="pay-button">
								Choose Payment Method >>
							</button>
							<!-- <pre><div id="result-json">JSON result will appear here after payment:<br></div></pre> -->
						</div>

					</div>
				</div>
			</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</div>
@endsection