<header class="menu-3 type-5">
	<div class="top-header-bar">
		<div class="container">
			<div class="left-col">
				<!-- <a href="/contactUs"><i class="fa fa-envelope"></i>Contact Us</a> -->
				@if(!empty(Auth::user()->id))
				<a href="/agentDashboard"><i class="fa fa-suitcase"></i>My Travel</a>
				<a href="{{ url('/logout') }}" id="logout"
				   onclick="event.preventDefault();
				             document.getElementById('logout-form').submit();">
				    <i class="fa fa-envelope "></i>
				    <!-- trans('adminlte_lang::message.signout') }} -->
				    Sign Out
				</a>
				<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
				    {{ csrf_field() }}
				    <input type="submit" value="logout" style="display: none;">
				</form>

				<a href="{{ url('/Customer_Dashboard') }}"><i class="fa fa-envelope "></i>Profile</a>
				@else
				<a href="/register"><i class="fa fa-suitcase"></i>Sign Up</a>
				<a href="{{ url('/login') }}"><i class="fa fa-envelope "></i>Sign in</a>
				@endif
			</div>
			<div class="right-col">
				<div class="folow">
					<a href="/contactUs" style="width: 150px;"><i class="fa fa-envelope"></i> Contact Us</a>
					<a href="{{ url('/wishlist') }}"><i class="fa fa-heart-o"></i></a>
					<a href="{{ url('/cart') }}"><i class="fa fa-shopping-cart"></i></a>
				</div>
				 <!-- Form::open(array('route' => 'search', 'class' => 'serach-item')) !!} -->

				<div class="serach-item">
					<button type="submit" class="fa fa-search" onclick="Search()"></button>
					<input id="Search" type="text" name="keyword" placeholder="Search...">
				</div>
					<!-- <select class="js-example-placeholder-single js-states form-control" id="Search" name="keyword" style="width: 100%;">
					  <option selected></option>
					  <option>asd</option>
					  <option>opop</option>
					</select> -->

				 <!-- Form::close() !!} -->
			</div>
		</div>
	</div>
	<div class="nav">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="{{ url('/') }}" class="logo">
						<img src="{{ asset('/storage/assets/Icon/Logo.png') }}" alt="Rancupid Travel" style="width: 120%;">
					</a>
					<div class="nav-menu-icon">
						<a href="#"><i></i></a>
					</div>
					<nav class="menu">
					<ul>
						<li class="type-1 active">
							<a href="/">Home<span class="fa fa-angle-down"></span></a>
						</li>
						<li class="type-1">
							<a href="search">Indonesia Trip<span class="fa fa-angle-down"></span></a>
							<ul class="dropmenu">
								<li><a href="search?keyword=Jawa">Jawa</a></li>
								<li><a href="search?keyword=Bali">Bali</a></li>
								<li><a href="search?keyword=Nusa Tenggara Timur">Nusa Tenggara Timur</a></li>
								<li><a href="search?keyword=Nusa Tenggara Barat">Nusa Tenggara Barat</a></li>
								<li><a href="search?keyword=Papua">Papua</a></li>
								<li><a href="search?keyword=Sumatra">Sumatra</a></li>
								<li><a href="search?keyword=Kalimantan">Borneo / Kalimantan</a></li>
								<li><a href="search?keyword=Sulawesi">Sulawesi</a></li>
							</ul>
						</li>
						<li class="type-1"><a href="#">International Trip<span class="fa fa-angle-down"></span></a>
							<ul class="dropmenu">
								<li><a href="search?keyword=Europe">Europe</a></li>
								<li><a href="search?keyword=Asia">Asia</a></li>
								<li><a href="search?keyword=Autralia">Autralia</a></li>
								<li><a href="search?keyword=Afrika">Afrika</a></li>
								<li><a href="search?keyword=Antartika">Antartika</a></li>
								<li><a href="search?keyword=North Amerika">North Amerika</a></li>
								<li><a href="search?keyword=South Amerika">South Amerika</a></li>
							</ul>
						</li>
						<!-- <li class="type-1">
							<a href="#">How To Book<span class="fa fa-angle-down"></span></a>
						</li>
						<li class="type-1">
							<a href="#">Reviews<span class="fa fa-angle-down"></span></a>
						</li>
						<li class="type-1">
							<a href="#">Gallery<span class="fa fa-angle-down"></span></a>
						</li>
						<li class="type-1">
							<a href="#">Blog<span class="fa fa-angle-down"></span></a>
						</li> -->
					</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>