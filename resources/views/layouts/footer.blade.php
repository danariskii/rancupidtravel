<footer class="bg-dark type-2">
	<div class="container">
	<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="footer-block">
	<img src="{{ asset('/storage/assets/Icon/Logo.png') }}" alt="" class="logo-footer" style="width: 90%;">
	<div class="f_text color-grey-7">Travel is the only thing you buy that makes you richer</div>
	<div class="footer-share">
	<a href="https://www.facebook.com/rancupidtravel/"><span class="fa fa-facebook"></span></a>
	<!-- <a href="#"><span class="fa fa-twitter"></span></a> -->
	<!-- <a href="#"><span class="fa fa-google-plus"></span></a> -->
	<a href="https://www.instagram.com/rancupidtravel/"><span class="fa fa-instagram"></span></a>
	</div>
	</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-sm-6 no-padding">
	<div class="footer-block">
	<h6>Travel News</h6>
	<div class="f_news clearfix">
	<a class="f_news-img black-hover" href="#">
	<img class="img-responsive" src="{{ asset('/storage/blog/1/1.JPG') }}" alt="">
	<div class="tour-layer delay-1"></div>
	</a>
	<div class="f_news-content">
	<a class="f_news-tilte color-white link-red" href="#">Perjalanan ke Pulau Bangka dan Belitung</a>
	<span class="date-f">Sept 29, 2017</span>
	<a href="http://www.meutiadiary.com/2017/09/perjalanan-ke-pulau-bangka-dan-belitung.html" class="r-more">read more</a>
	</div>
	</div>
	<div class="f_news clearfix">
	<a class="f_news-img black-hover" href="http://www.meutiadiary.com/2017/09/floating-market-lok-baintan.html">
	<img class="img-responsive" src="{{ asset('/storage/blog/3/3.JPG') }}" alt="">
	<div class="tour-layer delay-1"></div>
	</a>
	<div class="f_news-content">
	<a class="f_news-tilte color-white link-red" href="#">Floating Market Lok Baintan</a>
	<span class="date-f">Sept 11, 2017</span>
	<a href="http://www.meutiadiary.com/2017/09/floating-market-lok-baintan.html" class="r-more">read more</a>
	</div>
	</div>
	<div class="f_news clearfix">
	<a class="f_news-img black-hover" href="#">
	<img class="img-responsive" src="{{ asset('/storage/blog/2/2.JPG') }}" alt="">
	<div class="tour-layer delay-1"></div>
	</a>
	<div class="f_news-content">
	<a class="f_news-tilte color-white link-red" href="#">Menikmati Kota Banjarmasin</a>
	<span class="date-f">Sept 08, 2017</span>
	<a href="http://www.meutiadiary.com/2017/09/menikmati-kota-banjarmasin.html" class="r-more">read more</a>
	</div>
	</div>
	</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="footer-block">
	<h6>Tags:</h6>
	<a href="#" class="tags-b">flights</a>
	<a href="#" class="tags-b">traveling</a>
	<a href="#" class="tags-b">sale</a>
	<a href="#" class="tags-b">cruises</a>
	<a href="#" class="tags-b">cars</a>
	<a href="#" class="tags-b">hotels</a>
	<a href="#" class="tags-b">tours</a>
	<a href="#" class="tags-b">booking</a>
	<a href="#" class="tags-b">countries</a>
	</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="footer-block">
	<h6>Contact Info</h6>
	<div class="contact-info">
	<!-- <div class="contact-line color-grey-3"><i class="fa fa-map-marker"></i><span>Aenean vulputate porttitor</span></div> -->
	<div class="contact-line color-grey-3"><i class="fa fa-phone"></i><a href="tel:93123456789">021 570 7879</a></div>
	<div class="contact-line color-grey-3"><i class="fa fa-envelope-o"></i><a href="">info@rancupidtravel.com</a></div>
	<!-- <div class="contact-line color-grey-3"><i class="fa fa-globe"></i><a href="#"></a></div> -->
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="footer-link bg-black">
	<div class="container">
	<div class="row">
	<div class="col-md-12">
	<div class="copyright">
	<span>&copy; 2015 All rights reserved. LET'STRAVEL</span>
	</div>
	<ul>
	<li><a class="link-aqua" href="#">Privacy Policy </a></li>
	<li><a class="link-aqua" href="#">About Us</a></li>
	<li><a class="link-aqua" href="#">Support</a></li>
	<li><a class="link-aqua" href="#">FAQ</a></li>
	<li><a class="link-aqua" href="#">Blog</a></li>
	<li><a class="link-aqua" href="#">How to Book</a></li>
	<li><a class="link-aqua" href="#">Gallery</a></li>
	</ul>
	</div>
	</div>
	</div>
	</div>
</footer>