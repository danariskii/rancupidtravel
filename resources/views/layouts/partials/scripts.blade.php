<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<!-- <script src=" url (mix('/js/app.js')) }}" type="text/javascript"></script> -->

<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->



<!-- jQuery 3 -->
<script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
<!-- checkbox and radio -->
<script src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- ckeditor -->
<script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>

<!-- DataTables -->
<!-- {{ asset('') }} -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script src="{{ asset('js/Price Format/jquery.price_format.2.0.js') }}"></script>
<script src="{{ asset('bower_components/sweetalert/sweetalert2.min.js') }}"></script>
<script src="{{ asset('bower_components/imageuploadify/imageuploadify.min.js') }}"></script>
<!-- <script src="{{ asset('bower_components/dropzone/js/dropzone.js') }}"></script> -->
<script src="{{ asset('bower_components/dropzone/js/main.js') }}"></script>

<script src="{{ asset('bower_components/autoNumeric/autoNumeric.min.js') }}"></script>

<script src="{{ asset('bower_components/dropzone/js/dropzone-eno.js') }}"></script>
<!-- <script src="{{ asset('bower_components/dropzone/js/dropzone-config.js') }}"></script> -->

<!--     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbFMOmUOlFI93XChSeuo2SjF4YBLIEc2w&libraries=places&callback=initAutocomplete"
        async defer></script> -->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBbFMOmUOlFI93XChSeuo2SjF4YBLIEc2w&libraries=places&callback=initMap"
        async defer></script>
    <!-- $('#example1').DataTable() -->
<script>    
    jQuery(document).ready(function($) 
    {
      $('#body').show();
      $('#msg').hide();
      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
      });

    });

    var table = $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true
    });

    // Event listener to the two range filtering inputs to redraw on input
    $('#min, #max').keyup( function() {
        table.draw();
    } );

/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
      var filterBy = document.getElementById('filterTrip').value;
      var status = data[3];

        // var min = parseInt( $('#min').val(), 10 );
        // var max = parseInt( $('#max').val(), 10 );
        // var age = parseFloat( data[3] ) || 0; // use data for the age column
 
        // if ( ( isNaN( min ) && isNaN( max ) ) ||
        //      ( isNaN( min ) && age <= max )   ||
        //      ( min <= age   && isNaN( max ) ) ||
        //      ( min <= age   && age <= max ) )
        if ( filterBy == status || filterBy == 'All')
        {
            return true;
        }
        return false;
    }
);

    function FilterTripAgent(filterBy)
    {
      oldFilter = document.getElementById('filterTrip').value;
      $("#Filter"+oldFilter).removeClass('active');

      $("#Filter"+filterBy).addClass('active');
      document.getElementById('filterTrip').value = filterBy;

      table.draw();
    };


  // ===================== admin function ====================
    function updatestatusAgent(id_agent,status)
    {
      // alert('id_agent= '+id_agent);
      $.ajax({
          async:false,
          type: "POST",
          url: "updateStatusAgent",
          data:{ agent_id: id_agent, status: status },
          success:function(data)
          {
            // alert('data');
            // console.log(data);
            location.reload();
          }
      });
    };
  // ================== end admin function ===================

  // ================== agent function =======================

    function deleteTrip(id)
    {
        swal({
          type: 'warning',
          title: 'Caution',
          text: 'Are you sure to delete this trip ? (this can\'t be undone)',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          showLoaderOnConfirm: true,
          allowOutsideClick: false
          }).then((result) => {
            if (result.value) 
            {
              swal.showLoading();
              $.ajax({
                type: "POST",
                url: "deleteTrip",
                data:'id='+id,
                success: function () {
                  swal({
                    type: 'success',
                    title: 'Trip has ',
                    html: 'deleted',
                    timer: 2000
                  }).then((result) => {
                    location.reload();
                  });
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error deleting!", "Please try again in few minute", "error");
                }
              });

            }

          });
    };

    // PHOTO id= '+photo_id+',  product_id= '+product_id+', filename= '+filename
    function deletePhoto(photo_id, product_id, filename)
    {
        swal({
          type: 'warning',
          title: 'Caution',
          text: 'Are you sure to delete this photo ? (this can\'t be undone) ',
          showCancelButton: true,
          confirmButtonText: 'Ok',
          showLoaderOnConfirm: true,
          allowOutsideClick: false
          }).then((result) => {
            if (result.value) 
            {
              swal.showLoading();
              // $.ajax({
              //   type: "POST",
              //   url: "deletePhoto",
              //   data: 'filename='+ filename,
              //   success: function () {
              //     swal({
              //       type: 'success',
              //       title: 'Photo has ',
              //       html: 'deleted',
              //       timer: 2000
              //     }).then((result) => {
              //         // location.reload();
              var updateCountPhotos = document.getElementById('PhotosNumber').value;
              updateCountPhotos = updateCountPhotos - 1;
              document.getElementById('PhotosNumber').value = updateCountPhotos;

                     $('#photo_id_'+photo_id).remove();

                     var ready_id_photo_todelete = document.getElementById("ready_id_photo_todelete").value;
                     if (ready_id_photo_todelete == "")
                     {
                        ready_id_photo_todelete = photo_id;
                     }
                     else
                     {
                       ready_id_photo_todelete = ready_id_photo_todelete + ', ' + photo_id;
                     }

                     document.getElementById("ready_id_photo_todelete").value = ready_id_photo_todelete;

                     swal({
                    type: 'success',
                    title: 'Photo has ',
                    html: 'deleted',
                    timer: 2000
                  });
              //     });
              //   },
              //   error: function (xhr, ajaxOptions, thrownError) {
              //       swal("Error deleting!", "Please try again in later", "error");
              //   }
              // });

            }

          });
    };

    //prevent enter input trip page
    $(window).keydown(function(event){
        if(event.keyCode == 13)
        {
          if (event.target.id == 'submitTrip')
          {
            return true;
          }
          else
          {
            event.preventDefault();
            return false;
          }
        }
      });

    function validateFormSubmit() {
      // var x = document.forms["myForm"]["fname"].value;
      // if (x == "") {
      //     alert("Name must be filled out");
      // }
      // alert('poco loco');
      submit = document.getElementById('submitTrip');
      submit.innerHTML = ' please wait ';
      $('#submitTrip').append('<i id="spinner" class="fa fa-spinner fa-spin"  ></i>');
      submit.disabled = true;
      // return false;
    };

    $('#submitTripi').keydown(function()
      {
        if (event.keyCode == 13) {
          // alert('asd');
          return true;
        }
        // submit = document.getElementById('submitTrip');
        // submit.innerHTML = ' please wait ';
        // $('#submitTrip').append('<i id="spinner" class="fa fa-spinner fa-spin"  ></i>');
        // submit.disabled = true;
      });

    function checkstatus(status)
    {
        if (status == '1')
        {
          swal({
                title:'Pending',
                text:'Request untuk menjadi travel agent belum di approve oleh Admin Rancupid',
                type:'warning',
                showConfirmButton:true,
          });
        }
    };

    var myDropzoneTripPhoto = new Dropzone(
        '#myDropzoneTripPhoto', //id of drop zone element 1
        {
            acceptedFiles: 'image/*',
            init: function () {
                this.on("maxfilesexceeded", function (data) {
                  notifyuploadsizefilemax();
                });

                // this.on("complete", function (data) {
                //     var res = eval('(' + data.xhr.responseText + ')');
                //     $('#newImage').text(res.Message);
                // });
                // this.on("maxfilesexceeded", function (data) {
                //     this.removeFile(data);
                // });
            }
        }
    );
    
    function updateProgressbar(item)
    {

      total_size_photo = document.getElementById('total_size_photo').value;
      // console.log('#1 '+total_size_photo);
      total_size_photo = Number(total_size_photo) + Number(item);
      
      document.getElementById('total_size_photo').value = total_size_photo;

      // console.log('#2 '+total_size_photo);
      var percent = (total_size_photo / 35000000) * 100;
      document.getElementById('maxsizephoto').style.width = percent+"%";
      sizePhotostatus = (total_size_photo/1000000);
      sizePhotostatus = sizePhotostatus.toFixed(2);
      document.getElementById('countmaxsizephoto').innerHTML = sizePhotostatus+'mb/ 35mb';
      var countphotos = document.getElementById('PhotosNumber').value;
      countphotos = Number(countphotos) + 1;
      document.getElementById('PhotosNumber').value = countphotos;
    };

    function removeProgressbar(itemR)
    {
      // console.log(itemR[0].file.size);
      // var sizePhotoR = 0;
      var sizePhoto = 0;
      for (var itR = 0; itR < itemR.length; itR++)
      {
        sizePhoto = sizePhoto + itemR[itR].file.size;        
        var percentR = (sizePhoto / 35000000) * 100;
        // console.log(percentR);
        document.getElementById('maxsizephoto').style.width = percentR+"%";
        sizePhotostatus = (sizePhoto/1000000);
        sizePhotostatus = sizePhotostatus.toFixed(2);
        document.getElementById('countmaxsizephoto').innerHTML = sizePhotostatus+'mb/ 35mb';
      }

      if(itemR.length == 0)
      {
        document.getElementById('maxsizephoto').style.width = '0%';
        document.getElementById('countmaxsizephoto').innerHTML = '0mb/ 35mb';
      }
    };

    function checkQuantityPhotos(id)
    {
      var numberPhotos = null;
      var countphotos = document.getElementById('PhotosNumber').value;
            // alert(countphotos);
      if(id != 'baru')
      {
        numberPhotos =  countphotos;
      }
      else if(id == 'baru')
      {
        if (countphotos == null)
        {
          numberPhotos = 0;         
        }
        else
        {
          numberPhotos = countphotos;          
        }
      }

      return numberPhotos;
    };

    function notifyuploadtypefile()
    {
      // position: 'top-right',
      swal({
        type: 'error',
        title: 'Failed',
        text: 'You cant upload this type file, only image file type accepted (jpg, png)',
        showConfirmButton: true,
        timer: 2000
      });
    };

    function notifyuploadsizefilemax(trigger)
    {
      // position: 'top-right',
      if (trigger == 'total')
      {
        swal({
          type: 'error',
          title: 'Failed',
          text: 'Failed to upload due to total size photos already reach 35mb',
          showConfirmButton: true,
          timer: 4000
        });        
      }
      else if (trigger == 'count')
      {
        swal({
          type: 'error',
          title: 'Failed',
          text: 'Failed to upload due to maximal photos already reach 5pcs, delete another photos for continue',
          showConfirmButton: true
        });
      }
    };
  // ================== end agent function ===================

  // ==================== date trip function ================
    $('#reservationtime').daterangepicker(
      {
        timePicker: true,
        timePickerIncrement: 30,
        locale:{
          format: 'DD/MM/YYYY' 
        }
      })

    $("#add_new_date").click(function()
    {
      $("#reservationtime").click();
    });

    $('#reservationtime').on('apply.daterangepicker', function(ev, picker) 
    {
        dt = document.getElementById('date_trip_number').value;
        if ( dt == "")
        {
          dt = 1;
        }
        else
        {
          dt = parseInt(dt) + 1;          
        }
        // alert(dt);
        // $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        var DateTrip = picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY');
        var datelist = document.getElementById('date_list');
        var newDate = document.createElement('div');
        newDate.setAttribute('class','alert alert-success alert-dismissible');

        //udt = urutan date trip
        //cdk = cari date kosong
        var udt = null;
        for (var cdk = 1; cdk <= dt; cdk++)
        {
          if(document.getElementById("date_trip_"+cdk) === null)
          {
            udt = cdk;
          }
        }

        if (udt == null)
        {
          newDate.setAttribute('id','date_trip_'+dt);
          newDate.innerHTML = 
          '<button type ="button" class="close btn_remove_dt" data-dismiss="alert" aria-hidden="true" style="color: white; opacity:1">&times;</button>'
          + DateTrip +
          '<input type="hidden" name="start_trip_'+dt+'" id="start_trip_'+dt+'" value="'+picker.startDate.format('YYYY/MM/DD')+'"></input>'+
          '<input type="hidden" name="end_trip_'+dt+'" id="end_trip_'+dt+'" value="'+picker.endDate.format('YYYY/MM/DD')+'"></input>'
          '<input type="hidden" name="ready_id_date_'+dt+'" id="ready_id_date_'+dt+'" value="">';
          datelist.appendChild(newDate);
        }
        else
        {
          newDate.setAttribute('id','date_trip_'+udt);
          newDate.innerHTML = 
          '<button type ="button" class="close btn_remove_dt" data-dismiss="alert" aria-hidden="true" style="color: white; opacity:1">&times;</button>'
          + DateTrip +
          '<input type="hidden" name="start_trip_'+udt+'" id="start_trip_'+udt+'" value="'+picker.startDate.format('YYYY/MM/DD')+'"></input>'+
          '<input type="hidden" name="end_trip_'+udt+'" id="end_trip_'+udt+'" value="'+picker.endDate.format('YYYY/MM/DD')+'"></input>'
          '<input type="hidden" name="ready_id_date_'+udt+'" id="ready_id_date_'+udt+'" value="">';
          datelist.appendChild(newDate);          
        }

        document.getElementById('date_trip_number').value = dt;
    });

    $(".btn_remove_dt").mousedown(function(){  
         var btn_remove_dt = $(this).attr("id");
         var ready_id = document.getElementById("ready_id_date_"+btn_remove_dt).value;
         var ready_id_date_todelete = null;
         ready_id_date_todelete = document.getElementById('ready_id_date_todelete').value;

         if (ready_id != "")
         {
            if (ready_id_date_todelete == "")
            {
              ready_id_date_todelete = ready_id;
            }
            else
            {
              ready_id_date_todelete = ready_id_date_todelete + ',' + ready_id;
            }
            document.getElementById('ready_id_date_todelete').value = ready_id_date_todelete;
         }

         // $('#row_'+btn_remove_dt+'').remove();
         var dt = document.getElementById('date_trip_number').value;
         dt = parseInt(dt) - 1;
         document.getElementById('date_trip_number').value = dt;
    });

    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
  // ==================== end date trip function =============

  // ==================== include icon function ====================

    function formatIncludeIcon(icon) {
        var originalOption = icon.element;
        return '<i class="fa ' + $(originalOption).data('icon') + '"></i> ' + icon.text;
    }

    $('#include_icon').select2({
        width: "100%",
        formatResult: formatIncludeIcon
    });

    $('#include_icon').on('select2:select', function()
    {
        ii = document.getElementById('include_icon_list_number').value;
        towrite = document.getElementById('ready_part_icon_towrite').value;
        id_include_icon = $(this).val();

        ready_part_icon_towrite = towrite.split(",");
        var newIcon = true;
        // console.log(id_include_icon);
        // check include icon yg sama = ciis
        for (var ciis = 0; ciis < ii; ciis++) {
          part_id_icon = document.getElementById("icon_id_"+ready_part_icon_towrite[ciis]).value;
          if( part_id_icon != id_include_icon )
          {
            newIcon = true;
          }
          else
          {            
            newIcon = false;
            // alert(part_id_icon +' -- '+id_include_icon + 'true');
            break;
          }
        }

        if (newIcon == true)
        {
            if( ii == "")
            {
              ii = parseInt(ii);
              ii = 1;
            }
            else
            {
              ii = parseInt(ii) + 1;
            }
            
            include_text = $('#include_icon :selected').text();
            
            // alert(ii);
            //uii = urutan iclude icon
            //cii = cari include icon
            var uii = null;
            for (var cii = 1; cii <= ii; cii++)
            {
                      if(document.getElementById("include_"+cii) === null)
                      {
                        uii = cii;
                      }
            }
            
            var includelist = document.getElementById('include_list');
            var newInclude = document.createElement('div');
            newInclude.setAttribute('class','alert alert-info alert-dismissible');
            
            if (uii == null)
            {
                      newInclude.setAttribute('id','include_'+ii);
                      newInclude.innerHTML = 
                      '<button type ="button" class="close btn_remove_ii" data-dismiss="alert" aria-hidden="true" style="color: white; opacity:1" >&times;</button>'
                      +'<img src="/storage/assets/Include_Icon/'+include_text+'.png" style="width: 20%;">'
                      +include_text
                      +'<input type="hidden" name="icon_id_'+ii+'" id="icon_id_'+ii+'" value="'+id_include_icon+'"></input>'
                      +'<input type="hidden" name="include_id_'+ii+'" id="include_id_'+ii+'" value=""></input>';
          
                      if (towrite == "")
                      {
                        document.getElementById("ready_part_icon_towrite").value = ii;
                      }
                      else
                      {
                        document.getElementById("ready_part_icon_towrite").value = towrite+','+ii;
                      }
            }
            else
            {
                      newInclude.setAttribute('id','include_'+uii);
                      newInclude.innerHTML = 
                      '<button type ="button" id="'+uii+'"class="close btn_remove_ii" data-dismiss="alert" aria-hidden="true" style="color: white; opacity:1" >&times;</button>'
                      +'<img src="/storage/assets/Include_Icon/'+include_text+'.png" style="width: 20%;">'
                      +include_text
                      +'<input type="hidden" name="icon_id_'+uii+'" id="icon_id_'+uii+'" value="'+id_include_icon+'"></input>'
                      +'<input type="hidden" name="include_id_'+uii+'" id="include_id_'+uii+'" value=""></input>';
          
                      if (towrite == "")
                      {
                        document.getElementById("ready_part_icon_towrite").value = uii;
                      }
                      else
                      {
                        document.getElementById("ready_part_icon_towrite").value = towrite+','+uii;
                      }
            }
            
            include_list.appendChild(newInclude);          
            document.getElementById("include_icon_list_number").value = ii;
        }
        else
        {
            swal({
              type: 'warning',
              title: 'Caution',
              text: 'Icon Already Added',
              showConfirmButton: true,
              timer: 2000
            });
        }

    });

    $(document).on('mousedown','.btn_remove_ii',function(e){
         var button_id_ii = $(this).attr("id");
         var id_ii_todelete = document.getElementById("include_id_"+button_id_ii).value;
         var ready_id_ii_todelete = null;
         var towrite = document.getElementById('ready_part_icon_towrite').value;
         towrite = towrite.split(",");
         var towriteEnd = null;

         ready_id_ii_todelete = document.getElementById('include_icon_id_todelete').value;

         if (id_ii_todelete != null)
         {
            if (ready_id_ii_todelete == "")
            {
              ready_id_ii_todelete = id_ii_todelete;
            }
            else
            {
              ready_id_ii_todelete = ready_id_ii_todelete + ',' + id_ii_todelete;
            }
            document.getElementById('include_icon_id_todelete').value = ready_id_ii_todelete;
         }

         for (i = 0; i < towrite.length; i++)
          {
            if (button_id_ii != towrite[i])
            {
              if (towriteEnd == null)
              {
                towriteEnd = towrite[i];                
              }
              else
              {
                towriteEnd = towriteEnd+','+towrite[i];
              }
            }
          };

         var iiupdate = document.getElementById("include_icon_list_number").value;
         iiupdate = parseInt(iiupdate) - 1;
         document.getElementById("include_icon_list_number").value = iiupdate;
         document.getElementById('ready_part_icon_towrite').value = towriteEnd;
    });
  // ==================== end include icon function ================

  //===================== recommendations function =================
    $('#TypeItemRec').on('select2:select', function()
    {
        var id_rec_type = $(this).val();
        alert('id_rec_type');
    });
  //===================== end reccomendation function ==============

  //================== image popup gallery trip ======================
    $(document).on("click", ".img-c.active", function() {
      let copy = $(this);
      copy.removeClass("positioned active").addClass("postactive");
      setTimeout(function() {
        copy.remove();
      }, 500);
    });

    function toggleEditGallery()
    {
      var edit = $('input[name="edit[]"]:checked').length > 0;
      var gal = document.getElementsByClassName('img-c');

      if( edit == true)
      {
        for (var i = 0; i < gal.length; i++)
        {
          // var asd = document.getElementsByClassName('img-w')[i].style.transform;
          // console.log(asd);
          document.getElementsByClassName('img-w')[i].style.transform = "none";
          document.getElementsByClassName('photo-action')[i].style.visibility = "visible";
        }
      }
      else
      {
        for (var i = 0; i < gal.length; i++)
        {
          document.getElementsByClassName('img-w')[i].style.transform = "scale(1.08)";
          // document.createStyleSheet().addRule('.img-c:hover', 'transform: scale(1.08);');
          document.getElementsByClassName('photo-action')[i].style.visibility = "hidden";
        }
      }
    };

    function markTempPhoto(namePhoto)
    {
      // console.log('ini mark '+ namePhoto);
      var sticky_id = document.getElementById('sticky_id').value;
      if(sticky_id != 'false')
      {
        $("input:checkbox.triggersticky").not($(this)).removeAttr("checked");
      }

      sticky_id = namePhoto;
      document.getElementById('sticky_id').value = sticky_id;
    };

    function deleteTempPhoto(namePhoto)
    {
      console.log('ini delete '+ namePhoto);
    };
  //================= end image popup gallery trip ====================

  //======================= tagging function ==========================
    edit = document.getElementById('edit_boolean').value;
    if ( edit == 'true')
    {
      $('#tag').select2().val({!! isset($tagged)?$tagged:'' !!}).trigger('change');
    }
    // $('#tag').select2().val( ["1","2","3"] ).trigger('change');

    $('#tag').select2({
        width: "100%",
        multiple: true,
        tags: true,
        tokenSeparators: [',', ' '],
        theme: "flat",
        sorter: function(data) {
            /* Sort data using lowercase comparison */
            return data.sort(function (a, b) {
                a = a.text.toLowerCase();
                b = b.text.toLowerCase();
                if (a > b) {
                    return 1;
                } else if (a < b) {
                    return -1;
                }
                return 0;
            });
        },
        createTag: function (params) {
          var term = $.trim(params.term);

          if (term === '') {

            return null;
          }

          return {
            id: term,
            text: term,
            newTag: true // add additional parameters
          }
        },
        insertTag: function (data, tag) {
            // Insert the tag at the end of the results
            // console.log(tag);
            data.push(tag);

        }
        // ajax: {
        //     type: "POST",
        //     url: contextPath + '/clientssearch',
        //     dataType: 'json',
        //     contentType: "application/json",
        //     delay: 250,
        //     data: function (params) {
        //         return  JSON.stringify({
        //             term: params.term
        //         });
        //     },
        //     processResults: function (data) {
        //         return {
        //             results: $.map(data, function (item, i) {
        //                 return {
        //                     text: item,
        //                     id: i
        //                 }
        //             })
        //         };
        //       }
        //     }
    });
  //==================== end tagging function =========================

  $('input[type="checkbox"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass   : 'iradio_flat-blue'
  })

  // ==================== detail price function ====================
    $('.price_trip').priceFormat({
      prefix: 'Rp. ',
      centsSeparator: ',',
      thousandsSeparator: '.',
    });
    // centsLimit: 0

    // new AutoNumeric('.price_trip', { allowDecimalPadding: AutoNumeric.options.allowDecimalPadding.never, formatOnPageLoad: false, minimumValue: '0', leadingZero: 'deny', maximumValue: '2147483647', decimalPlaces: '0' });

    // var lastnumberquantity = null;
    // var lastnumberinputrange = null;
    $(".addDetailPrice").click(function() {
        // urutankosong
        var uk = null;
        var max_range = null;
        pln = document.getElementById('price_list_number').value;
        // lnq = document.getElementById('lastnumberquantity').value;

        if(price_list_number == '1')
        {
          min_range_next = Number(document.getElementById('max_range_1').value) + 1;
          max_range_next = min_range_next + 1;    
          // lastnumberquantity = max_range_next;
        }
        else
        {
          lastmaxrange = document.getElementById('price_list_number').value;
          nextid = 'max_range_' + lastmaxrange;
          min_range_next = Number(document.getElementById(nextid).value) + 1;

          // min_range_next = Number(lastnumberquantity) + 1;
          max_range_next = min_range_next + 1;
          // lastnumberquantity = max_range_next;
        }
        // alert(max_range);

        for (var p = 1; p <= pln; p++)
        {
          if(document.getElementById("row_"+p) === null)
          {
            uk = p;
          }
        }

        if (pln < 4)
        {
          if (uk == null)
          {
            pln = parseInt(pln) + 1;
            $('#price_list').append(
                    '<tr id="row_'+pln+'">'+
                      '<input type="hidden" name="ready_id_'+pln+'" id="ready_id_'+pln+'" value=""></input>'+
                      '<td class="form-control">'+pln+'</td>'+
                      '<td><input class="form-control quantitymin" type="number" value="'+min_range_next+'" id="min_range_'+pln+'" name="min_range_'+pln+'" min="0" readonly="true"></td>'+
                      '<td><input class="form-control quantitymax" type="number" value="'+max_range_next+'" id="max_range_'+pln+'" name="max_range_'+pln+'" min="0"></td>'+
                      '<td><input type="text" value="0" id="price_'+pln+'" class="price_trip form-control" required></td>'+
                      '<td><button type="button" name="remove" id="'+pln+'" class="btn btn-danger btn_remove_pl remove_quantity">X</button></td>'+
                      '<input type="hidden" name="ready_price_'+pln+'" id="ready_price_'+pln+'" value=""></input>'+
                    '</tr>');
            document.getElementById("price_list_number").value = pln;
            // lastnumberinputrange = pln;
            statusbtnremove(pln);
          }
          else
          {
            pln = parseInt(pln) + 1;
            $('#price_list').append(
                    '<tr id="row_'+uk+'">'+
                      '<input type="hidden" name="ready_id_'+uk+'" id="ready_id_'+uk+'" value=""></input>'+
                      '<td class="form-control">'+uk+'</td>'+
                      '<td><input class="form-control quantitymin" type="number" value="'+min_range_next+'" id="min_range_'+uk+'" name="min_range_'+uk+'" min="0" readonly="true"></td>'+
                      '<td><input class="form-control quantitymax" type="number" value="'+max_range_next+'" id="max_range_'+uk+'" name="max_range_'+uk+'" min="0"></td>'+
                      '<td><input type="text" value="0" id="price_'+uk+'" class="price_trip form-control" required></td>'+
                      '<td><button type="button" name="remove" id="'+uk+'" class="btn btn-danger btn_remove_pl remove_quantity">X</button></td>'+
                      '<input type="hidden" name="ready_price_'+uk+'" id="ready_price_'+uk+'" value=""></input>'+
                    '</tr>');
            document.getElementById("price_list_number").value = pln;
            // lastnumberinputrange = pln;
            statusbtnremove(pln);
          }

          if (pln == 4)
          {
            $('.addDetailPrice').prop('disabled', true);
          }
        }
    });

    function statusbtnremove(number)
    {
      // alert(number);
      for (var i = 2; i <= number; i++)
      {
         $('.remove_quantity').prop('disabled', true);
      }
      
      var btnremoveid = '#'+number;
      $(btnremoveid).prop('disabled', false);
    };


    // $('#DestinationPhoto').bind('change',function(){
    $(document).on('change', '.quantitymax',function(){
      var id = $(this).attr("id");
      changemaxquantity(id);
    });
    // $('.quantitymax').change(function(){
    $(document).on('keydown', '.quantitymax', function(){
      var id = $(this).attr("id");
      changemaxquantity(id);
    });

    $(document).on('focusout', '.quantitymax', function(){
      var id = $(this).attr("id");
      changemaxquantity(id);
    });

    function changemaxquantity(id)
    {
      valueQuantityMax = document.getElementById(id).value;
      nextid = id.split("_");
      nextid = Number(nextid[2]) + 1;
      nextidr = 'min_range_' + nextid;
      nextidrMax = 'max_range_' + nextid;
      lastEditedMin = Number(valueQuantityMax) + 1;
      document.getElementById(nextidr).value = lastEditedMin;
      $('#'+nextidrMax).attr({"min" : lastEditedMin});

      // CheckValidityQuantityRange(nextid);
    };

    function CheckValidityQuantityRange(lastEditedID)
    {
      pln = document.getElementById('price_list_number').value;
      // for (var i = lastEditedID; i <= pln; i++) 
      {
        // alert(lastEditedID);
        lastIDEditedMax = 'max_range_' + lastEditedID;
        lastIDEditedMin = 'min_range_' + lastEditedID;

        lastEditedMax = document.getElementById(lastIDEditedMax).value;
        lastEditedMin = document.getElementById(lastIDEditedMin).value;
        // console.log(lastEditedMin);
        if(lastEditedMin > lastEditedMax)
        {
          document.getElementById(lastIDEditedMax).value = Number(lastEditedMin) + 1;
        }
      }

    };
    

    $(document).on('click', '.btn_remove_pl', function(){  
          var button_id = $(this).attr("id");

          // var total_to_delete = $('#total_to_delete').value;
          // total_to_delete = Number(total_to_delete) + 1;
          // document.getElementById("total_to_delete").value = total_to_delete;

          var id_todelete = document.getElementById('ready_id_'+button_id).value;
          var ready_id_todelete = document.getElementById('ready_id_todelete').value;
          if( ready_id_todelete == "")
          {
            ready_id_todelete = id_todelete;
          }
          else
          {
            ready_id_todelete = ready_id_todelete + ',' + id_todelete;
          }
          document.getElementById("ready_id_todelete").value = ready_id_todelete;

         $('#row_'+button_id).remove();
         var pln = document.getElementById("price_list_number").value;
         var lnq = document.getElementById('max_range_'+ (button_id-1) ).value;
         pln = parseInt(pln) - 1;
         document.getElementById("price_list_number").value = pln;
         // document.getElementById('lastnumberquantity').value = lnq;

         $('.addDetailPrice').prop('disabled', false);
         pln = document.getElementById('price_list_number').value;
         // lastnumberquantity = lastnumberquantity - 2;
         statusbtnremove(pln);
    });

    $(document).on('focusout', '.price_trip', function()
    {
      var price_id = 'ready_'+ $(this).attr("id");
      var ReadyPrice = $(this).unmask();
      // ReadyPrice = ReadyPrice.substring(0, ReadyPrice.length - 2);
      document.getElementById(price_id).value = ReadyPrice;
    });

    $(document).on('click', '.price_trip', function(){
        $(this).priceFormat({
          prefix: 'Rp. ',
          centsSeparator: ',',
          thousandsSeparator: '.'
        });
    });

    $(document).on('keydown', '.price_trip', function()
    {
      // alert('hi');
        $(this).priceFormat({
          prefix: 'Rp. ',
          centsSeparator: ',',
          thousandsSeparator: '.'
        });
    });

    $("#submit").click(function(){
      // alert('ehhllo');
      var nama = 'danadina';
      $.ajax({
          async:false,
          type: "POST",
          url: "addNewTrip",
          data:$('#price_list').serialize(),
          success:function(data)
          {
            // alert(data);
            console.log(data);
            // $('#form_addtrip')[0].reset();
          }
      });
    });

    $('#submita').click(function(){            
             $.ajax({  
                  url:"add_NewTrip",  
                  method:"POST",  
                  data:$('#price_list').serialize(),  
                  success:function(data)  
                  {  
                       alert(data);  
                       $('#price_list')[0].reset();  
                  }  
             });  
    });
  // ==================== end detail price function =================

  // =============== autocomplete place google ====================
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var mepo = new google.maps.Map(document.getElementById('mapMePo'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });


        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));
        var inputMepo = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-inputMepo'));


        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var typesMepo = document.getElementById('type-selectorMepo');
        mepo.controls[google.maps.ControlPosition.TOP_LEFT].push(inputMepo);
        mepo.controls[google.maps.ControlPosition.TOP_LEFT].push(typesMepo);


        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var autocompleteMepo = new google.maps.places.Autocomplete(inputMepo);
        autocompleteMepo.bindTo('bounds', mepo);


        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        var geocoderMepo = new google.maps.Geocoder;
        var infowindowMepo = new google.maps.InfoWindow();
        var markerMepo = new google.maps.Marker({
          mepo: mepo,
          anchorPoint: new google.maps.Point(0, -29)
        });


        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();

          var lat = place.geometry.location.lat();
          var lng = place.geometry.location.lng();

          addlatlng(lat, lng);
          // console.log('lat = ' + lat +'---'+ 'lng = ' + lng);
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            // window.alert("No details available for input: '" + place.name + "'");
            swal({
              type: 'warning',
              title: 'Caution',
              text: 'choose one item to fullfill address',
              showConfirmButton: true,
              timer: 2000
            });
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
          
          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });

          lat = document.getElementById('latitude_data').value;
          lng = document.getElementById('longitude_data').value;
          if (lat != "" && lng != "")
          {
            var Type = 'Trip';
            geocodeLatLng(geocoder, map, infowindow, Type);
          }        

        // mepo
        autocompleteMepo.addListener('place_changed', function() {
          infowindowMepo.close();
          markerMepo.setVisible(false);
          var placeMepo = autocompleteMepo.getPlace();
          var latMepo = placeMepo.geometry.location.lat();
          var lngMepo = placeMepo.geometry.location.lng();

          addlatlngMepo(latMepo, lngMepo);
          // console.log('lat = ' + lat +'---'+ 'lng = ' + lng);
          if (!placeMepo.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            // window.alert("No details available for input: '" + place.name + "'");
            swal({
              type: 'warning',
              title: 'Caution',
              text: 'choose one item to fullfill address',
              showConfirmButton: true,
              timer: 2000
            });
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (placeMepo.geometry.viewport) {
            mepo.fitBounds(placeMepo.geometry.viewport);
          } else {
            mepo.setCenter(placeMepo.geometry.location);
            mepo.setZoom(17);  // Why 17? Because it looks good.
          }
          markerMepo.setIcon(/** @type {google.maps.Icon} */({
            url: placeMepo.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          markerMepo.setPosition(placeMepo.geometry.location);
          markerMepo.setVisible(true);

          var addressMepo = '';
          if (placeMepo.address_components) {
            addressMepo = [
              (placeMepo.address_components[0] && placeMepo.address_components[0].short_name || ''),
              (placeMepo.address_components[1] && placeMepo.address_components[1].short_name || ''),
              (placeMepo.address_components[2] && placeMepo.address_components[2].short_name || '')
            ].join(' ');
          }
          
          infowindowMepo.setContent('<div><strong>' + placeMepo.name + '</strong><br>' + addressMepo);
          infowindowMepo.open(mepo, markerMepo);
        });

          latMepo = document.getElementById('latitude_data_mepo').value;
          lngMepo = document.getElementById('longitude_data_mepo').value;
          if (latMepo != "" && lngMepo != "")
          {
            var Type = 'Mepo';
            geocodeLatLng(geocoderMepo, mepo, infowindowMepo, Type);
          }

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        // function setupClickListener(id, types) {
        //   var radioButton = document.getElementById(id);
        //   radioButton.addEventListener('click', function() {
        //     autocomplete.setTypes(types);
        //   });
        // }

        // setupClickListener('changetype-all', []);
        // setupClickListener('changetype-address', ['address']);
        // setupClickListener('changetype-establishment', ['establishment']);
        // setupClickListener('changetype-geocode', ['geocode']);
      }

      function geocodeLatLng(geocoder, map, infowindow, Type)
      {
        // var input = document.getElementById('latlng').value;
        // var latlngStr = input.split(',', 2);
        if ( Type == 'Trip')
        {
          lat = document.getElementById('latitude_data').value;
          lng = document.getElementById('longitude_data').value;          
        }
        else if ( Type == 'Mepo' )
        {
          lat = document.getElementById('latitude_data_mepo').value;
          lng = document.getElementById('longitude_data_mepo').value;
        }

        var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[1]) {
              map.setZoom(11);
              var marker = new google.maps.Marker({
                position: latlng,
                map: map
              });
              infowindow.setContent(results[1].formatted_address);
              infowindow.open(map, marker);
              // alert('asd');
            } else {
              window.alert('No results found');
            }
          } else {
            window.alert('Geocoder failed due to: ' + status);
          }
        });
      };

      function addlatlng (lat , lng){
          // console.log('sudah dikirim (lat = ' + lat +'---'+ 'lng = ' + lng+' )');
          document.getElementById('latitude_data').value = lat;
          document.getElementById('longitude_data').value = lng;
      };

      function addlatlngMepo (latMepo , lngMepo){
          // console.log('sudah dikirim (lat = ' + lat +'---'+ 'lng = ' + lng+' )');
          document.getElementById('latitude_data_mepo').value = latMepo;
          document.getElementById('longitude_data_mepo').value = lngMepo;
      };
  // =============== autocomplete place google ====================

  // ==================== make an order ===========================
  // ================== end make an order =========================  

  $(function () 
  {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('Itenerary');
    CKEDITOR.replace('Include');
    CKEDITOR.replace('Exclude');
    CKEDITOR.replace('Destination');

    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5();

    // replace class select2 with feature 
    // $('.select2').select2();

    $(".img-w").each(function() {
      $(this).wrap("<div class='img-c'></div>")
      let imgSrc = $(this).find("img").attr("src");
       $(this).css('background-image', 'url(' + imgSrc + ')');
    });
            
  
    $(".img-c").click(function() {
      var editGallery = $('input[name="edit[]"]:checked').length > 0;
      if( editGallery == false)
      {
        let w = $(this).outerWidth()
        let h = $(this).outerHeight()
        let x = $(this).offset().left
        let y = $(this).offset().top
        
        
        $(".active").not($(this)).remove()
        let copy = $(this).clone();
        copy.insertAfter($(this)).height(h).width(w).delay(500).addClass("active")
        $(".active").css('top', y - 8);
        $(".active").css('left', x - 8);
        
        setTimeout(function() {
          copy.addClass("positioned")
        }, 0)
      }
    });

    $(".checkmark").click(function(){
      // var oldId = $("input:checkbox.triggersticky").not($(this)).closest(".img-w").attr('id');
      var oldId = document.getElementById("sticky_id").value;
      if(oldId != 'false')
      {
        // document.getElementById('sticky_'+oldId).value = 'false';
        $("input:checkbox.triggersticky").not($(this)).removeAttr("checked");
      }

      var change = $(this).closest(".mark-photo").find('input');
      // console.log(change);
      change.attr("checked", "true");
      var id = $(this).attr('id');
      // document.getElementById('sticky_'+id).value = 'true';
      document.getElementById("sticky_id").value = id;
    });

  });

  function checkmarkTemp(aselole)
  {
    var tempMark = $(this).closest(".dz-preview").find('.rt-option');
    console.log( tempMark );
    // tempMark.attr("checked", "true");
    event.preventDefault();
            // dz-preview
  };

</script>