<!-- Content Header (Page header) -->



    <section class="content-header">
      <h1>
        <?php
        $r = App\User::select('roles')->where('id', Auth::user()->id)->first();
        ?>

        @if($r->roles == 'sa')
          
          Dashboard
          <small>Admin Rancupid</small>

        @elseif($r->roles == 'v' )
        
          <?php
            $TA = App\Agent::select('travel_agent_name')->where('user_id_1', Auth::user()->id)->first();
          ?>
          {{ $TA->travel_agent_name }}
        
        @endif

        <!-- yield('contentheader_title', 'Page Header here') -->
        <!-- <small>yield('contentheader_description')</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>