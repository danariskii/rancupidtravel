<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <!-- <a href="https://github.com/acacha/adminlte-laravel"></a><b>admin-lte-laravel</b></a>. {{ trans('adminlte_lang::message.descriptionpackage') }} -->
	    <strong>Copyright &copy; 2018 <a href="/">RancupidTravel.com</a>.</strong> 
    </div>
    <!-- Default to the left -->
    <strong>Let's Travel the World . . .</strong> 
</footer>