<script src="{{ asset('theme/js/jquery-2.1.4.min.js') }}"></script>
<script src="{{ asset('theme/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('theme/js/idangerous.swiper.min.js') }}"></script>
<script src="{{ asset('theme/js/jquery.viewportchecker.min.js') }}"></script>
<script src="{{ asset('theme/js/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('theme/js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('theme/js/all.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.5/sweetalert2.all.min.js"></script>
<script src="{{ asset('bower_components/easyAutocomplete/jquery.easy-autocomplete.min.js') }}"></script>

<script src='http://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.0/handlebars.min.js'></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/list.js/1.1.1/list.min.js'></script>
<script src="{{ asset('bower_components/chat/js/chat.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function()
    {
      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
      });

		$('.accordion-chooser a').on('click',function()
			{
				// if($(this).hasClass('active'))
				// 	return false;
				var filter=$(this).data('fifter');
				var accordion=$(this).parents('.accordion-filter').find('.accordion');
				// $(this).siblings('.active').removeClass('active');
				// $(this).addClass('active');

				if(filter=="*")
				{
				// 	accordion.find('.acc-panel').show();
					$(' .acc-icon').closest('.accordion').find('.active').removeClass('active');
					$(' .acc-icon').closest('.accordion').find('.acc-body').slideUp('slow');

					// $('.Overview .acc-icon').toggleClass('active');
					// $('.Overview .acc-icon').closest('.accordion').find('.acc-body').slideDown('slow');
					return false;
				}
				// else
				// {
				// 	accordion.find('.acc-panel:not('+filter+')').hide();accordion.find(filter).show();
				// }

				// return false;
				
				// alert(accordion);
				
				if($('.acc-icon').hasClass('active'))
				{
					$('.acc-icon').removeClass('active');
					$('.acc-icon').siblings('.acc-body').slideUp();
				}
				else
				{					
					$(' .acc-icon').closest('.accordion').find('.active').removeClass('active');
					$(' .acc-icon').closest('.accordion').find('.acc-body').slideUp('slow');
					$(filter+' .acc-icon').toggleClass('active');
					$(filter+' .acc-icon').siblings('.acc-body').slideToggle('slow');
					$(filter+' .acc-icon').closest('.accordion').find('.acc-body').slideDown('slow');
				}
			});

	// =========================== snap midtrans =======================

	    // document.getElementById('pay-buttoni').onclick = function(){
	    //     swal({
	    //       type: 'warning',
	    //       title: 'Caution',
	    //       text: 'Make sure your that items trip you want to buy ?',
	    //       showCancelButton: true,
	    //       confirmButtonText: 'Ok',
	    //       showLoaderOnConfirm: true,
	    //       allowOutsideClick: false
	    //       }).then((result) => {
	    //         if (result.value) 
	    //         {
	    //           swal.showLoading();
	    //           $.ajax({
	    //             type: "POST",
	    //             url: "makeOrder",
	    //             success: function () {
	    //               swal({
	    //                 type: 'success',
	    //                 title: 'Trip has ',
	    //                 html: 'deleted',
	    //                 timer: 2000
	    //               }).then((result) => {
	    //                 // location.reload();
	    //               });
	    //             },
	    //             error: function (xhr, ajaxOptions, thrownError) {
	    //                 swal("Error deleting!", "Please try again in few minute", "error");
	    //             }
	    //           });

	    //         }

	    //       });
	    // };

    // ========================= end snap midtrans =====================


    });

var options = {

  url: "resources/sheroes.json",

  getValue: "name",

  cssClasses: "sheroes",

  template: {
    type: "iconRight",
    fields: {
      iconSrc: "icon"
    }
  },

  list: {
    showAnimation: {
      type: "slide"
    },
    hideAnimation: {
      type: "slide"
    }
  }

};

// $("#Searchi").easyAutocomplete(options);

    function Search()
    {
    	var keyword = document.getElementById("Search").value;
    	history.replaceState({}, 'some title', '/');
        window.location.href = "search?keyword="+keyword;
    	// var parameter = window.location.pathname;
    	// alert(keyword);
        // window.location.href = "search/"+keyword;
    }

    function SearchSide()
    {
		var keyword = document.getElementById("SearchSide").value;
    	history.replaceState({}, 'some title', '/');
        window.location.href = "search?keyword="+keyword;
    }

    function addtocart(id_product)
    {
      $.ajax({
          async:false,
          type: "POST",
          url: "/addtocart",
          data:'id_product='+ id_product,
          success:function(data)
          {
          	if ( data == 'success')
          	{
				swal({
				  title: 'Trip Booked',
				  text: "Trip Already in Cart",
				  type: 'success',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#23b0e8',
				  confirmButtonText: 'Go to Cart',
				  cancelButtonText: 'Continue Shopping',
				  confirmButtonClass: 'btn btn-success',
				  cancelButtonClass: 'btn btn-info',
				  buttonsStyling: false
				}).then((result) => {
				  if (result.value) {
				    // swal(
				    //   'Deleted!',
				    //   'Your file has been deleted.',
				    //   'success'
				    // )
				    window.location.href = '/cart';
				  // result.dismiss can be 'cancel', 'overlay',
				  // 'close', and 'timer'
				  } 
				  // else if (result.dismiss === 'cancel') {
				  //   swal(
				  //     'Cancelled',
				  //     'Your imaginary file is safe :)',
				  //     'error'
				  //   )
				  // }
				})
          	}
          	else
	        {
	          	loginfirst();
	        }

          }
      });        
    };

    function addtowishlist(id_product)
    {
	      $.ajax({
	          async:false,
	          type: "POST",
	          url: "/addtowishlist",
	          data:'id_product='+ id_product,
	          success:function(data)
	          {

	          	// console.log(data);
	          	if (data == 'success') 
	          	{
		      		swal({
					  // position: 'top-right',
					  type: 'success',
					  title: 'Trip added to Wishlist',
					  showConfirmButton: false,
					  timer: 1500
					});
	          	}
	          	else
	          	{
	          		loginfirst();
	          	}

	          }
	      });
    };

    function loginfirst()
    {
		swal({
		// position: 'top-right',
			type: 'warning',
			text: 'Sign In or Sign Up First to Continue',
			timer: 3000,
			showConfirmButton: true,
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Go to Login/ Sign-Up Page',
			cancelButtonText: 'close'
		}).then((result) => {
		if (result.value) 
		{
			window.location.href = '/login';
		} 
		})
    }

    function paymentmethod()
    {
    	window.location.href = 'https://app.sandbox.midtrans.com/snap/v1/transactions';
    }
	
</script>