<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<meta name="apple-mobile-web-app-capable" content="yes"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
		<meta name="format-detection" content="telephone=no"/>
		<!-- <script src="../../cdn-cgi/apps/head/1sZCq7BECvDgKDoo_5GdSy-HJEo.js"></script> -->
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	
		<link rel="shortcut icon" href="{{ asset('/storage/assets/Icon/Icon.png') }}"/>

		<link href="{{ asset('/theme/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('/theme/css/jquery-ui.structure.min.css') }}" rel="stylesheet">
		<link href="{{ asset('/theme/css/jquery-ui.min.css') }}" rel="stylesheet">
		<link href="{{ asset('/theme/css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('/fonts/font-awesome.min.css') }}" rel="stylesheet">

		<link href="{{ asset('/css/Rcustom.css')}}" rel="stylesheet" type="text/css">

		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.0.5/sweetalert2.min.css">
	    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/easyAutocomplete/easy-autocomplete.themes.min.css')}}">


	    <link rel="stylesheet" type="text/css" href="{{ asset('/bower_components/chat/css/style_chat.css') }}">

		<!-- <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/> -->
		<!-- <link href="css/jquery-ui.structure.min.css" rel="stylesheet" type="text/css"/> -->
		<!-- <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css"/> -->
		<!-- <link rel="stylesheet" href="../../../maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> -->
		<!-- <link href="css/style.css" rel="stylesheet" type="text/css"/> -->

		<title>Rancupid's - @yield('htmlheader_title', 'Home')</title>
        <!-- <link rel="shortcut icon" href="{{ asset('themes/images/icons/favicon.png') }}"> -->
	</head>

<body data-color="theme-3">
	
<div class="loading">
<div class="loading-center">
<div class="loading-center-absolute">
<div class="object object_four"></div>
<div class="object object_three"></div>
<div class="object object_two"></div>
<div class="object object_one"></div>
</div>
</div>
</div>


@include('layouts.header')


@yield('main-content')


@include('layouts.footer')

@section('scripts')
    @include('layouts.partials.scripts-frontend')
@show

</body>

<!-- Mirrored from demo.nrgthemes.com/projects/travel/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 07 Aug 2017 10:10:10 GMT -->
</html>
