<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <title> config('app.name', 'Laravel') }}</title> -->
    <title>Login</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

<style type="text/css">
    body {
    background-image: url( "{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" );
    background-repeat: no-repeat;
    background-position: center center;
    background-attachment: fixed;
    -moz-background-size: cover;
    background-size: cover;
    }

    #login
    {
        width: 600px;
    }
</style>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                         <!-- config('app.name', 'Laravel') }} -->
                         <img src="{{ asset('/storage/assets/Icon/Logo.png') }}" alt="Rancupid Travel" style="width: 60%;">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->

    <!-- datepicker -->
<script src="js\simple_date_picker\jquery.date-dropdowns.min.js"></script>

<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>

    <script type="text/javascript">
        //Date picker
    //     $('#datepicker').datepicker({
    // dateFormat    : 'dd - MM - yy',
    // altField  : '#alt-datepicker',
    // altFormat : 'yy-mm-dd'
    //     });


         $("#datepicker").dateDropdowns({
             minAge: 17,
             required: true,
             defaultDateFormat: 'yyyy-mm-dd'
         });
    </script>
</body>
</html>
