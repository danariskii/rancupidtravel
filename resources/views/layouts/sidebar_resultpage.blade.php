		<div class="col-xs-12 col-sm-4 col-md-3">
			<div class="sidebar bg-white clearfix">
				<div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2">search</h4>
					<div class="search-inputs">
						<div class="form-block clearfix">
							<div class="input-style-1 b-50 color-3">
								<img src="img/loc_icon_small_grey.png" alt="">
								<input id="SearchSide" type="text" placeholder="Where do you want to go?">
							</div>
						</div>
					</div>
					<input type="submit" class="c-button b-40 bg-blue-2 hv-blue-2-o" value="search" onclick="SearchSide()">
				</div>

				<!-- type trip -->
				<div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2">Type Trip</h4>
					<div class="sidebar-rating">
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-1" type="radio" name="checkbox" value="climat control">
							<label class="clearfix" for="text-1">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text" style="color: black">All</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-2" type="radio" name="checkbox" value="climat control">
							<label class="clearfix" for="text-2">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text" style="color: black">Open Trip</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-3" type="radio" name="checkbox" value="climat control">
							<label class="clearfix" for="text-3">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text" style="color: black">Private Trip</span>
							</label>
						</div>
					</div>
				</div>
				<div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2" style="margin-bottom: 0;">price range </h4><h6 style="margin: 0 0 2em 0">(in thousand rupiah)</h6>
					<div class="slider-range color-1 clearfix" data-counter="Rp" data-position="start" data-from="100" data-to="600" data-min="0" data-max="1000">
						<div class="range"></div>
						<input type="text" class="amount-start" readonly value="$0">
						<input type="text" class="amount-end" readonly value="$1500">
					</div>
					<!-- <input type="submit" class="c-button b-40 bg-blue-2 hv-blue-2-o" value="search"> -->
				</div>
				<!-- <div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2">star rating</h4>
					<div class="sidebar-rating">
						<div class="input-entry color-1">
							<input class="checkbox-form" id="star-5" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="star-5">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="rate">
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
							</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="star-4" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="star-4">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="rate">
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
							</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="star-3" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="star-3">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="rate">
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
							</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="star-2" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="star-2">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="rate">
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
							</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="star-1" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="star-1">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="rate">
								<span class="fa fa-star color-yellow"></span>
							</span>
							</label>
						</div>
					</div>
				</div> -->
				<!-- <div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2">Facility</h4>
					<div class="sidebar-rating">
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-1" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-1">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Pet allowed</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-2" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-2">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Groups allowed</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-3" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-3">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Tour guides</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-4" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-4">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Access for disabled</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-5" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-5">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Pet allowed</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-6" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-6">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Groups allowed</span>
							</label>
						</div>
						<div class="input-entry color-1">
							<input class="checkbox-form" id="text-7" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="text-7">
							<span class="sp-check"><i class="fa fa-check"></i></span>
							<span class="checkbox-text">Access for disabled</span>
							</label>
						</div>
					</div>
				</div> -->
				<!-- <div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2">Review Score</h4>
					<div class="sidebar-score">
						<div class="input-entry type-2 color-2">
							<input class="checkbox-form" id="score-5" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="score-5">
								<span class="checkbox-text">
								5
									<span class="rate">
										<span class="fa fa-star color-yellow"></span>
									</span>
								</span>
							<span class="sp-check"><i class="fa fa-check"></i></span>
							</label>
						</div>
						<div class="input-entry type-2 color-2">
							<input class="checkbox-form" id="score-4" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="score-4">
							<span class="checkbox-text">
								4
								<span class="rate">
									<span class="fa fa-star color-yellow"></span>
								</span>
							</span>
							<span class="sp-check"><i class="fa fa-check"></i></span>
							</label>
						</div>
						<div class="input-entry type-2 color-2">
							<input class="checkbox-form" id="score-3" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="score-3">
							<span class="checkbox-text">
								3
								<span class="rate">
									<span class="fa fa-star color-yellow"></span>
								</span>
							</span>
							<span class="sp-check"><i class="fa fa-check"></i></span>
							</label>
						</div>
						<div class="input-entry type-2 color-2">
							<input class="checkbox-form" id="score-2" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="score-2">
							<span class="checkbox-text">
								2
								<span class="rate">
									<span class="fa fa-star color-yellow"></span>
								</span>
							</span>
							<span class="sp-check"><i class="fa fa-check"></i></span>
							</label>
						</div>
						<div class="input-entry type-2 color-2">
							<input class="checkbox-form" id="score-1" type="checkbox" name="checkbox" value="climat control">
							<label class="clearfix" for="score-1">
							<span class="checkbox-text">
								1
								<span class="rate">
									<span class="fa fa-star color-yellow"></span>
								</span>
							</span>
							<span class="sp-check"><i class="fa fa-check"></i></span>
							</label>
						</div>
					</div>
				</div> -->
				<!-- categories -->
				<div class="sidebar-block">
					<h4 class="sidebar-title color-dark-2">categories activity</h4>
					<ul class="sidebar-category color-1">
					<li class="active">
						<a class="cat-drop" href="#">Islands <span class="fr">(-)</span></a>
						<!-- <ul>
							<li><a href="#">sea tours (785)</a></li>
							<li><a href="#">food tours (85)</a></li>
							<li><a href="#">romantic tours (125)</a></li>
							<li><a href="#">honeymoon tours (70)</a></li>
							<li><a href="#">mountain tours (159)</a></li>
						</ul> -->
					</li>
					<li>
						<a class="cat-drop" href="#">Snorkling <span class="fr">(-)</span></a>
					</li>
					<li>
						<a class="cat-drop" href="#">Hiking <span class="fr">(-)</span></a>
					</li>
					</ul>
				</div>
			</div>
		</div>