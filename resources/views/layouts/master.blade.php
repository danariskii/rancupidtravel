<!DOCTYPE html>

<html lang="en">
@section('htmlheader')
    @include('layouts.partials.htmlheader')
@show

<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-black layout-boxed sidebar-mini">

<div id="msg" style="font-size:largest; text-align:center; margin-top:30%;">
<!-- you can set whatever style you want on this -->
Loading, please wait...
</div>
<div id="body" class="wrapper" style="display:none;">

    @include('layouts.partials.mainheader')

    <!-- Left side column. contains the logo and sidebar -->
    @if(Auth::user()->roles == 'sa')

      @include('admin.layouts.sidebar')

    @elseif(Auth::user()->roles == 'v' && $Agent->status == '2')
    
      @include('agent.layouts.sidebar')

    @elseif($select == 'RegisterAgent' || $Agent->status == '1')
    
      @include('agent.layouts.sidebarRegist')
    
    @endif


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      
      <!-- Content Header (Page header) -->
      @include('layouts.partials.contentheader')

      <!-- Main content -->
      <section class="content">
        @yield('main-content')
      </section>
      <!-- /.content -->
    </div>
    
    <!-- /.content-wrapper -->
    @include('layouts.partials.footer')
    

    <!-- Control Sidebar right-->
    <!-- include('layouts.partials.controlsidebar') -->

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@section('scripts')
    @include('layouts.partials.scripts')
@show

</body>
</html>
