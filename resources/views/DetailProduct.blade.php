@extends('layouts.master-frontend')

@section('htmlheader_title')
    Detail Product
@endsection

@section('main-content')
<div class="inner-banner style-6" style="margin-top: 0em;">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8 col-md-offset-2">
					<ul class="banner-breadcrumb color-white clearfix">
					<!-- <li><a class="link-blue-2" href="#">home</a> /</li> -->
					<!-- <li><a class="link-blue-2" href="#">flights</a> /</li> -->
					<!-- <li><span>detail</span></li> -->
					</ul>
					<!-- <h2 class="color-white">flights for you</h2> -->
				</div>
			</div>
		</div>
	</div>
</div>
 
<div class="detail-wrapper">
	<div class="container">
		<div class="detail-header">
			<div class="row">
				<div class="col-xs-12 col-sm-8">
				<h2 class="detail-title color-dark-2">{{ $product->title }}</h2>
					<div class="detail-rate rate-wrap clearfix">
						<div class="rate">
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
						</div>
						<i>485 Rewies</i>
					</div>
				</div>
			</div>
		</div>
		<div class="row padd-90">
			<div class="col-xs-12 col-md-8">
				<div class="detail-content">
					<div class="detail-top">
						<?php 
							$photos = App\Product::find($product->id)->tripPhoto()->where('relation_id', $product->id)->where('sticky', '1')->get();
						?>
						@foreach($photos as $photo)
							<img class="img-responsive" src="{{ asset('/storage/product/'.$product->id.'/'.$photo->filename) }}" alt="">
						@endforeach
					</div>
					<div class="detail-content-block clearfix">
						<?php $dateTrips = App\product::find($product->id)->dateProduct()->orderBy('start_trip_date', 'asc')->get(); 
							$month_group = null;
						?>						
						<h3>Available Trip (2017)</h3>
						<div class="col-xs-12 col-md-12">
							@foreach ($dateTrips as $dateTrip )
								<?php
									$start_trip_date = date("d /M /Y", strtotime($dateTrip->start_trip_date));
									$end_trip_date = date("d /M /Y", strtotime($dateTrip->end_trip_date));
									$month = date("M", strtotime($dateTrip->end_trip_date));
								 ?>
								@if($month_group == null)
								<div class="col-xs-6 col-md-6">
									<h4>{{ $month }}</h4>
									<ul style="color: black; width:100%;">
										<li>{{ $start_trip_date }}  -  {{ $end_trip_date }}</li>
									<?php $month_group = $month ?>
								@elseif($month_group != $month)
									</ul>
								</div>
								<div class="col-xs-6 col-md-6">
									<h4>{{ $month }}</h4>
									<ul style="color: black; width:100%;">
										<li>{{ $start_trip_date }}  -  {{ $end_trip_date }}</li>
									<?php $month_group = $month ?>
								@else($month_group == $month)
										<li>{{ $start_trip_date }}  -  {{ $end_trip_date }}</li>
								@endif
							@endforeach
								</ul>
							</div>
						</div>
					</div>
					<!-- <div class="detail-content-block clearfix">
						<h3>Available Trip (2018)</h3>
						<div class="col-xs-6 col-md-6">
							<h4>Januari</h4>
							<ul style="color: black;">
								<li>10 - 13 Januari</li>
								<li>20 - 23 Januari</li>
								<li>29 - 31 Januari</li>
							</ul>							
						</div>
						<div class="col-xs-6 col-md-6">
							<h4>February</h4>
							<ul style="color: black;">
								<li>10 - 13 February</li>
								<li>20 - 23 February</li>
								<li>29 - 31 February</li>
							</ul>							
						</div>
					</div> -->
	<div class="detail-content-block">
		<h3>Detail Trip</h3>
		<div class="accordion style-4">
			<!-- itenerary -->
			<div class="acc-panel">
				<div class="acc-title">Itenerary Detail</div>
				<div class="acc-body" style="display: block; color: black;">
					{!! $product->text_1 !!}
					<!-- <h5>Day 1</h5>
					<p>Mauris posuere diam at enim malesuada, ac malesuada erat auctor. Ut porta mattis tellus eu sagittis. Nunc maximus ipsum a mattis dignissim. Suspendisse id pharetra lacus, et hendrerit mi. Praesent at vestibulum tortor. Ut porta mattis tellus eu sagittis. Nunc maximus ipsum a mattis dignissim.</p>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<ul>
								<li>11.00 Penjemputan di bandara menuju pelabuhan</li>
								<li>Perjalanan ke pulau derawan menggunakan Speed Boat</li>
								<li>Menuju ke penginapan dan persiapan kegiatan</li>
								<li>Permainan banana boat</li>
								<li>Snorkling dan foto bersama penyu</li>
								<li>Menikmati Indahnya Sunset</li>
								<li>Kembali ke penginapan dan mandi</li>
								<li>Acara bebas makan malam (disediakan)</li>
								<li>21.00- Istirahat</li>
							</ul>
						</div>
					</div>

					<h5>Day 2</h5>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<ul>
								<li>06.00 Bangun pagi dilanjutkan dengan sarapan pagi (kita sediakan)</li>
								<li>Persiapan untuk memulai kegiatan wisata</li>
								<li>Menuju pulau maratua</li>
								<li>Kegiatan di Pulau maratua, snorkling, dan foto-foto</li>
								<li>Perjalanan menuju Pulau Kakaban</li>
								<li>Menuju ke danau ikan pulau kakaban (optional)</li>
								<li>Kegiatan di danau ikan pulau kakaban (optional)</li>
								<li>makan siang (kita sediakan)</li>
								<li>Menuju danau ubur-ubur tanpa sengat di pulau kakaban</li>
								<li>Aktifitas di danau ubur-ubur, berenang, snorkling foto-foto</li>
								<li>Menuju Pulau Sangalaki</li>
								<li>Kegiatan di pulau sangalaki, snorkling, melihat penangkaran penyu dan bermain di pantai</li>
								<li>jika beruntung akan ada kegiatan berenang bersama pari manta</li>
								<li>Kegiatan di Spot Snapper, snorkling dan foto-foto</li>
								<li>Kegiatan di Pulau Gusung, foto-foto dan bermain pasir</li>
								<li>Kembali ke Pulau Derawan</li>
								<li>Mandi dilanjut dengan makan malam (disediakan)</li>
								<li>Istirahat sejenak</li>
								<li>acara bebas atau Ke pantai Derawan Dive Resort untuk melihat proses penyu bertelur jika beruntung dan musim penyu bertelur</li>
								<li>22.00- Kembali ke Penginapan dan Istirahat</li>
							</ul>
						</div>
					</div>

					<h5>Day 3</h5>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<ul>
								<li>06.00 Bangun pagi dilanjutkan dengan sarapan pagi (kita sediakan)</li>
								<li>Acara bebas</li>
								<li>Persiapan pulang</li>
								<li>Perjalanan kembali ke Tarakan</li>
								<li>14.00- tiba di bandara dan Perpisahan</li>

							</ul>
						</div>
					</div> -->
				</div>
			</div>

			<!-- include -->
			<div class="acc-panel">
				<div class="acc-title"></span>Detail Include </div>
				<div class="acc-body" style="display: block; color: black;">
					{!! $product->text_2 !!}
				</div>
			</div>

			<!-- exclude -->
			<div class="acc-panel">
				<div class="acc-title"></span>Detail Exclude</div>
				<div class="acc-body" style="display: block; color: black;">
					{!! $product->text_3 !!}
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

	<div class="col-xs-12 col-md-4" style="margin: 0 0 0em 0; ">
		<?php $lowestPrice = App\product::find($product->id)->lowestPrice()->select('price')->min('price'); ?>
			<div class="detail-price color-dark-2">price from <span class="color-dr-blue"> Rp. {{ $lowestPrice }}</span> /person</div>
		<a onclick="addtocart('{{$product->id}}')" class="c-button  hv-transparent" style="float: right; background-color: red; color: white;" ><span>Book Now</span></a>
	</div>
	<div class="col-xs-12 col-md-4">
		<div class="right-sidebar">
			<!-- <div class="detail-logo bg-grey-2"> -->
				<!-- <div class="detail-logo-name">Jelajah Indonesia Travel</div> -->
				<!-- <img src="{{ asset('/img/intro01.png') }}" alt=""> -->
			<!-- </div> -->

			<!-- Information Agent -->
			<div class="detail-block bg-red-3">
				<h4 class="color-white" style="text-align: center;">Information Agent</h4>
				<div class="details-desc" style="text-align: center;">
					<p><img src="{{ asset('/storage/assets/Include_Icon/default.png') }}" width="65;"></p>
					<p class="color-white"><strong><span class="color-white">{{ $agent->travel_agent_name }}</span></strong></p>
					<p class="color-white"> 
						<span class="fa fa-star color-yellow"></span>
						<span class="fa fa-star color-yellow"></span>
						<span class="fa fa-star color-yellow"></span>
						<span class="fa fa-star color-yellow"></span>
						<!-- <span class="fa fa-star color-yellow"></span> -->
					</p>
					<!-- <p class="color-white">Type Trip: <span class="color-white">Open Trip</span></p> -->
					<!-- <p class="color-white">Next Trip Date: <span class="color-white">july 19th to july 29th 2017</span></p> -->
					<!-- <p class="color-pink">FLIGHT TYPE: <span class="color-white">ECONOMY</span></p> -->
					<!-- <p class="color-pink">FARE TYPE: <span class="color-white">REFUNDABLE</span></p> -->
					<!-- <p class="color-pink">CANCELLATION: <span class="color-white">$78 / PERSON</span></p> -->
					<!-- <p class="color-pink">FLIGHT CHANGE: <span class="color-white">$53 / PERSON</span></p> -->
					<!-- <p class="color-pink">SEATS & BAGGAGE: <span class="color-white">EXTRA CHARGE</span></p> -->
					<!-- <p class="color-pink">INFLIGHT FEATURES: <span class="color-white">AVAILABLE</span></p> -->
					<!-- <p class="color-pink">BASE FARE: <span class="color-white">$320.00</span></p> -->
					<!-- <p class="color-pink">TAXES & FEES: <span class="color-white">$300.00</span></p> -->
					<!-- <p class="color-pink">TOTAL PRICE: <span class="color-white">$620.00</span></p> -->
				</div>
				<div class="details-btn">
					<!-- <a href="#" class="c-button b-40 bg-tr-1 hv-white"><span>Detail</span></a> -->
					<a href="#" class="c-button  bg-white hv-transparent"><span>Favorite Agent</span></a>
					<a href="#" class="c-button  bg-white hv-transparent"><span>Send Message</span></a>
				</div>
			</div>

			<!-- popular tours (iklan) -->
			<div class="popular-tours bg-grey-2">
				<h4 class="color-dark-2">popular tours</h4>
				<div class="hotel-small style-2 clearfix">
					<a class="hotel-img black-hover" href="#">
					<img class="img-responsive radius-3" src="{{ asset('/storage/gallery/1/1.jpg') }}" alt="">
					<div class="tour-layer delay-1 radius-3"></div>
					</a>
					<div class="hotel-desc">
					<h5><span class="color-dark-2">from <strong>Rp2.730.000</strong></span></h5>
					<h4>Flights to Derawan</h4>
					<div class="hotel-loc tt">Derawan</div>
					</div>
				</div>
				<div class="hotel-small style-2 clearfix">
					<a class="hotel-img black-hover" href="#">
					<img class="img-responsive radius-3" src="{{ asset('/storage/gallery/4/4.jpg') }}" alt="">
					<div class="tour-layer delay-1 radius-3"></div>
					</a>
					<div class="hotel-desc">
					<h5><span class="color-dark-2">from <strong>Rp2.730.000</strong></span></h5>
					<h4>Flights to Belitung</h4>
					<div class="hotel-loc tt">Belitung</div>
					</div>
				</div>
				<div class="hotel-small style-2 clearfix">
					<a class="hotel-img black-hover" href="#">
					<img class="img-responsive radius-3" src="{{ asset('/storage/gallery/3/3.JPG') }}" alt="">
					<div class="tour-layer delay-1 radius-3"></div>
					</a>
					<div class="hotel-desc">
					<h5><span class="color-dark-2">from <strong>Rp2.730.000</strong></span></h5>
					<h4>Flights to Tokyo</h4>
					<div class="hotel-loc tt">Tokyo</div>
					</div>
				</div>
			</div>

			<!-- <div class="sidebar-text-label bg-red-3 color-white">useful information</div> -->
			<!-- need help -->
			<div class="help-contact bg-grey-2">
				<h4 class="color-dark-2">Need Help?</h4>
				<i class="fa fa-envelope-o"></i><a href=""> info@rancupidtravel.com</a>
				<br>OR<br>
				<i class="fa fa-phone"></i><a> 021 570 7879</a>
			</div>
		</div>
	</div>
</div>
<div class="may-interested padd-90">
	<div class="row">
		<div class="top-baner arrows">
			<div class="swiper-container pad-15" data-autoplay="0" data-loop="1" data-speed="1000" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="4" data-add-slides="4">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="hotel-item style-7">
							<div class="radius-top">
								<img src="{{ asset('/storage/product/1/1.JPG') }}" alt="">
							</div>
							<div class="title">
								<h5>from <strong class="color-red-3">$860</strong> / person</h5>
								<h6 class="color-grey-3">one way flights</h6>
								<h4><b>Rancupid Explore Derawan</b></h4>
								<p>Book now and <span class="color-red-3">save 30%</span></p>
								<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
								<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
							</div>
						</div>
					</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
						<div class="radius-top">
							<img src="{{ asset('/storage/product/2/2.JPG') }}" alt="">
						</div>
						<div class="title">
							<h5>from <strong class="color-red-3">$860</strong> / person</h5>
							<h6 class="color-grey-3">one way flights</h6>
							<h4><b>Rancupid Explore Derawan</b></h4>
							<p>Book now and <span class="color-red-3">save 30%</span></p>
							<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
							<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
					<div class="radius-top">
					<img src="{{ asset('/storage/product/3/3.JPG') }}" alt="">
					</div>
					<div class="title">
					<h5>from <strong class="color-red-3">$860</strong> / person</h5>
					<h6 class="color-grey-3">one way flights</h6>
					<h4><b>Flights to monaco</b></h4>
					<p>Book now and <span class="color-red-3">save 30%</span></p>
					<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
					<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
					</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
					<div class="radius-top">
					<img src="{{ asset('/storage/product/1/1.JPG') }}" alt="">
					</div>
					<div class="title">
					<h5>from <strong class="color-red-3">$860</strong> / person</h5>
					<h6 class="color-grey-3">one way flights</h6>
					<h4><b>flights to new zealand</b></h4>
					<p>Book now and <span class="color-red-3">save 30%</span></p>
					<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
					<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
					</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
					<div class="radius-top">
					<img src="{{ asset('/storage/product/2/2.JPG') }}" alt="">
					</div>
					<div class="title">
					<h5>from <strong class="color-red-3">$860</strong> / person</h5>
					<h6 class="color-grey-3">one way flights</h6>
					<h4><b>Cheap Flights to Paris</b></h4>
					<p>Book now and <span class="color-red-3">save 30%</span></p>
					<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
					<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
					</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
					<div class="radius-top">
					<img src="{{ asset('/storage/product/1/1.JPG') }}" alt="">
					</div>
					<div class="title">
					<h5>from <strong class="color-red-3">$860</strong> / person</h5>
					<h6 class="color-grey-3">one way flights</h6>
					<h4><b>Cheap Flights to london</b></h4>
					<p>Book now and <span class="color-red-3">save 30%</span></p>
					<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
					<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
					</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
					<div class="radius-top">
					<img src="{{ asset('/storage/product/2/2.JPG') }}" alt="">
					</div>
					<div class="title">
					<h5>from <strong class="color-red-3">$860</strong> / person</h5>
					<h6 class="color-grey-3">one way flights</h6>
					<h4><b>Flights to monaco</b></h4>
					<p>Book now and <span class="color-red-3">save 30%</span></p>
					<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
					<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
					</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hotel-item style-7">
					<div class="radius-top">
					<img src="{{ asset('/storage/product/3/3.JPG') }}" alt="">
					</div>
					<div class="title">
					<h5>from <strong class="color-red-3">$860</strong> / person</h5>
					<h6 class="color-grey-3">one way flights</h6>
					<h4><b>flights to new zealand</b></h4>
					<p>Book now and <span class="color-red-3">save 30%</span></p>
					<a href="#" class="c-button b-40 bg-red-3 hv-red-3-o">book now</a>
					<a href="#" class="c-button b-40 color-grey-3 hv-o fr"><img src="img/flag_icon_grey.png" alt="">view more</a>
					</div>
					</div>
				</div>
				</div>
				<div class="pagination pagination-hidden"></div>
			</div>
			<div class="swiper-arrow-left offers-arrow color-4"><span class="fa fa-angle-left"></span></div>
			<div class="swiper-arrow-right offers-arrow color-4"><span class="fa fa-angle-right"></span></div>
		</div>
	</div>
</div>
</div>
</div>
@endsection