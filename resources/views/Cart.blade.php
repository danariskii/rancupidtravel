@extends('layouts.master-frontend')

@section('main-content')
<div class="inner-banner">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container" style="padding-top: 5em;">
			<!-- <ul class="banner-breadcrumb color-white clearfix">
			<li><a class="link-blue-2" href="#">home</a> /</li>
			<li><a class="link-blue-2" href="#">tours</a> /</li>
			<li><span class="color-blue-2">list tours</span></li>
			</ul> -->
			<h2 class="color-white">Lets Travel the World</h2>
			<!-- <h4 class="color-white">We found: <span>640</span> tours</h4> -->
		</div>
	</div>
</div>
<div class="main-wraper padd-90 bg-grey-2 tabs-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<div class="second-title">
			<h2>Cart</h2>
			</div>
			</div>
		</div>
		<div class="row">
		<div class="col-md-12">
			<div class="simple-tab type-2 tab-wrapper">
			<!-- <div class="tab-nav-wrapper">
				<div class="nav-tab  clearfix">
					<div class="nav-tab-item active">
					Sports
					</div>
					<div class="nav-tab-item">
					Culture and History
					</div>
					<div class="nav-tab-item">
					Nightlife
					</div>
					<div class="nav-tab-item">
					hotels
					</div>
					<div class="nav-tab-item">
					Flights
					</div>
				</div>
			</div> -->
			<div class="tabs-content tabs-wrap-style clearfix" style="background-color: white;">
			<div class="tab-info active">
				<div class="acc-body">
					<div class="acc-body-block">

<!-- 							<div class="alert bg-1" style="background-color: #c4eeff; color:black;">
							</div> -->
							<style type="text/css">
.datagrid table { 
	border-collapse: collapse; 
	text-align: left; 
	width: 100%; 
	} 
.datagrid {
	font: normal 12px/150% Arial, Helvetica, sans-serif;
	background: #fff;
	overflow: hidden;
	border: 1px solid #9BBCC9;
	-webkit-border-radius: 9px;
	-moz-border-radius: 9px;
	border-radius: 9px;
	}
.datagrid table td, .datagrid table th {
	padding: 8px 10px; 
	}
.datagrid table thead th {
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #F2F2F2), color-stop(1, #F2F2F2) );
	background:-moz-linear-gradient( center top, #F2F2F2 5%, #F2F2F2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#F2F2F2', endColorstr='#F2F2F2');
	background-color:#F2F2F2;
	color:#000000;
	font-size: 15px;
	font-weight: bold;
	}
.datagrid table thead th:first-child {
	border: none;
	}
.datagrid table tbody td {
	color: #007DB8;
	border-left: 2px solid #F2F2F2;
	font-size: 12px;
	border-bottom: 2px solid #F2F2F2;
	font-weight: bold;
	}
.datagrid table tbody td:first-child {
	border-left: none;
	}
.datagrid table tbody tr:last-child td {
	border-bottom: none;
	}
.datagrid table tfoot td div { 
	border-top: 1px solid #9BBCC9;background: #F2F2F2;
	} 
.datagrid table tfoot td { 
	padding: 0; font-size: 15px 
	} 
.datagrid table tfoot td div{ 
	padding: 8px; 
	}
.datagrid table tfoot td ul { 
	margin: 0; 
	padding:0; 
	list-style: none; 
	text-align: right; 
	}
.datagrid table tfoot  li { 
	display: inline; 
	}
.datagrid table tfoot li a { 
	text-decoration: none;
	display: inline-block;  
	padding: 2px 8px; 
	margin: 1px;
	color: #FFFFFF;
	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #007DB8), color-stop(1, #007DB8) );
	background:-moz-linear-gradient( center top, #007DB8 5%, #007DB8 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#007DB8', endColorstr='#007DB8');
	background-color:#007DB8; 
	}
.datagrid table tfoot ul.active, .datagrid table tfoot ul a:hover { text-decoration: none;
	border-color: #C4EEFF;
	color: #000000; 
	background: none; 
	background-color:#C4EEFF;
	}
div.dhtmlx_window_active, div.dhx_modal_cover_dv 
	{ 
		position: fixed !important; 
	}
							</style>
							<?php
						$item_details = Array();
							?>
						@foreach( $products as $product)
<div class="datagrid" style="margin-bottom: 1em;">
	<table>
		<thead>
			<tr style="border: 1px solid #9BBCC9;"">
				<th colspan="3"><span>Trip by <a href="#" style="font-weight: bold; color: #007DB8;">{{ $product->Agent_Name }}</a></span></th>
				<th><strong><a href=""> Delete </a></strong><i class="fa fa-times-circle-o"></i> </th>
			</tr>
			<tr>
				<th>Trip name</th>
				<th style="width: 15%;">Number of Person</th>
				<th style="width: 15%;">price/ person</th>
				<th style="width: 15%;">Sub Total Price</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<img src="{{ '/storage/product/'.$product->id.'/'.$product->dp_item }}" alt="" style="width: 10%; margin-bottom: 0; vertical-align: middle;">
					{{ $product->title }}
				</td>
				<td>
					<?php
						$user_id = Auth::user()->id;


						$quantity = App\cart::select('quantity')->where('customer_id', $user_id)->where('product_id', $product->id)->first();
					?>
					{{ $quantity->quantity }}
					<a href=""> (edit)</a>
				</td>
				<td>
					<?php
						$price = App\Detail_price::select('price')
                        ->where('product_id', $product->id)
                        ->where('min_quantity', '<=', $quantity->quantity)
                        ->where('max_quantity', '>=', $quantity->quantity)->first();

                       if ($price == null)
                       {
						$price = App\Detail_price::select('price')
						->where('product_id', $product->id)
						->where('min_quantity', ' >', $quantity->quantity)->first();
                       }
					?>
					{{ $price->price }}
				</td>
					<?php
						$subtotal = $quantity->quantity * $price->price;
					?>
				<td>
					{{ $subtotal }}
				</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3">
					<div id="paging">
						<a href="" style=" color: black;">
							Total 
						</a>
						<!-- <ul>
							<li><a href="#"><span>Previous</span></a></li>
							<li><a href="#" class="active"><span>1</span></a></li>
							<li><a href="#"><span>2</span></a></li>
							<li><a href="#"><span>3</span></a></li>
							<li><a href="#"><span>4</span></a></li>
							<li><a href="#"><span>5</span></a></li>
							<li><a href="#"><span>Next</span></a></li>
						</ul> -->
					</div>
				</td>
				<td style="text-align: center; color: black; border: 1px solid #9BBCC9;">
					{{ $subtotal }}
				</td>
			</tr>
		</tfoot>
	</table>
</div>							
<?php

    $values = Array(
	  'id' => $product->id,
	  'price' => $price->price,
	  'quantity' => $quantity->quantity,
	  'name' => $product->title
    );

    array_push($item_details, $values);
?>
						@endforeach



<?php


//Set Your server key
Veritrans_Config::$serverKey = "VT-server-RF7wcX1Z2p0zeNl3iFRybmQK";

// Uncomment for production environment
// Veritrans_Config::$isProduction = true;

// Enable sanitization
Veritrans_Config::$isSanitized = true;

// Enable 3D-Secure
Veritrans_Config::$is3ds = true;

// Required
$transaction_details = array(
  'order_id' => rand(),
  'gross_amount' => 94000, // no decimal allowed for creditcard
);


// var_dump($item_details);

// Optional
// $billing_address = array(
//   'first_name'    => "Andri",
//   'last_name'     => "Litani",
//   'address'       => "Mangga 20",
//   'city'          => "Jakarta",
//   'postal_code'   => "16602",
//   'phone'         => "081122334455",
//   'country_code'  => 'IDN'
// );

// // Optional
// $shipping_address = array(
//   'first_name'    => "Obet",
//   'last_name'     => "Supriadi",
//   'address'       => "Manggis 90",
//   'city'          => "Jakarta",
//   'postal_code'   => "16601",
//   'phone'         => "08113366345",
//   'country_code'  => 'IDN'
// );

// Optional
// $customer_details = array(
//   'first_name'    => "Andri",
//   'last_name'     => "Litani",
//   'email'         => "andri@litani.com",
//   'phone'         => "081122334455",
//   'billing_address'  => $billing_address
//   'shipping_address' => $shipping_address
// );

// Optional, remove this to display all available payment methods
$enable_payments = array('credit_card','cimb_clicks','mandiri_clickpay','echannel');

// Fill transaction details
$transaction = array(
  'enabled_payments' => $enable_payments,
  'transaction_details' => $transaction_details,
  'item_details' => $item_details,
);
  // 'customer_details' => $customer_details,

$snapToken = Veritrans_Snap::getSnapToken($transaction);
// echo "snapToken = ".$snapToken;
?>

						<div>
							<button style="float: left; background-color: red; color: white;" class="c-button  hv-transparent"><a href="/">Back</a></button>
							<button style="float: right; background-color: #23b0e8; color: white;" class="c-button  hv-transparent" id="pay-button">
								Choose Payment Method >>		
							</button>
							<!-- <pre><div id="result-json">JSON result will appear here after payment:<br></div></pre> -->
						</div>

					</div>
				</div>
			</div>
			</div>
			</div>
		</div>
		</div>
	</div>
</div>

<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="VT-client-GV_p2jBzOwMAoizy"></script>
    <script type="text/javascript">
      document.getElementById('pay-button').onclick = function(){
        // SnapToken acquired from previous step
        // lg ngurus midtrans untuk kirim value , tapi belum make order to table
			swal({
				type: 'warning',
				title: 'Caution',
				text: 'Make sure your that items trip you want to buy ?',
				showCancelButton: true,
				confirmButtonText: 'Ok',
				showLoaderOnConfirm: true,
				allowOutsideClick: false
				}).then((result) => {
		            if (result.value) 
		            {
						swal.showLoading();
						$.ajax({
							type: "POST",
							url: "makeOrder",
						success: function () {
						swal({
						type: 'success',
						title: 'Trip has ',
						html: 'deleted',
						timer: 2000
						}).then((result) => {
						// location.reload();
						});
						},
						error: function (xhr, ajaxOptions, thrownError) {
						swal("Error deleting!", "Please try again in few minute", "error");
						}
						});
					}
			});


      };
    </script>
@endsection