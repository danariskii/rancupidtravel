@extends('layouts.master-frontend')

@section('main-content')
<div class="inner-banner">
	<img class="center-image" src="{{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }}" alt="">
	<div class="vertical-align">
		<div class="container" style="padding-top: 5em;">
			<!-- <ul class="banner-breadcrumb color-white clearfix">
			<li><a class="link-blue-2" href="#">home</a> /</li>
			<li><a class="link-blue-2" href="#">tours</a> /</li>
			<li><span class="color-blue-2">list tours</span></li>
			</ul> -->
			<?php
			$storagePath  = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
			?>
			<h2 class="color-white">{{ $keyword }}</h2>
			<h4 class="color-white">We found: <span> {{ count($products) }}</span> tours</h4>
		</div>
	</div>
</div>
<div class="list-wrapper bg-grey-2">
	<div class="container">
	<div class="row">
		
		@include('layouts.sidebar_resultpage')

		<div class="col-xs-12 col-sm-8 col-md-9">
			<div class="list-header clearfix">
				<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
					<div class="drop">
						<b>Sort by price</b>
						<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
						<span>
							<a href="#">Low price to High</a>
							<a href="#">High price to Lowest</a>
						</span>
					</div>
				</div>
				<div class="drop-wrap drop-wrap-s-4 color-4 list-sort">
					<div class="drop">
						<b>Sort by ranking</b>
						<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
						<span>
							<a href="#">Low Ranking to High</a>
							<a href="#">High Ranking to Low</a>
						</span>
					</div>
				</div>
				<div class="list-view-change">
				<div class="change-grid color-1 fr active"><i class="fa fa-th"></i></div>
				<div class="change-list color-1 fr"><i class="fa fa-bars"></i></div>
				<div class="change-to-label fr color-grey-8">View:</div>
				</div>
			</div>
			<div class="grid-content clearfix">

			@foreach ($products as $product)
			<div class="list-item-entry">
				<div class="hotel-item style-8 bg-white">
					<div class="table-view">
						<div class="radius-top cell-view">
							<?php $dp_item = App\Product::find($product->id)->displayItem()->select('filename')->where('sticky', '1')->first(); ?>
							@if ( ($dp_item != null) )
								<img src="{{ asset('/storage/product/').'/'.$product->id.'/'.$dp_item->filename }}" alt="">
							@endif
							<div class="price price-s-3 red tt">hot price</div>
							<a class="c-button b-50 bg-aqua hv-transparent fr" onclick="addtowishlist('{{$product->id}}')" style="top: 0; right: 0; position: absolute;">
								<i class="fa fa-heart-o"></i>
							</a>
							<!-- <div class="price price-s-3 red tt">hot prica</div> -->
						</div>
						<div class="title hotel-middle clearfix cell-view">
							<div class="hotel-person color-dark-2 list-hidden">from<span style="color: red">
								<?php $lowestPrice = App\product::find($product->id)->lowestPrice()->select('price')->min('price'); ?>
								Rp {{ $lowestPrice }}</span></div>
							<div class="rate-wrap">
								<div class="rate">
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
								</div>
								<!-- <i>485 rewies</i> -->
							</div>
							<h4><b>{{ $product->title }}</b></h4>
							<!-- <span class="f-14 color-dark-2 grid-hidden">2 Place de la Sans Défense, Puteaux</span> -->
							<p class="f-14">{{ $product->text5 }}</p>
							<div class="hotel-icons-block grid-hidden">
								<img class="hotel-icon" src="{{ asset('/storage/assets/Icon/hotel_icon_1.png') }}" alt="">
								<img class="hotel-icon" src="{{ asset('/storage/assets/Icon/hotel_icon_2.png') }}" alt="">
								<img class="hotel-icon" src="{{ asset('/storage/assets/Icon/hotel_icon_3.png') }}" alt="">
								<img class="hotel-icon" src="{{ asset('/storage/assets/Icon/hotel_icon_4.png') }}" alt="">
								<img class="hotel-icon" src="{{ asset('/storage/assets/Icon/hotel_icon_5.png') }}" alt="">
							</div>
							<?php
								$agent_name = App\Agent::select('travel_agent_name')->where('id', $product->agent_id)->first();
								$agent_name = str_replace(' ', '-', $agent_name->travel_agent_name);
							?>
							<a href="{{ url('/detail-trip/'.$agent_name.'/'.$product->slug) }}" class="c-button bg-dr-blue hv-dr-blue-o b-40 fl list-hidden">
								<img src="img/loc_icon_small_drak.png" alt="">Detail
							</a>
							<a onclick="addtocart('{{$product->id}}')" class="c-button bg-dr-blue hv-dr-blue-o b-40 fr list-hidden">
								<img src="img/loc_icon_small_drak.png" alt="">Book Now
							</a>
						</div>
						<!-- <div class="title hotel-right bg-dr-blue clearfix cell-view">
							<div class="hotel-person color-white">from <span>$703</span></div>
							<a class="c-button b-40 bg-white color-dark-2 hv-dark-2-o grid-hidden" href="#">view more</a>
						</div> -->
					</div>
				</div>
			</div>
			@endforeach

			</div>
			<div class="c_pagination clearfix padd-120">
				<a href="#" class="c-button b-40 bg-blue-2 hv-blue-2-o fl">prev page</a>
				<a href="#" class="c-button b-40 bg-blue-2 hv-blue-2-o fr">next page</a>
				<ul class="cp_content color-1">
					<li class="active"><a href="#">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li><a href="#">4</a></li>
					<li><a href="#">...</a></li>
					<li><a href="#">10</a></li>
				</ul>
			</div>
		</div>
	</div>
	</div>
</div>
@endsection