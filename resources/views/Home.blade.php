@extends('layouts.master-frontend')

@section('main-content')
<!-- Slider Home -->
<div class="top-baner swiper-animate arrows">
	<div class="swiper-container main-slider" data-autoplay="5000" data-loop="1" data-speed="900" data-center="0" data-slides-per-view="1">
	<div class="swiper-wrapper">
		<div class="swiper-slide active" data-val="0">
			<div class="clip">
				<div class="bg bg-bg-chrome act" style="background-image:url({{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }})"></div>
			</div>
			<div class="vertical-align">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="main-title vert-title">
								<div class="top-weather-info delay-1">
									<!-- <p>Karimun Jawa</p> -->
									<!-- <img src="{{ asset('/storage/assets/Icon/weather_icon.png') }}" alt=""> -->
									<!-- <span>+36&deg;C</span> -->
								</div>
							<h1 class="color-white delay-1">Karimun Jawa</h1>
							<!-- <p class="color-white-op delay-2">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi quis leo elementum.</p> -->
							<a href="{{ url('/detail-package') }}" class="c-button bg-aqua hv-transparent delay-2"><img src="{{ asset('/storage/assets/Icon/loc_icon.png') }}" alt=""><span>view detail trip</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="swiper-slide" data-val="1">p

			<div class="clip">
				<div class="bg bg-bg-chrome act" style="background-image:url({{ asset('/storage/assets/Banner_Home/bannner_2.JPG') }})"></div>
			</div>
			<div class="vertical-align">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="main-title vert-title">
								<div class="top-weather-info delay-1">
									<!-- <p>Belitung</p> -->
									<!-- <img src="{{ asset('/storage/assets/Icon/weather_icon.png') }}" alt=""> -->
									<!-- <span>+36&deg;C</span> -->
								</div>
							<h1 class="color-white delay-1" style="color: black">Explore Belitung</h1>
							<!-- <p class="color-white-op delay-2">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi quis leo elementum.</p> -->
							<a href="{{ url('/detail-package') }}" class="c-button bg-aqua delay-2"><img src="{{ asset('/storage/assets/Icon/loc_icon.png') }}" alt=""><span>view detail trip</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pagination pagination-hidden poin-style-1"></div>
	</div>

	<!-- next and previus button -->
	<div class="arrow-wrapp m-200">
		<div class="cont-1170">
			<div class="swiper-arrow-left sw-arrow"><span class="fa fa-angle-left"></span></div>
			<div class="swiper-arrow-right sw-arrow"><span class="fa fa-angle-right"></span></div>
		</div>
	</div>

	<!-- tab bottom banner -->
<!-- 	<div class="baner-tabs">
		<div class="text-center">
			<div class="drop-tabs">
				<b>hotels</b>
				<a href="#" class="arrow-down"><i class="fa fa-angle-down"></i></a>
				<ul class="nav-tabs tpl-tabs tabs-style-1">
					<li class="active click-tabs"><a href="#one" data-toggle="tab" aria-expanded="false">hotels</a></li>
					<li class="click-tabs"><a href="#two" data-toggle="tab" aria-expanded="false">flights</a></li>
					<li class="click-tabs"><a href="#three" data-toggle="tab" aria-expanded="false">cars</a></li>
					<li class="click-tabs"><a href="#four" data-toggle="tab" aria-expanded="false">CRUISES</a></li>
					<li class="click-tabs"><a href="#five" data-toggle="tab" aria-expanded="false">activities</a></li>
				</ul>
			</div>
		</div>
		<div class="tab-content tpl-tabs-cont section-text t-con-style-1">
			<div class="tab-pane active in" id="one">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<div class="tabs-block">
								<h5>Your Destinationss</h5>
								<div class="input-style">
									<img src="img/loc_icon_small.png" alt="">
									<input type="text" placeholder="Enter a destination or hotel name">
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
							<div class="tabs-block">
								<h5>Check In</h5>
								<div class="input-style">
									<img src="img/calendar_icon.png" alt="">
									<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
							<div class="tabs-block">
								<h5>Check Out</h5>
								<div class="input-style">
									<img src="img/calendar_icon.png" alt="">
									<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
								</div>
							</div>
						</div>
						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
							<div class="tabs-block">
								<h5>Kids</h5>
								<div class="drop-wrap">
									<div class="drop">
										<b>01 kids</b>
										<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										<span>
											<a href="#">01 kids</a>
											<a href="#">02 kids</a>
											<a href="#">03 kids</a>
											<a href="#">04 kids</a>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
							<div class="tabs-block">
								<h5>Adults</h5>
								<div class="drop-wrap">
									<div class="drop">
									<b>01 adult</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										<span>
											<a href="#">01 adult</a>
											<a href="#">02 adult</a>
											<a href="#">03 adult</a>
											<a href="#">04 adult</a>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
							<div class="tabs-block">
								<h5>Rooms</h5>
								<div class="drop-wrap">
									<div class="drop">
									<b>01 room</b>
									<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
										<span>
											<a href="#">01 room</a>
											<a href="#">02 room</a>
											<a href="#">03 room</a>
											<a href="#">04 room</a>
										</span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
							<a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane" id="two">
				<div class="container">
				<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<div class="tabs-block">
				<h5>Your Destinationss</h5>
				<div class="input-style">
				<img src="img/loc_icon_small.png" alt="">
				<input type="text" placeholder="Enter a destination or flight name">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check In</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check Out</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Kids</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 kids</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 kids</a>
				<a href="#">02 kids</a>
				<a href="#">03 kids</a>
				<a href="#">04 kids</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Adults</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 adult</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 adult</a>
				<a href="#">02 adult</a>
				<a href="#">03 adult</a>
				<a href="#">04 adult</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Rooms</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 room</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 room</a>
				<a href="#">02 room</a>
				<a href="#">03 room</a>
				<a href="#">04 room</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
				<a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
				</div>
				</div>
				</div>
			</div>
			<div class="tab-pane" id="three">
				<div class="container">
				<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<div class="tabs-block">
				<h5>Your Destinationss</h5>
				<div class="input-style">
				<img src="img/loc_icon_small.png" alt="">
				<input type="text" placeholder="Enter a destination or car name">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check In</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check Out</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Kids</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 kids</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 kids</a>
				<a href="#">02 kids</a>
				<a href="#">03 kids</a>
				<a href="#">04 kids</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Adults</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 adult</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 adult</a>
				<a href="#">02 adult</a>
				<a href="#">03 adult</a>
				<a href="#">04 adult</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Rooms</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 room</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 room</a>
				<a href="#">02 room</a>
				<a href="#">03 room</a>
				<a href="#">04 room</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
				<a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
				</div>
				</div>
				</div>
			</div>
			<div class="tab-pane" id="four">
				<div class="container">
				<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<div class="tabs-block">
				<h5>Your Destinationss</h5>
				<div class="input-style">
				<img src="img/loc_icon_small.png" alt="">
				<input type="text" placeholder="Enter a destination or cruise name">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check In</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check Out</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Kids</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 kids</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 kids</a>
				<a href="#">02 kids</a>
				<a href="#">03 kids</a>
				<a href="#">04 kids</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Adults</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 adult</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 adult</a>
				<a href="#">02 adult</a>
				<a href="#">03 adult</a>
				<a href="#">04 adult</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Rooms</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 room</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 room</a>
				<a href="#">02 room</a>
				<a href="#">03 room</a>
				<a href="#">04 room</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
				<a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
				</div>
				</div>
				</div>
			</div>
			<div class="tab-pane" id="five">
				<div class="container">
				<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<div class="tabs-block">
				<h5>Your Destinationss</h5>
				<div class="input-style">
				<img src="img/loc_icon_small.png" alt="">
				<input type="text" placeholder="Enter a destination or activities name">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check In</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-4 col-sm-4 col-xs-6">
				<div class="tabs-block">
				<h5>Check Out</h5>
				<div class="input-style">
				<img src="img/calendar_icon.png" alt="">
				<input type="text" placeholder="Mm/Dd/Yy" class="datepicker">
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Kids</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 kids</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 kids</a>
				<a href="#">02 kids</a>
				<a href="#">03 kids</a>
				<a href="#">04 kids</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Adults</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 adult</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 adult</a>
				<a href="#">02 adult</a>
				<a href="#">03 adult</a>
				<a href="#">04 adult</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-4">
				<div class="tabs-block">
				<h5>Rooms</h5>
				<div class="drop-wrap">
				<div class="drop">
				<b>01 room</b>
				<a href="#" class="drop-list"><i class="fa fa-angle-down"></i></a>
				<span>
				<a href="#">01 room</a>
				<a href="#">02 room</a>
				<a href="#">03 room</a>
				<a href="#">04 room</a>
				</span>
				</div>
				</div>
				</div>
				</div>
				<div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
				<a href="#" class="c-button b-60 bg-aqua hv-transparent"><i class="fa fa-search"></i><span>search now</span></a>
				</div>
				</div>
				</div>
			</div>
		</div>
	</div> -->
</div>
<!-- End Home Slider -->

<!-- Item Promo Home -->
<div class="main-wraper padd-90">
	<div class="container">
		<!-- title section -->
		<div class="row">
			<div class="col-md-12">
				<div class="second-title">
					<h2>Hot Package Trip</h2>
					<!-- <p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id tor.</p> -->
				</div>
			</div>
		</div>
		<!-- item section -->
		<div class="row">
			@foreach( $HotTrips as $HotTrip)
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				<div class="radius-mask tour-block hover-aqua">
				<div class="clip">
					<div class="bg bg-bg-chrome act" style="background-image:url({{ asset('/asset/app/public/product/').'/'.$HotTrip->id.'/'.$HotTrip->dp_item }})"></div>
				</div>
				<div class="tour-layer delay-1"></div>
					<div class="tour-caption">
					<div class="vertical-align">
						<h3 class="hover-it">{{ $HotTrip->title }}</h3>
						<div class="rate">
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
							<span class="fa fa-star color-yellow"></span>
							<!-- <span class="fa fa-star color-yellow"></span> -->
						</div>
						<h4>from <b>Rp {{ $HotTrip->Trip_Price }}</b></h4>
					</div>
					<div class="vertical-bottom">
						<div class="fl">
							<!-- <div class="tour-info">
								<img src="img/people_icon.png" alt="">
								<span class="font-style-2 color-grey-4"><strong class="color-white">2</strong> adults, <strong class="color-white">1</strong> kids</span>
							</div> -->
							<div class="tour-info">
								<img src="{{ asset('/storage/assets/Icon/calendar_icon.png') }}" alt="">
								<span class="font-style-2 color-grey-4"><strong class="color-white"> {{ $HotTrip->start_date }} </strong> to 
									<strong class="color-white"> {{ $HotTrip->end_date }} </strong></span>
							</div>
						</div>
						<?php
							$Agent_Name = str_replace(' ', '-', $HotTrip->Agent_Name);
						?>
						<a class="c-button b-50 bg-aqua hv-transparent" style="float: left;" onclick="addtowishlist('{{$HotTrip->id}}')">
							<i class="fa fa-heart-o"></i>
						</a>
						<a href="{{ url('/detail-trip/'.$Agent_Name.'/'.$HotTrip->slug) }}" class="c-button b-50 bg-aqua hv-transparent fr">
							<img src="{{ asset('/storage/assets/Icon/flag_icon.png') }}" alt="">
							<span>view more</span>
						</a>
					</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
	</div>
</div>
<!-- end Item Promo Home -->

<!-- banner full width -->
<div class="main-wraper">
	<div class="clip">
		<div class="bg bg-bg-chrome" style="background-image:url({{ asset('/storage/assets/Banner_Home/bannner_1.JPG') }})"></div>
	</div>
	<div class="swiper-container" data-autoplay="0" data-loop="1" data-speed="1000" data-slides-per-view="1" id="tour-slide">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="slider-tour padd-90-90">
							<h3>from Rp. 14.268.000</h3>
							<div class="rate">
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
							</div>
							<h2>TOKYO MT FUJI NARITA</h2>
							<h5>April <b></b> to Desember <b>2017</b></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="slider-tour padd-94-94">
								<h3>from Rp. 3,321,000</h3>
								<div class="rate">
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star"></span>
									<span class="fa fa-star"></span>
								</div>
								<h2>BANGKOK PATTAYA</h2>
								<h5>Febuary<b>31th</b> to Desember <b>31th 2017</b></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="slider-tour padd-94-94">
								<h3>from Rp. 5,265,000</h3>
							<div class="rate">
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
								<span class="fa fa-star color-yellow"></span>
							</div>
								<h2>PHUKET PHI PHI ISLAND PANG NGA</h2>
								<h5>April <b></b> to Desember <b>31th</b></h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pagination poin-style-1"></div>
	</div>
</div>
<!-- end banner full width -->

<!-- Special Offers -->
<div class="main-wraper" style="margin-bottom: 2em;">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="second-title">
					<h2>Special Offers</h2>
					<!-- <p class="color-grey">We offer you special trip package in limited time, hurry up get it now^,^</p> -->
				</div>
			</div>
		</div>
	<div class="row">
		<div class="top-baner arrows">
			<div class="swiper-container offers-slider" data-autoplay="5000" data-loop="1" data-speed="500" data-slides-per-view="responsive" data-mob-slides="1" data-xs-slides="2" data-sm-slides="2" data-md-slides="3" data-lg-slides="3" data-add-slides="3">
				<div class="swiper-wrapper">

					<div class="swiper-slide" data-val="0">
						<div class="offers-block radius-mask">
							<div class="clip">
								<div class="bg bg-bg-chrome act" style="background-image:url({{ asset('/storage/gallery/1/1.jpg') }})"></div>
							</div>
							<div class="tour-layer delay-1"></div>
							<div class="vertical-top">
								<div class="rate">
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
								</div>
								<h3>Derawan</h3>
							</div>
							<div class="vertical-bottom">
								<ul class="offers-info">
									<li><b>35</b>Private Trip</li>
									<li><b>24</b>Open trip</li>
									<!-- <li><b>90</b>hotels</li> -->
								</ul>
								<p>Salah satu kepulauan di indonesia dengan biota laut yg indah</p>
								<a href="#" class="c-button bg-aqua hv-aqua-o b-40"><span>view more</span></a>
							</div>
						</div>
					</div>
					<div class="swiper-slide" data-val="1">
						<div class="offers-block radius-mask">
							<div class="clip">
								<div class="bg bg-bg-chrome act" style="background-image:url({{ asset('/storage/product/2/2.jpg') }})"></div>
							</div>
							<div class="tour-layer delay-1"></div>
							<div class="vertical-top">
								<div class="rate">
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<!-- <span class="fa fa-star color-yellow"></span> -->
									<!-- <span class="fa fa-star color-yellow"></span> -->
								</div>
								<h3>Karimun Jawa</h3>
							</div>
							<div class="vertical-bottom">
								<ul class="offers-info">
									<li><b>58</b>Open Trip</li>
									<li><b>49</b>Private Trip</li>
									<!-- <li><b>70</b>hotels</li> -->
								</ul>
								<!-- <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p> -->
								<a href="#" class="c-button bg-aqua hv-aqua-o b-40"><span>view more</span></a>
							</div>
						</div>
					</div>
					<div class="swiper-slide" data-val="2">
						<div class="offers-block radius-mask">
							<div class="clip">
								<div class="bg bg-bg-chrome act" style="background-image:url({{ asset('/storage/product/3/3.jpg') }})"></div>
							</div>
							<div class="tour-layer delay-1"></div>
							<div class="vertical-top">
								<div class="rate">
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
									<span class="fa fa-star color-yellow"></span>
								</div>
								<h3>TOKYO MT FUJI NARITA</h3>
							</div>
							<div class="vertical-bottom">
								<ul class="offers-info">
									<li><b>88</b>Open Trip</li>
									<li><b>10</b>Trip</li>
									<!-- <li><b>193</b>hotels</li> -->
								</ul>
								<!-- <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massa idporta nequetiam.</p> -->
								<a href="#" class="c-button bg-aqua hv-aqua-o b-40"><span>view more</span></a>
							</div>
						</div>
					</div>
			</div>
			<div class="pagination  poin-style-1 pagination-hidden"></div>
		</div>
		<div class="swiper-arrow-left offers-arrow"><span class="fa fa-angle-left"></span></div>
		<div class="swiper-arrow-right offers-arrow"><span class="fa fa-angle-right"></span></div>
		</div>
	</div>
	</div>
</div>
<!-- end Special Offers -->

<!-- Gallery -->
<!-- <div class="main-wraper">
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="second-title">
				<h2>Our Gallery</h2>
				<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla</p>
			</div>
		</div>
	</div>
	<div class="row col-no-padd">
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="photo-block hover-aqua">
				<div class="tour-layer delay-1"></div>
					<img src="{{ asset('/storage/gallery/1/1.jpg') }}" alt="">
					<div class="vertical-align">
				<div class="photo-title">
						<h4 class="delay-1"><b>Only <span class="color-aqua">$235</span></b></h4>
						<a class="hover-it" href="#"><h3>promotional trip</h3></a>
						<h5 class="delay-1">Orlando, Air/3 Nights</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="photo-block hover-aqua">
				<div class="tour-layer delay-1"></div>
					<img src="{{ asset('/storage/gallery/2/2.jpg') }}" alt="">
					<div class="vertical-align">
				<div class="photo-title">
						<h4 class="delay-1"><b>Only <span class="color-aqua">$235</span></b></h4>
						<a class="hover-it" href="#"><h3>promotional trip</h3></a>
						<h5 class="delay-1">Orlando, Air/3 Nights</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="photo-block hover-aqua">
				<div class="tour-layer delay-1"></div>
					<img src="{{ asset('/storage/gallery/3/3.jpg') }}" alt="">
					<div class="vertical-align">
				<div class="photo-title">
						<h4 class="delay-1"><b>Only <span class="color-aqua">$235</span></b></h4>
						<a class="hover-it" href="#"><h3>promotional trip</h3></a>
						<h5 class="delay-1">Orlando, Air/3 Nights</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="photo-block hover-aqua">
				<div class="tour-layer delay-1"></div>
					<img src="{{ asset('/storage/gallery/4/4.jpg') }}" alt="">
					<div class="vertical-align">
				<div class="photo-title">
						<h4 class="delay-1"><b>Only <span class="color-aqua">$235</span></b></h4>
						<a class="hover-it" href="#"><h3>promotional trip</h3></a>
						<h5 class="delay-1">Orlando, Air/3 Nights</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="photo-block hover-aqua">
				<div class="tour-layer delay-1"></div>
					<img src="{{ asset('/storage/gallery/5/5.jpg') }}" alt="">
					<div class="vertical-align">
				<div class="photo-title">
						<h4 class="delay-1"><b>Only <span class="color-aqua">$235</span></b></h4>
						<a class="hover-it" href="#"><h3>promotional trip</h3></a>
						<h5 class="delay-1">Orlando, Air/3 Nights</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="photo-block hover-aqua">
				<div class="tour-layer delay-1"></div>
					<img src="{{ asset('/storage/gallery/6/6.jpg') }}" alt="">
					<div class="vertical-align">
				<div class="photo-title">
						<h4 class="delay-1"><b>Only <span class="color-aqua">$235</span></b></h4>
						<a class="hover-it" href="#"><h3>promotional trip</h3></a>
						<h5 class="delay-1">Orlando, Air/3 Nights</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->
<!-- end Gallery -->



<!-- Most Popular Travel Countries -->
<!-- <div class="main-wraper padd-90">
	<div class="container">
	<div class="row">
	<div class="col-md-12">
	<div class="second-title">
	<h2>Most Popular Travel Countries</h2>
	<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla.</p>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-xs-12 col-md-6 col-md-push-6 col-sm-12">
	<div class="popular-desc text-left">
	<div class="clip">
	<div class="bg bg-bg-chrome act bg-contain" style="background-image:url(img/home_1/map_1.png)">
	</div>
	</div>
	<div class="vertical-align">
	<h3>italy, europe</h3>
	<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi Praesent at vestibulum tortor. Praesent condimentum efficitur massa, nec congue sem dapibus sed. </p>
	<h4>best cities</h4>
	<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-4">
	<ul>
	<li><a href="#">Rome</a></li>
	<li><a href="#">Venice</a></li>
	<li><a href="#">Pisa</a></li>
	<li><a href="#">Naples</a></li>
	</ul>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
	<ul>
	<li><a href="#">Bologna</a></li>
	<li><a href="#">Florence</a></li>
	<li><a href="#">Genoa</a></li>
	<li><a href="#">Turin</a></li>
	</ul>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
	<ul>
	<li><a href="#">Milan</a></li>
	<li><a href="#">Capri</a></li>
	<li><a href="#">Matera</a></li>
	<li><a href="#">Pompeii</a></li>
	</ul>
	</div>
	</div>
	<a href="#" class="c-button bg-aqua hv-transparent b-50 custom-icon">
	<img class="img-hide" src="img/flag_icon.png" alt="">
	<img class="img-hov" src="img/flag_icon_aqua.png" alt="">
	<span>view all places</span></a>
	</div>
	</div>
	</div>
	<div class="col-xs-12 col-md-6 col-md-pull-6 col-sm-12">
	<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_1.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Maecenas sit amet</a></h4>
	<h5><b class="color-aqua">from $235</b> per person</h5>
	</div>
	</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_2.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Aenean iaculis enim</a></h4>
	<h5><b class="color-aqua">from $180</b> per person</h5>
	</div>
	</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_3.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Pellentesque tempus</a></h4>
	<h5><b class="color-aqua">from $195</b> per person</h5>
	</div>
	</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_4.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Donec id maximus</a></h4>
	<h5><b class="color-aqua">from $350</b> per person</h5>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-xs-12 col-md-6 col-sm-12">
	<div class="popular-desc text-right">
	<div class="clip">
	<div class="bg bg-bg-chrome act bg-contain" style="background-image:url(img/home_1/map_2.png)">
	</div>
	</div>
	<div class="vertical-align">
	<h3>france, europe</h3>
	<p class="color-grey">Curabitur nunc erat, consequat in erat ut, congue bibendum nulla. Suspendisse id pharetra lacus, et hendrerit mi Praesent at vestibulum tortor. Praesent condimentum efficitur massa, nec congue sem dapibus sed. </p>
	<h4>best cities</h4>
	<div class="row">
	<div class="col-md-4 col-sm-4 col-xs-4">
	<ul>
	<li><a href="#">Rome</a></li>
	<li><a href="#">Venice</a></li>
	<li><a href="#">Pisa</a></li>
	<li><a href="#">Naples</a></li>
	</ul>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
	<ul>
	<li><a href="#">Bologna</a></li>
	<li><a href="#">Florence</a></li>
	<li><a href="#">Genoa</a></li>
	<li><a href="#">Turin</a></li>
	</ul>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-4">
	<ul>
	<li><a href="#">Milan</a></li>
	<li><a href="#">Capri</a></li>
	<li><a href="#">Matera</a></li>
	<li><a href="#">Pompeii</a></li>
	</ul>
	</div>
	</div>
	<a href="#" class="c-button bg-aqua hv-transparent b-50 custom-icon">
	<img class="img-hide" src="img/flag_icon.png" alt="">
	<img class="img-hov" src="img/flag_icon_aqua.png" alt="">
	<span>view all places</span>
	</a>
	</div>
	</div>
	</div>
	<div class="col-xs-12 col-md-6 col-sm-12">
	<div class="row">
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_5.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Donec id maximus</a></h4>
	<h5><b class="color-aqua">from $215</b> per person</h5>
	</div>
	</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_6.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Pellentesque tempus</a></h4>
	<h5><b class="color-aqua">from $175</b> per person</h5>
	</div>
	</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_7.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Aenean iaculis enim</a></h4>
	<h5><b class="color-aqua">from $150</b> per person</h5>
	</div>
	</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
	<div class="radius-mask popular-img">
	<div class="clip">
	<div class="bg bg-bg-chrome act" style="background-image:url(img/home_1/popular_travel_img_8.jpg)">
	</div>
	</div>
	<div class="tour-layer delay-1"></div>
	<div class="vertical-bottom">
	<h4><a href="#">Maecenas sit amet</a></h4>
	<h5><b class="color-aqua">from $290</b> per person</h5>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
</div> -->
<!-- end Most Popular Travel Countries -->
@endsection
