let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css');

/*mix.js([
		'resources/assets/js/app.js',
		'public/js/simple_date_picker/date.format.js',
		'public/js/simple_date_picker/jquery-dropdate.js'], 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css');*/

/*
mix.scripts([
    'node_modules/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
    'node_modules/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'
], 'public/js/all.js');
*/